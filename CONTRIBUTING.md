Contributing
============

When contributing to this repository, please first
[create an issue](https://gitlab.com/SnSDev/markdown-database/-/issues/new)
using the template below.  Then follow the Pull Request Process below.

Issue Template
--------------

```md
Steps Taken
-----------
1. {}

Expected Results
----------------
- {}

Actual Results
--------------
- {}

Notes
-----
(None)
```

Pull Request Process
--------------------

1. [Fork the Project](https://gitlab.com/SnSDev/markdown-database/-/forks/new)
2. Clone the project to your computer (`git clone <URL>`)
3. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
4. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
5. Push to the Branch (`git push origin feature/AmazingFeature`)
6. Open a Pull Request
