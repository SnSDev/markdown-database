use super::*;

#[derive(Debug, Default)]
pub struct FilePathCrossReferencer {
    pub data: HashSet<FilePath>,
}

impl FilePathCrossReferencer {
    pub fn populate<I: Iterator<Item = FilePath>>(&mut self, iter: I) {
        iter.for_each(|item| {
            self.data.insert(item);
        })
    }

    pub fn count(&self) -> usize {
        self.data.len()
    }

    pub fn cross_reference(&mut self, item: &FilePath) {
        self.data.remove(item);
    }
}

impl IntoIterator for FilePathCrossReferencer {
    type Item = FilePath;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        let mut vec = self.data.into_iter().collect::<Vec<Self::Item>>();

        vec.sort();
        vec.into_iter()
    }
}
