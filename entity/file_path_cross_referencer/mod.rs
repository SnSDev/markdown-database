mod entity;

#[cfg(test)]
mod test;

pub use entity::FilePathCrossReferencer;

use crate::wrapper::FilePath;
use std::collections::HashSet;
