use std::collections::HashSet;

use pretty_assertions::assert_eq;

use crate::entity::FilePathCrossReferencer;
use crate::wrapper::file_path::prelude::*;
use crate::wrapper::FilePath;

struct TestPrelude {
    file1: FilePath,
    file2: FilePath,
    file3: FilePath,
    pre_filled_cross_referencer: FilePathCrossReferencer,
}

impl Default for TestPrelude {
    fn default() -> Self {
        let root = FilePath::new(MemoryFS::new());

        let file1 = root.join("File1.test").unwrap();
        let file2 = root.join("File2.test").unwrap();
        let file3 = root.join("File3.test").unwrap();

        let mut data = HashSet::new();

        data.insert(file1.clone().into());
        data.insert(file2.clone().into());
        data.insert(file3.clone().into());

        Self {
            file1,
            file2,
            file3,
            pre_filled_cross_referencer: FilePathCrossReferencer { data },
        }
    }
}

#[test]
fn test1638497201_populate() {
    let TestPrelude {
        file1,
        file2,
        file3,
        ..
    } = TestPrelude::default();

    let mut actual = FilePathCrossReferencer::default();

    actual.populate(
        vec![file1, file2.clone(), file2]
            .into_iter()
            .map(FilePath::from),
    );
    actual.populate(vec![file3].into_iter().map(FilePath::from));

    assert_eq!(3, actual.count())
}

#[test]
fn test1638498003_cross_reference() {
    let TestPrelude {
        file2,
        pre_filled_cross_referencer: mut actual,
        ..
    } = TestPrelude::default();

    assert_eq!(3, actual.count());

    actual.cross_reference(&file2.clone().into());

    assert_eq!(2, actual.count());

    actual.cross_reference(&file2.into());

    assert_eq!(2, actual.count());
}

#[test]
fn test1638499431_into_iter() {
    let TestPrelude {
        pre_filled_cross_referencer: actual,
        ..
    } = TestPrelude::default();

    let iter: std::vec::IntoIter<FilePath> = actual.into_iter();

    assert_eq!(3, iter.len())
}
