//! Used In documentation tests

mod entity;

pub use entity::LineNumberCalculator;

use crate::data_object::PositionInFile;
use crate::wrapper::file_path::prelude::*;

/// Used In documentation tests
pub mod test_prelude {
    pub use crate::data_object::PositionInFile;
    pub use crate::entity::LineNumberCalculator;
}
