use super::*;

/// Calculate [PositionInFile] given a specific file contents and character index
#[derive(Debug, PartialEq, Clone)]
pub struct LineNumberCalculator {
    /// Vector of character index of all line breaks in supplied file
    pub line_break_position: Vec<usize>,
    pub file_path: FilePath,
    pub content: String,
}

impl TryFrom<FilePath> for LineNumberCalculator {
    type Error = VfsError;

    /// Calculate all the line break positions in the supplied file contents
    ///
    /// Example:
    /// --------
    ///
    /// ```
    /// use markdown_database_lib::entity::line_number_calculator::test_prelude::*;
    ///
    /// let calculator = LineNumberCalculator::from("0123\n567\n9012");
    ///
    /// assert_eq!([4, 8], Vec::<usize>::from(calculator))
    /// ```
    fn try_from(file_path: FilePath) -> Result<Self, VfsError> {
        let content = file_path.read_to_string()?;
        Ok(Self::from_opened_file(content, file_path))
    }
}

impl From<LineNumberCalculator> for Vec<usize> {
    /// Used for testing
    fn from(line_number_calculator: LineNumberCalculator) -> Self {
        line_number_calculator.line_break_position
    }
}

impl LineNumberCalculator {
    /// Calculated [PositionInFile] based on specified character index
    ///
    /// Example:
    /// --------
    ///
    /// ```
    /// use markdown_database_lib::entity::line_number_calculator::test_prelude::*;
    ///
    /// let calculator = LineNumberCalculator::from("0123\n567\n9012");
    ///
    /// assert_eq!(
    ///     vec![
    ///         (1, 1), // Char 0
    ///         (1, 2), // Char 1
    ///         (1, 3), // Char 2
    ///         (1, 4), // Char 3
    ///         (1, 5), // Char 4 (New Line)
    ///         (2, 1), // Char 5
    ///         (2, 2), // Char 6
    ///         (2, 3), // Char 7
    ///         (2, 4), // Char 8 (New Line)
    ///         (3, 1), // Char 9
    ///         (3, 2), // Char 10
    ///         (3, 3), // Char 11
    ///         (3, 4)  // Char 12
    ///     ]
    ///     .into_iter()
    ///     .map(|(l, c)| PositionInFile::new(l, c))
    ///     .collect::<Vec<PositionInFile>>(),
    ///     vec![0usize, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    ///         .into_iter()
    ///         .map(|char_num| calculator.calculate(char_num))
    ///         .collect::<Vec<PositionInFile>>()
    /// )
    /// ```
    pub fn calculate(&self, char_num: usize) -> PositionInFile {
        let mut line_number = 1;
        let mut last_pos = 0;
        self.line_break_position.iter().any(|pos| {
            if pos < &char_num {
                line_number += 1;
                last_pos = *pos + 1;
                false
            } else {
                true
            }
        });
        PositionInFile {
            line_number,
            character: char_num - last_pos + 1,
        }
    }

    pub fn from_opened_file(content: String, file_path: FilePath) -> Self {
        Self {
            line_break_position: content
                .char_indices()
                .filter_map(|(i, c)| (c == '\n').then_some(i))
                .collect::<Vec<usize>>(),
            content,
            file_path,
        }
    }
}
