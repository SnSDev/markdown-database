use std::ops::{Deref, DerefMut};
use std::path::PathBuf;
use vfs::FileSystem;

/// TODO
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Database<FS: FileSystem> {
    /// TODO
    pub file_system: FS,
    /// TODO
    pub base_path: PathBuf,
}

pub enum ReadTokenStreamError {
    FileDoesNotExist,
    FileOutsideDatabase,
    NonMarkdownFile,
    UnableToReadFile,
}

impl<FS: FileSystem> Deref for Database<FS> {
    type Target = FS;
    fn deref(&self) -> &Self::Target {
        &self.file_system
    }
}

impl<FS: FileSystem> DerefMut for Database<FS> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.file_system
    }
}

impl<T> From<ReadTokenStreamError> for Result<T, ReadTokenStreamError> {
    fn from(value: ReadTokenStreamError) -> Self {
        Err(value)
    }
}

impl<FS: FileSystem> Database<FS> {
    /// TODO
    pub fn borrow_file_system(&mut self) -> &mut FS {
        &mut self.file_system
    }

    /// TODO
    pub fn borrow_base_path(&self) -> &PathBuf {
        &self.base_path
    }

    /// TODO
    pub fn new(file_system: FS, base_path: PathBuf) -> Self {
        Self {
            file_system,
            base_path,
        }
    }

    // /// TODO
    // ///
    // /// # Errors
    // ///
    // /// // TODO
    // pub fn read_token_stream(
    //     &self,
    //     file_path: &Path,
    // ) -> Result<TokenIterator, ReadTokenStreamError> {
    //     let file_path_str = file_path.to_str().unwrap_or_default();

    //     if !self.file_system.exists(file_path_str).unwrap_or(false) {
    //         return ReadTokenStreamError::FileDoesNotExist.into();
    //     }

    //     if !file_path.starts_with(&self.base_path) {
    //         return ReadTokenStreamError::FileOutsideDatabase.into();
    //     }

    //     let maybe_md = file_path.extension().filter(|e| {
    //         e.to_str().unwrap_or_default().eq_ignore_ascii_case("md")
    //     });
    //     maybe_md.ok_or(ReadTokenStreamError::NonMarkdownFile)?;

    //     let file = { self.file_system.open_file(file_path_str) }
    //         .map_err(|_| ReadTokenStreamError::UnableToReadFile)?;

    //     Ok(TokenIterator::from(file))
    // }
}
