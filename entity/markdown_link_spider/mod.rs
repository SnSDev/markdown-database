mod entity;
mod local_file_filter;
mod message;

/// Used In documentation tests
pub mod test_prelude;

pub use entity::MarkdownLinkSpider;
pub use local_file_filter::{filter_map_item, LocalFileFilter};
pub use message::SpiderMessage;

use crate::compiler::TokenEnum;
use crate::data_object::{CompileProblem, Url};
use crate::entity::{compiler::prelude::*, LineNumberCalculator};
use crate::parse_file_use_case::prelude::*;
use crate::wrapper::FilePath;
use rayon::prelude::*;
use std::{collections::HashSet, sync::mpsc::Sender};

pub type EarlyReturnOutput<'a> =
    &'a mut dyn FnMut(SpiderMessage) -> Result<(), ()>;
