use super::*;

/// Recursively parse all local markdown files
pub struct MarkdownLinkSpider {
    pub unparsed_results: HashSet<FilePath>,
    pub parsed_results: HashSet<FilePath>,
}

impl MarkdownLinkSpider {
    /// **Main function** : Recursively parses local links in markdown files
    ///
    /// With each iteration:
    /// - Threads are spun off to process markdown files pending to parse in
    ///   parallel (by default, initialized with 'index.md')
    /// - As files get parsed they are passed via intra-thread message
    /// - The links inside each file are filtered for local paths
    /// - All local paths from the various threads are collected into one list
    /// - Duplicates are removed
    /// - Files already parsed are removed from the list
    /// - The remaining paths are passed to the next iteration
    /// - The loop continues until no paths remain to parse
    ///
    /// Example 1 - No `index.md`
    /// =========================
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
    /// let TestEnvironment { root, tx, rx, file1, .. } = TestEnvironment::default();
    ///
    /// // Given
    /// // No index.md in the current directory
    /// let mut spider =
    ///     MarkdownLinkSpider::new(file1.clone());
    ///
    /// // When
    /// spider.crawl(tx);
    ///
    /// // Then
    /// assert_eq!(
    ///     vec![SpiderMessage::FileNotFound(file1)],
    ///     rx.into_iter().collect::<Vec<SpiderMessage>>()
    /// )
    /// ```
    ///
    /// Example 2 - Success
    /// ===================
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
    /// let TestEnvironment {
    ///     file1,
    ///     file2,
    ///     file3,
    ///     tx,
    ///     rx,
    ///     ..
    /// } = TestEnvironment::default();
    ///
    /// // Given
    /// write!(file1.create_file().unwrap().as_mut(), "[Label](./file2.md)")
    ///     .unwrap();
    /// write!(file2.create_file().unwrap().as_mut(), "[Label](./file3.md)")
    ///     .unwrap();
    /// write!(file3.create_file().unwrap().as_mut(), "[Label](./file1.md)")
    ///     .unwrap();
    ///
    /// let mut spider = MarkdownLinkSpider {
    ///     parsed_results: Default::default(),
    ///     unparsed_results: HashSet::from_iter([file1.clone().into()]),
    /// };
    ///
    /// // When
    /// spider.crawl(tx);
    ///
    /// // Then
    /// assert_eq!(
    ///     vec![
    ///         SpiderMessage::Success(file1),
    ///         SpiderMessage::Success(file2),
    ///         SpiderMessage::Success(file3),
    ///     ],
    ///     rx.into_iter().collect::<Vec<SpiderMessage>>()
    /// )
    ///
    /// ```
    ///
    /// Example 3 - File not markdown file
    /// ==================================
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
    /// let TestEnvironment { root, tx, rx, .. } = TestEnvironment::default();
    ///
    /// // Given
    /// let file1 = root.join("test.png").unwrap();
    /// file1.create_file().unwrap();
    ///
    /// let mut spider = MarkdownLinkSpider {
    ///     parsed_results: Default::default(),
    ///     unparsed_results: HashSet::from_iter([file1.clone().into()]),
    /// };
    ///
    /// // When
    /// spider.crawl(tx);
    ///
    /// // Then
    /// assert_eq!(
    ///     vec![SpiderMessage::FileExists(file1)],
    ///     rx.try_iter().collect::<Vec<SpiderMessage>>()
    /// )
    /// ```
    pub fn crawl(&mut self, tx: Sender<SpiderMessage>) {
        let mut results = HashSet::new();
        while !self.unparsed_results.is_empty() {
            let iter = { self.unparsed_results.par_iter() }
                .map_with(tx.clone(), move |tx, absolute_path| {
                    if !absolute_path.exists().unwrap_or(false) {
                        tx.send(SpiderMessage::FileNotFound(
                            absolute_path.clone().into(),
                        ))
                        .expect("1633551321 - Unreachable: Unable to send");
                        None
                    } else if absolute_path.extension() == Some("md".to_owned())
                    {
                        Self::parse_markdown_file(absolute_path, tx)
                    } else {
                        tx.send(SpiderMessage::FileExists(
                            absolute_path.clone().into(),
                        ))
                        .expect("1633551323 - Unreachable: Unable to send");
                        None
                    }
                })
                .flatten() // Iterator<Item=Option<Vec<T>>> -> Iterator<Item=Vec<T>>
                .flatten() // Iterator<Item=Vec<T>> -> Iterator<Item=T>;
                .map(FilePath::from);
            results.par_extend(iter);

            self.move_unparsed_to_parsed();
            self.remove_completed(&mut results);
            // Keep allocation on each iteration instead of making a new one.
            std::mem::swap(&mut self.unparsed_results, &mut results);
        }
    }

    /// Move unparsed file paths into parsed file path
    ///
    /// Used in [Self::crawl]
    ///
    /// Example
    /// =======
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
    /// let TestEnvironment {
    ///     file1,
    ///     file2,
    ///     file3,
    ///     ..
    /// } = TestEnvironment::default();
    ///
    /// // Given
    /// let mut spider = MarkdownLinkSpider {
    ///     parsed_results: HashSet::from_iter([file2.clone().into()]),
    ///     unparsed_results: HashSet::from_iter([
    ///         file1.clone().into(),
    ///         file3.clone().into(),
    ///     ]),
    /// };
    ///
    /// // When
    /// spider.move_unparsed_to_parsed();
    ///
    /// // Then
    /// assert_eq!(
    ///     (
    ///         HashSet::from_iter([file1.into(), file2.into(), file3.into()]),
    ///         HashSet::<FilePathHash>::new()
    ///     ),
    ///     (spider.parsed_results, spider.unparsed_results)
    /// )
    /// ```
    pub fn move_unparsed_to_parsed(&mut self) {
        self.parsed_results.extend(self.unparsed_results.drain());
    }

    pub fn new(index_path: FilePath) -> Self {
        Self {
            unparsed_results: HashSet::from_iter([index_path.into()]),
            parsed_results: HashSet::new(),
        }
    }

    /// Given a file path, parse the file's contents, filter only local paths
    ///
    /// Example 1: File Does NOT Exist
    /// ==============================
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
    /// let TestEnvironment {
    ///     mock_bad_path,
    ///     tx,
    ///     rx,
    ///     ..
    /// } = TestEnvironment::default();
    ///
    /// // Given
    /// let path = mock_bad_path;
    ///
    /// // When
    /// let actual = MarkdownLinkSpider::parse_markdown_file(&path.into(), &tx);
    ///
    /// // Then
    /// assert_eq!(
    ///     (
    ///         None,
    ///         vec![SpiderMessage::IoError(
    ///             "WithContext { context: \"Could not get metadata for \
    ///              '/does_not_exist.file'\", cause: FileNotFound { path: \
    ///              \"/does_not_exist.file\" } }"
    ///                 .to_owned()
    ///         )]
    ///     ),
    ///     (actual, rx.try_iter().collect::<Vec<SpiderMessage>>())
    /// );
    /// ```
    ///
    /// Example 2: File Exists
    /// ======================
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
    /// let TestEnvironment {
    ///     parsable_file_path,
    ///     parsed_file_contents,
    ///     url,
    ///     tx,
    ///     rx,
    ///     ..
    /// } = TestEnvironment::default();
    ///
    /// // Given
    /// let path = parsable_file_path.clone();
    ///
    /// // When
    /// let actual = MarkdownLinkSpider::parse_markdown_file(&path.into(), &tx);
    ///
    /// // Then
    /// assert_eq!(
    ///     (
    ///         Some(parsed_file_contents),
    ///         vec![
    ///             SpiderMessage::Url(url),
    ///             SpiderMessage::Success(parsable_file_path)
    ///         ]
    ///     ),
    ///     (actual, rx.try_iter().collect::<Vec<SpiderMessage>>())
    /// );
    /// ```
    pub fn parse_markdown_file(
        absolute_path: &FilePath,
        tx: &Sender<SpiderMessage>,
    ) -> Option<Vec<FilePath>> {
        let file_contents = match absolute_path.read_to_string() {
            Ok(c) => c,
            Err(e) => {
                tx.send(e.into())
                    .expect("1633404795 - Unreachable: Unable to send");
                return None;
            }
        };

        let file_path: FilePath = absolute_path.clone().into();
        let line_number_calculator = LineNumberCalculator::from_opened_file(
            file_contents,
            file_path.clone(),
        );

        let request_model = ParseFileRequestModel {
            file_contents,
            file_path: file_path.clone(),
        };

        let vec: Vec<FilePath> = LocalFileFilter::new(
            parse_file(request_model).expect(
                "1630906070 - Unreachable: parse_markdown_file use case does \
                 not have any error variants",
            ),
            tx.clone(),
            file_path,
            line_number_calculator,
        )
        .collect();

        tx.send(SpiderMessage::Success(absolute_path.clone().into()))
            .expect("1631557548 - Unreachable: Unable to send");
        Some(vec)
    }

    /// Remove already parsed file paths
    ///
    /// Used in [Self::crawl]
    ///
    /// Example
    /// =======
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
    /// let TestEnvironment {
    ///     file1,
    ///     file2,
    ///     file3,
    ///     ..
    /// } = TestEnvironment::default();
    ///
    /// let mut spider = MarkdownLinkSpider {
    ///     parsed_results: HashSet::from_iter([file2.clone().into()]),
    ///     unparsed_results: Default::default(),
    /// };
    ///
    /// // Given
    /// let mut actual = HashSet::from_iter([
    ///     file3.clone().into(),
    ///     file1.clone().into(),
    ///     file2.clone().into(),
    ///     file1.clone().into(),
    ///     file2.into(),
    /// ]);
    ///
    /// // When
    /// spider.move_unparsed_to_parsed();
    /// spider.remove_completed(&mut actual);
    ///
    /// // Then
    /// assert_eq!(HashSet::from_iter([file1.into(), file3.into()]), actual)
    /// ```
    pub fn remove_completed(&self, new: &mut HashSet<FilePath>) {
        new.retain(|a| !self.parsed_results.contains(a));
    }
}
