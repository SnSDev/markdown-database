use super::*;

#[derive(Debug, PartialEq, Clone)]
pub enum SpiderMessage {
    CompileProblem(CompileProblem),
    Url(Url),
    IoError(String),
    Success(FilePath),
    FileExists(FilePath),
    FileNotFound(FilePath),
}

impl From<VfsError> for SpiderMessage {
    fn from(vfs_error: VfsError) -> Self {
        Self::IoError(format!("{:?}", vfs_error))
    }
}

impl From<CompileProblem> for SpiderMessage {
    fn from(compile_problem: CompileProblem) -> Self {
        Self::CompileProblem(compile_problem)
    }
}

impl From<Url> for SpiderMessage {
    fn from(url: Url) -> Self {
        Self::Url(url)
    }
}

impl From<FilePath> for SpiderMessage {
    fn from(vfs_path: FilePath) -> Self {
        Self::Success(vfs_path)
    }
}
