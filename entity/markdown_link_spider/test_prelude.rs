pub use crate::compiler::{
    markdown_link::{MarkdownLinkLocalFileToken, MarkdownLinkUrlToken},
    markdown_link_alias::MarkdownLinkAliasUnusedDefinitionToken,
    TokenEnum,
};
pub use crate::data_object::{
    CompileProblem, MarkdownLink, PositionInFile, ProblemLevel,
};
pub use crate::entity::{
    markdown_link_spider::filter_map_item, MarkdownLinkSpider, SpiderMessage,
};
pub use crate::wrapper::file_path::prelude::*;
pub use crate::wrapper::FilePath;
pub use std::collections::HashSet;

use crate::entity::line_number_calculator::LineNumberCalculator;
use std::sync::mpsc;
use url::Url;

/// Set the variables Used In documentation tests
///
/// This is used for variables that are largely irrelevant to the respective
/// tests.  Each test can extract the variables needed reducing the size
/// of the documentation.
pub struct TestEnvironment {
    pub root: FilePath,
    pub mock_path: FilePath,
    pub mock_bad_path: FilePath,
    pub parsable_file_path: FilePath,
    pub parsed_file_contents: Vec<FilePath>,
    pub position_in_file: PositionInFile,
    pub line_number_calculator: LineNumberCalculator,
    pub url: url::Url,
    pub compile_problem: CompileProblem,
    pub tx: mpsc::Sender<SpiderMessage>,
    pub rx: mpsc::Receiver<SpiderMessage>,
    pub file1: FilePath,
    pub file2: FilePath,
    pub file3: FilePath,
}

impl Default for TestEnvironment {
    fn default() -> Self {
        let root = FilePath::new(MemoryFS::default());
        let position_in_file = PositionInFile {
            line_number: 2,
            character: 90,
        };

        let parsable_file_path = root.join("parsable_file.md").unwrap();
        let mut parsable_file = parsable_file_path.create_file().unwrap();
        let parsable_file_contents = r#"[Label1631898300](Path1631898310)
        [Label1631899603](http://example.com)"#;
        let line_number_calculator =
            LineNumberCalculator::from(parsable_file_contents);
        write!(parsable_file.as_mut(), "{}", parsable_file_contents)
            .expect("1632957173");
        let (tx, rx) = mpsc::channel();

        Self {
            mock_path: root.clone(),
            file1: root.join("file1.md").unwrap(),
            file2: root.join("file2.md").unwrap(),
            file3: root.join("file3.md").unwrap(),
            mock_bad_path: root.join("does_not_exist.file").unwrap(),
            position_in_file,
            url: Url::parse("http://example.com").unwrap(),
            compile_problem: CompileProblem {
                problem_level: ProblemLevel::Warning,
                message: "Unable to locate a use for Alias `Alias Name \
                          1638382740`"
                    .to_owned(),
                file_path: root.clone(),
                position_in_file,
            },
            parsed_file_contents: vec![root.join("Path1631898310").unwrap()],
            line_number_calculator,
            parsable_file_path,
            root,
            tx,
            rx,
        }
    }
}
