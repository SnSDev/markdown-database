use super::*;

pub struct LocalFileFilter {
    pub response_model: ParseFileResponseModel,
    pub tx: Sender<SpiderMessage>,
    pub file_path: FilePath,
    pub line_number_calculator: LineNumberCalculator,
}

impl LocalFileFilter {
    pub fn new(
        response_model: ParseFileResponseModel,
        tx: Sender<SpiderMessage>,
        file_path: FilePath,
        line_number_calculator: LineNumberCalculator,
    ) -> Self {
        Self {
            response_model,
            tx,
            file_path,
            line_number_calculator,
        }
    }
}

/// Filter local files, transmit errors and other links
///
/// In the current version, the spider only follows local paths.  In order
/// to allow some versions to provide real-time output of the unused data to
/// the user (ex: CLI), when a link has an issue or is not local (eg: URL)
/// it is wrapped in an [SpiderMessage] enum and sent via
/// [std::sync::mpsc::Sender<SpiderMessage>].
///
/// Example 1: Error Supplied
/// =========================
///
/// ```
/// // Test Environment
/// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
/// let TestEnvironment {
///     compile_problem,
///     tx,
///     rx,
///     mock_path,
///     line_number_calculator,
///     ..
/// } = TestEnvironment::default();
///
/// // Given
/// let item = TokenEnum::MarkdownLinkAliasUnused(
///     MarkdownLinkAliasUnusedDefinitionToken {
///         range: 123..321,
///         alias_name: "Alias Name 1638382740".to_string(),
///     },
/// );
///
/// // When
/// let actual =
///     filter_map_item(&tx, &mock_path, &line_number_calculator, item);
///
/// // Then
/// assert_eq!(
///     (None, vec![SpiderMessage::CompileProblem(compile_problem)]),
///     (actual, rx.try_iter().collect::<Vec<SpiderMessage>>())
/// );
/// ```
///
/// Example 2: URL Supplied
/// =======================
///
/// ```
/// // Test Environment
/// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
/// let TestEnvironment {
///     url,
///     tx,
///     rx,
///     mock_path,
///     line_number_calculator,
///     ..
/// } = TestEnvironment::default();
///
/// // Given
/// let item = TokenEnum::MarkdownLinkUrl(MarkdownLinkUrlToken {
///     range: 123..321,
///     url: url.clone(),
/// });
///
/// // When
/// let actual =
///     filter_map_item(&tx, &mock_path, &line_number_calculator, item);
///
/// // Then
/// assert_eq!(
///     (None, vec![SpiderMessage::Url(url)]),
///     (actual, rx.try_iter().collect::<Vec<SpiderMessage>>())
/// );
/// ```
///
/// Example 3: Local File Supplied
/// ==============================
///
/// ```
/// // Test Environment
/// use markdown_database_lib::entity::markdown_link_spider::test_prelude::*;
/// let TestEnvironment {
///     mock_path,
///     tx,
///     rx,
///     line_number_calculator,
///     ..
/// } = TestEnvironment::default();
///
/// // Given
/// let item =
///     TokenEnum::MarkdownLinkLocalFile(MarkdownLinkLocalFileToken {
///         range: 123..321,
///         file_path: mock_path.clone(),
///     });
///
/// // When
/// let actual =
///     filter_map_item(&tx, &mock_path, &line_number_calculator, item);
///
/// // Then
/// assert_eq!(
///     (Some(mock_path), Vec::new()),
///     (actual, rx.try_iter().collect::<Vec<SpiderMessage>>())
/// );
/// ```
pub fn filter_map_item(
    tx: &Sender<SpiderMessage>,
    file_path: &FilePath,
    line_number_calculator: &LineNumberCalculator,
    item: TokenEnum,
) -> Option<FilePath> {
    match item {
        TokenEnum::MarkdownLinkLocalFile(t) => return Some(t.file_path),
        TokenEnum::MarkdownLinkUrl(t) => {
            tx.send(t.url.into())
                .expect("1631557492 - Unreachable: Unable to send");
        }
        t if t.problem_data().is_some() => {
            tx.send(
                CompileProblem::from_problem_token(
                    t.problem_data()
                        .expect("1640486504 - Unreachable: Already checked"),
                    t, // TODO: Token<_> -> Token<TokenSource::File>
                )
                .into(),
            )
            .expect("1631557512 - Unreachable: Unable to send");
        }
        _ => (), // Ignore other tokens
    };

    None
}

impl Iterator for LocalFileFilter {
    type Item = FilePath;

    fn next(&mut self) -> Option<Self::Item> {
        self.response_model
            .by_ref()
            .filter_map(|item| {
                filter_map_item(
                    &self.tx,
                    &self.file_path,
                    &self.line_number_calculator,
                    item,
                )
            })
            .next()
    }
}
