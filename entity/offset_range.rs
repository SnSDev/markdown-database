use std::ops::Range;

/// TODO
pub trait OffsetRange {
    /// TODO
    #[must_use]
    fn offset(self, offset: usize) -> Self;
    /// TODO
    #[must_use]
    fn increase_margin(self, margin: usize) -> Self;
    /// TODO
    #[must_use]
    fn decrease_margin(self, margin: usize) -> Self;
}

impl OffsetRange for Range<usize> {
    fn offset(self, offset: usize) -> Range<usize> {
        self.start + offset..self.end + offset
    }

    fn increase_margin(self, margin: usize) -> Range<usize> {
        self.start + margin..self.end - margin
    }

    fn decrease_margin(self, margin: usize) -> Range<usize> {
        self.start - margin..self.end + margin
    }
}
