//! `use_case` independent business logic

// // mod string_slice;
// mod file_path_cross_referencer;
// mod vec_xor;

// // These are needed for DocTests
// #[doc(hidden)]
// pub mod compiler;
// #[doc(hidden)]
// pub mod line_number_calculator;
// #[doc(hidden)]
// pub mod markdown_link_spider;
// #[doc(hidden)]
// pub mod presenter;

mod database;
mod offset_range;

// pub use compiler::{Coordinator, Parser, Token, Tokenizer, TokenizerResult};
// pub use line_number_calculator::LineNumberCalculator;
// pub use markdown_link_spider::{MarkdownLinkSpider, SpiderMessage};
// // pub use string_slice::StringSlice;
// pub use file_path_cross_referencer::FilePathCrossReferencer;

pub use database::Database;
pub use offset_range::OffsetRange;
