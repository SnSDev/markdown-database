use super::*;

#[derive(Debug)]
pub struct RegexTokenizer<
    R: GetRegex,
    C: FromCaptures<T>,
    T: Token,
> {
    regex: Regex,
    phantom_data: PhantomData<(R, C, T)>,
}

impl<R: GetRegex, C: FromCaptures<T>, T: Token> Default
    for RegexTokenizer<R, C, T>
{
    fn default() -> Self {
        Self {
            regex: R::get_regex(),
            phantom_data: Default::default(),
        }
    }
}

impl<
        R: GetRegex,
        C: FromCaptures<T>,
        T: Token + From<C>,
    > Tokenizer<T> for RegexTokenizer<R, C, T>
{
    fn find_next<'a>(
        &self,
        range: &Range<usize>,
        file_data: FileData,
    ) -> Option<T> {
        self.regex
            .captures(&file_data.borrow_contents()[range.clone()])
            .map(|c| C::from_captures(c, range.clone(), file_data).into())
    }
}
