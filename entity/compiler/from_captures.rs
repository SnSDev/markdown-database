use super::*;

const HANDLE_MARKDOWN_WHITESPACE_REG_EX: &str = r#"\s+"#;

pub trait FromCaptures<F: Token>: Token + Sized {
    fn from_captures(
        regex_captures: Captures,
        range: Range<usize>,
        file_data: FileData,
    ) -> F;

    fn range_from_captures(
        regex_captures: &Captures,
        range: &Range<usize>,
    ) -> Range<usize> {
        let mut result = range.clone();
        let regex_match = regex_captures.get(0).expect(
            "1636240024 - Unreachable: All matches should have 0 index",
        );
        let start = result.start;

        result.start = start + regex_match.start();
        result.end = start + regex_match.end();

        result
    }

    // TODO: Unreachable -> unreachable!
    fn error_valid_match(error_number: &str) -> String {
        format!(
            "{} - Unreachable: Only valid matches should get here",
            error_number
        )
    }

    fn handle_markdown_whitespace(str: &str) -> String {
        // let regex = Regex::new(r#"(?m)[\s\n]+"#)

        // Regex::new(r#"\s+"#) does not include new-lines.  Possible bug with
        // regex crate

        let regex = Regex::new(HANDLE_MARKDOWN_WHITESPACE_REG_EX)
            .expect("1636066822 - Unreachable: Pre-validated string");

        str.split("\\\n")
            .map(|s| regex.replace_all(s, " ").trim().to_owned())
            .collect::<Vec<String>>()
            .join("\n")
    }

    fn match_to_string(captures: &Captures, name: &str) -> Option<String> {
        captures.name(name).map(|m| m.as_str().to_string())
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    use crate::entity::compiler::prelude::*;

    #[derive(Debug, PartialEq, Clone)]
    struct DummyToken {}

    impl Token for DummyToken {
        fn problem_data(&self) -> &Option<ProblemData> {
            todo!("1640651068")
        }
    }

    impl FromCaptures<DummyToken> for DummyToken {
        fn from_captures(
            _regex_captures: Captures,
            _range: Range<usize>,
            _file_data: FileData,
        ) -> Self {
            unimplemented!("1636752601")
        }
    }

    #[test]
    fn test1636066053_handle_markdown_whitespace() {
        assert_eq!(
            "test1\ntest2 test3",
            DummyToken::handle_markdown_whitespace("test1\\\n   test2\ntest3"),
        )
    }

    #[test]
    fn test1636249214_bug() {
        assert_eq!(
            "Value 1 C",
            DummyToken::handle_markdown_whitespace("Value   1   C")
        )
    }

    #[test]
    fn test1638479044_bug_fix() {
        let test_string = "<http://example.com/1> (<http://example.com/2>)";

        assert_eq!(
            test_string,
            DummyToken::handle_markdown_whitespace(test_string)
        )
    }
}
