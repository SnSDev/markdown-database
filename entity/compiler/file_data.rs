use super::*;
use crate::{data_object::PositionInFile, entity::LineNumberCalculator};
// use crate::{entity::LineNumberCalculator, wrapper::FilePath};
// use std::rc::Rc;

pub type FileData = Rc<FileDataInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct FileDataInner {
    parent_dir: FilePath,
    line_number_calculator: LineNumberCalculator,
}

impl FileDataInner {
    pub fn from_opened_file(content: String, file_path: FilePath) -> FileData {
        let parent_dir = file_path
            .parent()
            .expect("1642216169: File path is not for file")
            .into();
        let line_number_calculator =
            LineNumberCalculator::from_opened_file(content, file_path);

        Self {
            parent_dir,
            line_number_calculator,
        }
        .into()
    }

    pub fn borrow_contents(&self) -> &str {
        &self.line_number_calculator.content
    }

    pub fn borrow_parent_dir(&self) -> &FilePath {
        &self.parent_dir
    }

    pub fn borrow_file_path(&self) -> &FilePath {
        &self.line_number_calculator.file_path
    }

    pub fn calculate_position_in_file(
        &self,
        char_num: usize,
    ) -> PositionInFile {
        self.line_number_calculator.calculate(char_num)
    }
}
