use super::*;

#[derive(Debug)]
pub struct ParserVec<T: Token, P: Parser<T>> {
    vec: Vec<P>,
    pd: PhantomData<T>,
}

impl<T: Token, P: Parser<T>> From<Vec<P>>
    for ParserVec<T, P>
{
    fn from(vec: Vec<P>) -> Self {
        ParserVec {
            vec,
            pd: PhantomData,
        }
    }
}

impl<T: Token, P: Parser<T>> std::ops::Deref
    for ParserVec<T, P>
{
    type Target = Vec<P>;

    fn deref(&self) -> &Self::Target {
        &self.vec
    }
}

impl<T: Token, P: Parser<T>> std::ops::DerefMut
    for ParserVec<T, P>
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.vec
    }
}

impl<T: Token, P: Parser<T>> Parser<T>
    for ParserVec<T, P>
{
    fn next_token(&mut self) -> Option<T> {
        self.iter_mut().find_map(P::next_token)
    }

    fn parse_token(&mut self, token: &T) {
        for parser in &mut self.vec {
            parser.parse_token(token)
        }
    }

    fn push_eof(&mut self, eof: FileCaret) {
        self.iter_mut().for_each(|p| p.push_eof(eof))
    }
}
