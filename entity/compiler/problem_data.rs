use super::*;

pub struct ProblemData {
    pub problem_level: ProblemLevel,
    pub message: String,
}
