use super::*;

#[derive(Debug, PartialEq, Clone)]
pub struct FileCaret {
    pub range: Range<usize>,
    pub inner_range: Option<Range<usize>>,
    pub file_data: FileData,
}

impl FileCaret {
    pub fn is_in_range(&self, range: &Range<usize>) -> bool {
        let Range {
            start: outer_start,
            end: outer_end,
        } = self.range;
        let Range {
            start: inner_start,
            end: inner_end,
        } = range;

        (outer_start <= *inner_start) && (outer_end >= *inner_end)
    }
}
