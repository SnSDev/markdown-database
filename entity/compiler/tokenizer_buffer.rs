use super::*;

#[derive(Debug)]
pub struct TokenizerBuffer<T: Token, Z: Tokenizer<T>> {
    result: TokenizerResult<T>,
    tokenizer: Z,
}

impl<T: Token, Z: Tokenizer<T>> TokenizerBuffer<T, Z> {
    pub fn find_first(&self, first: Option<usize>) -> Option<usize> {
        match &self.result {
            TokenizerResult::NotFound => first,
            TokenizerResult::Match(token) => {
                let Range { start, .. } =
                    token.token_source().file_carrot().range;
                match first {
                    Some(f) if f > start => Some(start),
                    None => Some(start),
                    _ => first,
                }
            }
            TokenizerResult::Pending => panic!(
                "1636612376 - prime_tokenizer should be ran before find_first"
            ),
        }
    }

    pub fn prime_tokenizer(
        &mut self,
        range: &Range<usize>,
        file_data: FileData,
    ) {
        match &self.result {
            TokenizerResult::NotFound => (), // No need to search
            TokenizerResult::Match(token)
                if token.token_source().file_carrot().is_in_range(range) => {}
            _ => {
                // Current token taken or invalid
                self.result = self.tokenizer.find_next(range, file_data).into()
            }
        }
    }

    pub fn take_if_match(&mut self, first: usize) -> Option<T> {
        match &self.result {
            TokenizerResult::Match(token)
                if token.token_source().file_carrot().range.start == first =>
            {
                std::mem::take(&mut self.result).into()
            }
            _ => None,
        }
    }

    pub fn vec_from(vec: Vec<Z>) -> Vec<TokenizerBuffer<T, Z>> {
        vec.into_iter().map(TokenizerBuffer::from).collect()
    }

    pub fn clear_buffer(&mut self) {
        self.result = TokenizerResult::default();
    }
}

impl<T: Token, Z: Tokenizer<T>> From<Z>
    for TokenizerBuffer<T, Z>
{
    fn from(tokenizer: Z) -> Self {
        Self {
            result: Default::default(),
            tokenizer,
        }
    }
}
