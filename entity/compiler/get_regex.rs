use super::*;

pub trait GetRegex {
    fn get_regex() -> Regex;

    fn regex_from_const(str: &str, error_number: &str) -> Regex {
        regex_from_const(str, error_number)
    }
}

pub fn regex_from_const(str: &str, error_number: &str) -> Regex {
    Regex::new(str).unwrap_or_else(|_| {
        panic!("{} - Unreachable: Pre-validated string", error_number)
    })
}
