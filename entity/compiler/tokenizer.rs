use super::*;

pub trait Tokenizer<T: Token> {
    fn find_next(&self, range: &Range<usize>, file_data: FileData)
        -> Option<T>;
}
