use super::*;

#[derive(Debug, PartialEq)]
pub enum TokenizerResult<T: Token> {
    /// Token found at [\start_index]
    Match(T),

    /// Token not found in specified range
    NotFound,

    /// Specified range not searched
    Pending,
}

impl<T: Token> Default for TokenizerResult<T> {
    fn default() -> Self {
        Self::Pending
    }
}

impl<T: Token> From<Option<T>> for TokenizerResult<T> {
    fn from(result: Option<T>) -> Self {
        result.map_or(Self::NotFound, Self::Match)
    }
}

impl<T: Token> From<TokenizerResult<T>> for Option<T> {
    fn from(result: TokenizerResult<T>) -> Self {
        match result {
            TokenizerResult::Match(t) => Some(t),
            _ => None,
        }
    }
}

impl<T: Token + PartialEq> TokenizerResult<T> {
    pub fn prime_tokenizer<Z: Tokenizer<T>>(
        status: Self,
        range: &Range<usize>,
        tokenizer: &Z,
        file_data: FileData,
    ) -> Self {
        let start = range.start;
        match status {
            Self::Match(token)
                if token.token_source().file_carrot().range.start < start =>
            {
                Self::Match(token)
            }
            Self::NotFound => Self::NotFound,
            _ => tokenizer.find_next(range, file_data).into(),
        }
    }

    pub fn unwrap_start_or(&self, default: usize) -> usize {
        self.start().unwrap_or(default)
    }

    pub fn start(&self) -> Option<usize> {
        match self {
            Self::Match(token) => {
                Some(token.token_source().file_carrot().range.start)
            }
            _ => None,
        }
    }

    pub fn overwrite_if_lower(&self, lowest: usize) -> usize {
        std::cmp::min(lowest, self.unwrap_start_or(lowest))
    }

    pub fn is_before(&self, start: usize) -> bool {
        self.start().unwrap_or_else(|| {
            panic!("1633828492 - Ran is_before on non-match")
        }) < start
    }

    pub fn into_token(self) -> T {
        match self {
            Self::Match(token) => token,
            _ => panic!("1633830282 - into_token on non-match"),
        }
    }

    pub fn is_pending(&self) -> bool {
        self == &Self::Pending
    }
}
