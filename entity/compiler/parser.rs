pub use super::*;

pub trait Parser<T: Token> {
    // Return the next token pending to be released (if any)
    fn next_token(&mut self) -> Option<T>;

    fn parse_token(&mut self, token: &T);

    // The end of the file has been reached, check for errors
    fn push_eof(&mut self, eof: FileCaret);
}
