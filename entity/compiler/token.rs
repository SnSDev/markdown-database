use super::*;

pub trait Token {
    fn problem_data(&self) -> &Option<ProblemData>;

    fn from_file(self, file_caret: FileCaret) -> TokenSource<Self>
    where
        Self: std::marker::Sized,
    {
        TokenSource::File {
            token: self,
            file_caret,
        }
    }

    fn from_code(self) -> TokenSource<Self>
    where
        Self: std::marker::Sized,
    {
        TokenSource::Code { token: self }
    }
}
