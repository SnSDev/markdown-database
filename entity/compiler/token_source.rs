use std::ops::Deref;

use super::*;

#[derive(Debug, PartialEq, Eq)]
pub enum TokenSource<T: Token> {
    File { token: T, file_caret: FileCaret },
    Code { token: T },
}

impl<T: Token> Deref for TokenSource<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        match self {
            Self::File { token, .. } => token,
            Self::Code { token } => token,
        }
    }
}

impl<T: Token> TokenSource<T> {
    pub fn is_from_file(&self) -> bool {
        match self {
            Self::File { .. } => true,
            _ => false,
        }
    }

    pub fn is_from_code(&self) -> bool {
        match self {
            Self::Code { .. } => true,
            _ => false,
        }
    }

    pub fn file_caret(&self) -> &FileCaret {
        match self {
            Self::File { file_caret, .. } => file_caret,
            _ => panic!("1642387571 - No file caret on token from code"),
        }
    }
}
