//! A struct that converts strings into tokens

mod coordinator;
pub mod markdown;
mod response;
mod token;
mod yaml;

/// Glob import what is needed to use or impl [Tokenizer]
pub mod prelude {
    pub use super::Response as TokenizerResponse;
    pub use crate::tokenizer::*;
}

pub use crate::entity::OffsetRange;
pub use coordinator::Coordinator;
pub use markdown::prelude::*;
pub use regex::Regex;
pub use response::Response;
pub use std::ops::Range;
pub use token::{
    CharacterEscaped, ColumnAlignment, LinkType, TaskStatus, Token,
    Type as TokenType,
};
pub use yaml::prelude::*;

/// TODO
pub trait Tokenizer {
    /// TODO
    fn capture_tokens(&self, file: &str, in_range: Range<usize>) -> Response;
}
