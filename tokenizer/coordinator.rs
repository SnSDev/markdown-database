use crate::tokenizer::prelude::*;

/// TODO
pub enum TokenizerEnum {
    /// TODO
    MarkdownEmphasis(EmphasisTokenizer),
    /// TODO
    MarkdownHeading(HeadingTokenizer),
    /// TODO
    MarkdownList(ListTokenizer),
    /// TODO
    MarkdownHorizontalRule(HorizontalRuleTokenizer),
    /// TODO
    MarkdownLink(LinkTokenizer),
    /// TODO
    Temp,
}

impl Tokenizer for TokenizerEnum {
    fn capture_tokens(&self, file: &str, in_range: Range<usize>) -> Response {
        match self {
            TokenizerEnum::MarkdownEmphasis(t) => {
                t.capture_tokens(file, in_range)
            }
            TokenizerEnum::MarkdownHeading(t) => {
                t.capture_tokens(file, in_range)
            }
            TokenizerEnum::MarkdownList(t) => t.capture_tokens(file, in_range),
            TokenizerEnum::MarkdownHorizontalRule(t) => {
                t.capture_tokens(file, in_range)
            }
            TokenizerEnum::MarkdownLink(t) => t.capture_tokens(file, in_range),
            Self::Temp => {
                unreachable!("1674187294 - Variant to suppress clippy pedantic")
            }
        }
    }
}

impl From<EmphasisTokenizer> for TokenizerEnum {
    fn from(value: EmphasisTokenizer) -> Self {
        TokenizerEnum::MarkdownEmphasis(value)
    }
}

impl From<HeadingTokenizer> for TokenizerEnum {
    fn from(value: HeadingTokenizer) -> Self {
        TokenizerEnum::MarkdownHeading(value)
    }
}

impl From<ListTokenizer> for TokenizerEnum {
    fn from(value: ListTokenizer) -> Self {
        TokenizerEnum::MarkdownList(value)
    }
}

impl From<HorizontalRuleTokenizer> for TokenizerEnum {
    fn from(value: HorizontalRuleTokenizer) -> Self {
        TokenizerEnum::MarkdownHorizontalRule(value)
    }
}

impl From<LinkTokenizer> for TokenizerEnum {
    fn from(value: LinkTokenizer) -> Self {
        TokenizerEnum::MarkdownLink(value)
    }
}

/// TODO
pub struct Coordinator {
    tokenizer_vec: Vec<TokenizerEnum>,
    file: String,
    caret: usize,
    file_len: usize,
}

impl Coordinator {
    /// TODO
    #[must_use]
    pub fn new(file: &str) -> Self {
        Self {
            tokenizer_vec: Vec::new(),
            file: file.to_owned(),
            caret: 0,
            file_len: file.len(),
        }
    }

    /// TODO
    #[must_use]
    pub fn with_tokenizer<Z: Tokenizer + Default + Into<TokenizerEnum>>(
        mut self,
    ) -> Self {
        self.tokenizer_vec.push(Z::default().into());
        self
    }
}

impl Iterator for Coordinator {
    type Item = Vec<Token>;

    fn next(&mut self) -> Option<Self::Item> {
        let first_response = self
            .tokenizer_vec
            .iter()
            .map(|z| z.capture_tokens(&self.file, self.caret..self.file_len))
            .filter(|r| !r.is_empty()) // Needed for ?
            .min_by(|a, b| a.min().cmp(&b.min()))?;

        let new_caret = first_response.last_inner();

        assert!(
            new_caret > self.caret,
            "1674186432 - Endless Loop Detected\n\tnew_caret: \
             {new_caret}\n\tself.caret: {}\n\tresponse: {:#?}",
            self.caret,
            first_response
        );

        self.caret = new_caret;

        Some(first_response.into())
    }
}
