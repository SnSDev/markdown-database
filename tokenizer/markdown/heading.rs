use crate::tokenizer::{
    prelude::*,
    HeadingLevel::{H1, H2, H3, H4, H5, H6},
    TokenType::Markdown,
};

/// TODO
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Level {
    /// TODO
    H1,
    /// TODO
    H2,
    /// TODO
    H3,
    /// TODO
    H4,
    /// TODO
    H5,
    /// TODO
    H6,
}

/// Tokenize markdown headings
///
/// # Example
/// TODO
pub struct Heading {
    regex_hash: Regex,
    regex_underline: Regex,
}

impl Default for Heading {
    fn default() -> Self {
        let regex_hash = Regex::new("(?m)^(?P<hashes>#{1,6}) *(?P<inner>.*)$")
            .expect("1673740224 - RegEx pre-validated");

        let regex_underline =
            Regex::new("(?m)^(?P<inner>.*)\n(?P<underline>[=-]+)$")
                .expect("1673741358 - RegEx pre-validated");

        Self {
            regex_hash,
            regex_underline,
        }
    }
}

fn capture_hash_token(
    file: &str,
    in_range: &Range<usize>,
    regex: &Regex,
) -> Option<Token> {
    regex.captures(&file[in_range.clone()]).map(|c| {
        let heading_level = match c
            .name("hashes")
            .expect("1673743376 - Already matched")
            .as_str()
        {
            "#" => H1,
            "##" => H2,
            "###" => H3,
            "####" => H4,
            "#####" => H5,
            "######" => H6,
            _ => unreachable!(),
        };

        let offset = in_range.start;

        let inner = {
            let Range { start, end } = { &c }
                .name("inner")
                .expect("1673743710 - Already matched")
                .range();
            start + offset..end + offset
        };

        let outer = {
            let Range { start, end } =
                { &c }.get(0).expect("1673743626 - Already matched").range();
            start + offset..end + offset
        };

        Token {
            token_type: Markdown(MarkdownToken::Heading(heading_level)),
            inner,
            outer,
        }
    })
}

fn capture_hash_underline(
    file: &str,
    in_range: &Range<usize>,
    regex: &Regex,
) -> Option<Token> {
    regex.captures(&file[in_range.clone()]).map(|c| {
        let heading_level = match &c
            .name("underline")
            .expect("1673746140 - Already matched")
            .as_str()[0..1]
        {
            "=" => H1,
            "-" => H2,
            _ => unreachable!(),
        };

        let offset = in_range.start;

        let inner = {
            let Range { start, end } = { &c }
                .name("inner")
                .expect("1673746229 - Already matched")
                .range();
            start + offset..end + offset
        };

        let outer = {
            let Range { start, end } =
                { &c }.get(0).expect("1673746252 - Already matched").range();
            start + offset..end + offset
        };

        Token {
            token_type: Markdown(MarkdownToken::Heading(heading_level)),
            inner,
            outer,
        }
    })
}
impl Tokenizer for Heading {
    /// TODO
    fn capture_tokens(&self, file: &str, in_range: Range<usize>) -> Response {
        match (
            capture_hash_token(file, &in_range, &self.regex_hash),
            capture_hash_underline(file, &in_range, &self.regex_underline),
        ) {
            (Some(v), None) | (None, Some(v)) => vec![v],
            (Some(a), Some(b)) if a.outer.start < b.outer.start => vec![a],
            (Some(_), Some(b)) => vec![b],
            (None, None) => Vec::new(),
        }
        .into()
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    #[test]
    fn test1673738750() {
        use crate::tokenizer::{
            prelude::*,
            HeadingLevel::{H1, H2, H3, H4, H5, H6},
            MarkdownToken::Heading,
            TokenType::Markdown,
        };
        // GIVEN
        let file = r#"
# Heading level 1
## Heading level 2
### Heading level 3
#### Heading level 4
##### Heading level 5
###### Heading level 6
Heading level 1
=

Heading level 2
-------------------------

"#;
        let tokenizer = HeadingTokenizer::default();
        let start_char = [0usize, 19, 34, 54, 78, 100, 123, 142, 159];

        // WHEN
        let actual = start_char
            .iter()
            .map(|&s| tokenizer.capture_tokens(file, s..file.len()))
            .flat_map(Vec::from)
            .collect::<Vec<Token>>();

        // THEN
        assert_eq!(
            actual,
            vec![
                Token {
                    token_type: Markdown(Heading(H1)),
                    outer: 1..18,
                    inner: 3..18,
                },
                Token {
                    token_type: Markdown(Heading(H2)),
                    outer: 19..37,
                    inner: 22..37,
                },
                Token {
                    token_type: Markdown(Heading(H3)),
                    outer: 38..57,
                    inner: 42..57,
                },
                Token {
                    token_type: Markdown(Heading(H4)),
                    outer: 58..78,
                    inner: 63..78,
                },
                Token {
                    token_type: Markdown(Heading(H5)),
                    outer: 79..100,
                    inner: 85..100,
                },
                Token {
                    token_type: Markdown(Heading(H6)),
                    outer: 101..123,
                    inner: 108..123,
                },
                Token {
                    token_type: Markdown(Heading(H1)),
                    outer: 124..141,
                    inner: 124..139,
                },
                Token {
                    token_type: Markdown(Heading(H2)),
                    outer: 143..184,
                    inner: 143..158,
                },
            ]
        );
    }
}
