use crate::tokenizer::prelude::*;

/// TODO
pub struct HorizontalRule {
    regex: Regex,
}

impl Tokenizer for HorizontalRule {
    fn capture_tokens(&self, file: &str, in_range: Range<usize>) -> Response {
        let Some(next_tag) = self.regex.captures_iter(&file[in_range.clone()]).next() else {
            return Response::empty();
        };

        let offset = in_range.start;
        let tag = next_tag.name("tag").expect("1674257246 - Already matched");
        let outer = tag.range().offset(offset);

        vec![Token {
            token_type: TokenType::Markdown(MarkdownToken::HorizontalRule),
            inner: outer.end..outer.end,
            outer,
        }]
        .into()
    }
}

impl Default for HorizontalRule {
    fn default() -> Self {
        let regex = Regex::new("(?m)^(?P<tag>\\*{3,}|-{3,}|_{3,})$")
            .expect("1674256421 - Unreachable: RegEx pre-validated");
        Self { regex }
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    #[test]
    fn test1674255885() {
        use crate::tokenizer::prelude::*;
        // GIVEN
        let file = r#"
***
---a
---
    ---

_________________"#;
        let coordinator =
            Coordinator::new(file).with_tokenizer::<HorizontalRuleTokenizer>();

        // WHEN
        let actual = coordinator.flatten().collect::<Vec<Token>>();

        // THEN
        assert_eq!(
            actual,
            vec![
                Token {
                    // ***
                    token_type: TokenType::Markdown(
                        MarkdownToken::HorizontalRule
                    ),
                    outer: 1..4,
                    inner: 4..4,
                },
                Token {
                    // ---
                    token_type: TokenType::Markdown(
                        MarkdownToken::HorizontalRule
                    ),
                    outer: 10..13,
                    inner: 13..13,
                },
                Token {
                    // _________________
                    token_type: TokenType::Markdown(
                        MarkdownToken::HorizontalRule
                    ),
                    outer: 23..40,
                    inner: 40..40,
                }
            ]
        );
    }
}
