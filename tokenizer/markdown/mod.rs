//! TODO

mod emphasis;
mod heading;
mod horizontal_rule;
mod link;
mod list;
mod yaml_front_matter_block;

pub use emphasis::Emphasis as EmphasisTokenizer;
pub use heading::{Heading as HeadingTokenizer, Level as HeadingLevel};
pub use horizontal_rule::HorizontalRule as HorizontalRuleTokenizer;
pub use link::Link as LinkTokenizer;
pub use list::{ItemType as ListItemType, List as ListTokenizer};

/// TODO
pub mod prelude {
    pub use super::{
        EmphasisTokenizer, HeadingLevel, HeadingTokenizer,
        HorizontalRuleTokenizer, LinkTokenizer, ListItemType, ListTokenizer,
        Token as MarkdownToken,
    };
}

use crate::tokenizer::prelude::*;

/// TODO
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Token {
    /// TODO
    String, //

    /// TODO
    Strong, // **Text** / __Test__ : Bold
    /// TODO
    Emphasis, // _Text_ / *Text* : Italic

    /// TODO
    Strikethrough, // ~~Text~~
    /// TODO
    TableRow, // | Datum | Datum |
    /// TODO
    ColumnDefinitionRow, // | --- | --: |
    /// TODO
    ColumnDefinition(ColumnAlignment), // | --: |
    /// TODO
    TableDatum, // | Datum |
    /// TODO
    Heading(HeadingLevel), // ## Heading
    /// TODO
    FencedCodeBlock, // ```language
    /// TODO
    FencedCodeBlockLanguage, // ```language
    /// TODO
    InlineCode, // `code`
    /// TODO
    Indent(usize), //
    /// TODO
    EscapedNewLine, // ...\
    /// TODO
    InlineLink, // <http://emeraldinspirations.xyz>
    /// TODO
    ReferenceUnknown, // ?
    /// TODO
    Collapsed, // ?
    /// TODO
    CollapsedUnknown, // ?
    /// TODO
    Shortcut, // ?
    /// TODO
    ShortcutUnknown, // ?
    /// TODO
    Autolink, // ?

    /// TODO
    Link(LinkType), // <Link> / [...](Link)
    /// TODO
    LinkTitle, // [...](... "Link Title")
    /// TODO
    LinkCaption, // [LinkCaption](...)
    /// TODO
    RefLinkUse, // [...][RefLink]
    /// TODO
    RefLinkDefinition, // [RefLink]: ...
    /// TODO
    FootnoteDefinition, // [Footnote^]: ...
    /// TODO
    FootnoteUse, // [Footnote^]
    /// TODO
    Image, // ![Title](...)

    /// TODO
    Task(TaskStatus), // [ ] Task
    /// TODO
    ListItem(ListItemType), //

    /// TODO
    SoftBreak, // ??
    /// TODO
    HardBreak, //

    /// TODO
    BlockQuote, // >

    /// TODO
    HorizontalRule, // ---

    /// TODO
    EscapedCharacter(CharacterEscaped), // \`
}
