// /// TODO
// #[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
// pub struct YamlFrontMatterBlock {}

// impl TokenizerTrait<StreamToken, Token> for YamlFrontMatterBlock {
//     fn find_next(
//         &self,
//         current_buffer: &UnparsedLineBuffer<StreamToken>,
//         last_response: Option<TokenizerResponse<StreamToken, Token>>,
//     ) -> TokenizerResponse<StreamToken, Token> {
//         if current_buffer[0].line_number != 0 {
//             // Yaml Front Matter Block only occurs on the first line
//             return TokenizerResponse::DropMe;
//         }

//         if last_response.is_none() && current_buffer[0].contents != "---" {
//             // Yaml Front Matter Block only starts with "---" at the start of the first line
//             return TokenizerResponse::DropMe;
//         }

//         let len = current_buffer.len();

//         if len > 1 && current_buffer[len - 1].contents == "---" {
//             let yaml_str = current_buffer
//                 .iter()
//                 .filter_map(|v| {
//                     // Return the contents of lines that are not the beginning or end
//                     { (1..(len - 1)).contains(&v.line_number) }
//                         .then_some(v.contents.as_str())
//                 })
//                 .collect::<Vec<&str>>()
//                 .join("\n");

//             return TokenizerResponse::TokenClosed {
//                 start_tag: 0..0, // TODO
//                 token: TokenClosed::new(Token::YamlFrontMatterBlock {
//                     lines: 0..len - 1,
//                     content: YamlLoader::load_from_str(&yaml_str),
//                 }),
//             };
//         }

//         TokenizerResponse::TokenOpened { start_tag: 0..0 }
//     }
// }

// #[cfg(test)]
// mod test {
//     use pretty_assertions::assert_eq;

//     #[test]
//     fn test1671212274() {
//         use crate::tokenizer::prelude::*;
//         use crate::tokenizer::YamlFrontMatterBlock;
//         // GIVEN
//         let buffer = VecDeque::from([StreamToken {
//             line_number: 0,
//             range: 0..0,
//             contents: "---".to_owned(),
//         }])
//         .into();
//         let tokenizer = YamlFrontMatterBlock::default();

//         // WHEN
//         let actual = tokenizer.find_next(&buffer, None);

//         // THEN
//         assert_eq!(actual, TokenizerResponse::TokenOpened { start_tag: 0..0 });
//     }

//     #[test]
//     fn test1671212676() {
//         use crate::tokenizer::prelude::*;
//         use crate::tokenizer::YamlFrontMatterBlock;
//         // GIVEN
//         let buffer = VecDeque::from([StreamToken {
//             line_number: 1,
//             range: 0..0,
//             contents: "---".to_owned(),
//         }])
//         .into();
//         let tokenizer = YamlFrontMatterBlock::default();

//         // WHEN
//         let actual = tokenizer.find_next(&buffer, None);

//         // THEN
//         assert_eq!(actual, TokenizerResponse::DropMe,);
//     }

//     #[test]
//     fn test1671212877() {
//         use crate::tokenizer::prelude::*;
//         use crate::tokenizer::YamlFrontMatterBlock;
//         // GIVEN
//         let buffer = VecDeque::from([StreamToken {
//             line_number: 0,
//             range: 0..0,
//             contents: "---anything".to_owned(),
//         }])
//         .into();
//         let tokenizer = YamlFrontMatterBlock::default();

//         // WHEN
//         let actual = tokenizer.find_next(&buffer, None);

//         // THEN
//         assert_eq!(actual, TokenizerResponse::DropMe,);
//     }

//     #[test]
//     fn test1671213032() {
//         use crate::tokenizer::prelude::*;
//         use crate::tokenizer::YamlFrontMatterBlock;
//         // GIVEN
//         let buffer = VecDeque::from([
//             StreamToken {
//                 line_number: 0,
//                 range: 0..0,
//                 contents: "---".to_owned(),
//             },
//             StreamToken {
//                 line_number: 1,
//                 range: 0..0,
//                 contents: "---".to_owned(),
//             },
//         ])
//         .into();
//         let tokenizer = YamlFrontMatterBlock::default();

//         // WHEN
//         let actual = tokenizer.find_next(
//             &buffer,
//             Some(TokenizerResponse::TokenOpened { start_tag: 0..0 }),
//         );

//         // THEN
//         assert_eq!(
//             actual,
//             TokenizerResponse::TokenClosed {
//                 start_tag: 0..0, // TODO
//                 token: TokenClosed::new(Token::YamlFrontMatterBlock {
//                     lines: 0..1,
//                     content: Ok(Vec::new())
//                 })
//             },
//         );
//     }

//     #[test]
//     fn test1671214921() {
//         use crate::tokenizer::prelude::*;
//         use crate::tokenizer::YamlFrontMatterBlock;
//         use yaml_rust::{yaml::Hash, Yaml};
//         // GIVEN
//         let buffer = VecDeque::from([
//             StreamToken {
//                 line_number: 0,
//                 range: 0..0,
//                 contents: "---".to_owned(),
//             },
//             StreamToken {
//                 line_number: 1,
//                 range: 0..0,
//                 contents: "title: This is the title specified".to_owned(),
//             },
//             StreamToken {
//                 line_number: 2,
//                 range: 0..0,
//                 contents: "---".to_owned(),
//             },
//         ])
//         .into();
//         let tokenizer = YamlFrontMatterBlock::default();

//         // WHEN
//         let actual = tokenizer.find_next(
//             &buffer,
//             Some(TokenizerResponse::TokenOpened { start_tag: 0..0 }),
//         );

//         // THEN
//         let mut expected = Hash::new();
//         expected.insert(
//             Yaml::String("title".to_owned()),
//             Yaml::String("This is the title specified".to_owned()),
//         );
//         assert_eq!(
//             actual,
//             TokenizerResponse::TokenClosed {
//                 start_tag: 0..0, // TODO
//                 token: TokenClosed::new(Token::YamlFrontMatterBlock {
//                     lines: 0..2,
//                     content: Ok(vec![Yaml::Hash(expected)])
//                 },)
//             },
//         );
//     }
// }
