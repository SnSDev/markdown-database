use std::fmt::{Display, Write};

use crate::tokenizer::prelude::{
    MarkdownToken::{Emphasis as EmphasisToken, Strong as StrongToken},
    TokenType::Markdown,
    *,
};
use pipe_trait::Pipe;

/// TODO
pub struct Emphasis {
    regex: Regex,
}

impl Default for Emphasis {
    fn default() -> Self {
        struct RegexInner<'a>(&'a str, &'a str);
        impl<'a> Display for RegexInner<'a> {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                let RegexInner(s0, s1) = self;
                write!(
                    f,
                    "(?P<{s0}{s1}>[^\\s_\\*\\n]([^_\\*\\n]*[^\\s_\\*\\n])?)"
                )
            }
        }

        const AAA: &str = "\\*\\*\\*";
        const UUU: &str = "___";
        const AA: &str = "\\*\\*";
        const UU: &str = "__";
        const A: &str = "\\*";
        const U: &str = "_";

        let mut regex_string = String::new();

        let mut first = true;
        let regex_single_tag = [
            ("aaa", AAA),
            ("uuu", UUU),
            ("aa", AA),
            ("uu", UU),
            ("a", A),
            ("u", U),
        ];
        { regex_single_tag.into_iter() }
            .try_for_each(|(key, tag)| {
                if !first {
                    regex_string.write_char('|')?;
                }
                first = false;
                write!(
                    regex_string,
                    "(?P<{key}T>{tag}{}{tag})",
                    RegexInner(key, "I")
                )
            })
            .expect("1674113040 - Why should writing to the string fail?");

        let regex_nested_tag = [
            ("aa_a", AA, A),
            ("aa_u", AA, U),
            ("uu_a", UU, A),
            ("uu_u", UU, U),
            ("a_aa", A, AA),
            ("a_uu", A, UU),
            ("u_aa", U, AA),
            ("u_uu", U, UU),
        ];
        { regex_nested_tag.into_iter() }
            .try_for_each(|(key, o_tag, i_tag)| {
                // rustfmt insists on formatting this regex,
                // which can break it and makes it harder to read
                #[cfg_attr(rustfmt, rustfmt_skip)]
                write!(
                    regex_string,
                    "|(?P<{key}O>\
                        {o_tag}\
                        (?P<{key}1>[^\\s_\\*\\n]?[^_\\*\\n]*)?\
                        (?P<{key}I>{i_tag}{}{i_tag})\
                        (?P<{key}3>[^_\\*\\n]*[^\\s_\\*\\n]?)?\
                        {o_tag}\
                    )",
                    RegexInner(key, "2")
                )
            })
            .expect("1674113041 - Why should writing to the string fail?");

        let regex = regex_string
            .pipe_borrow(Regex::new)
            .expect("1673796624 - RegEx pre-validated");

        Self { regex }
    }
}

impl Tokenizer for Emphasis {
    fn capture_tokens(&self, file: &str, in_range: Range<usize>) -> Response {
        let Some(next_tag) =
            self.regex.captures_iter(&file[in_range.clone()]).next()
        else {
            return Vec::new().into();
        };

        let offset = in_range.start;

        for key in ["aaa", "uuu"] {
            if let (Some(tag), Some(inner)) = (
                next_tag.name(&format!("{key}T")),
                next_tag.name(&format!("{key}I")),
            ) {
                return vec![
                    Token {
                        token_type: Markdown(StrongToken),
                        inner: inner.range().offset(offset).decrease_margin(1),
                        outer: tag.range().offset(offset),
                    },
                    Token {
                        token_type: Markdown(EmphasisToken),
                        inner: inner.range().offset(offset),
                        outer: tag.range().offset(offset).increase_margin(2),
                    },
                ]
                .into();
            }
        }

        for key in ["aa", "uu", "a", "u"] {
            if let (Some(tag), Some(inner)) = (
                next_tag.name(&format!("{key}T")),
                next_tag.name(&format!("{key}I")),
            ) {
                let token_type = Markdown(if key.len() > 1 {
                    StrongToken
                } else {
                    EmphasisToken
                });

                return vec![Token {
                    token_type,
                    inner: inner.range().offset(offset),
                    outer: tag.range().offset(offset),
                }]
                .into();
            }
        }

        let margin_from_token_type = |token_type| match token_type {
            StrongToken => 2,
            EmphasisToken => 1,
            _ => unreachable!("1674067930 - Not supplied variant"),
        };

        for (key, o_token_type, i_token_type) in [
            ("aa_a", StrongToken, EmphasisToken),
            ("aa_u", StrongToken, EmphasisToken),
            ("uu_a", StrongToken, EmphasisToken),
            ("uu_u", StrongToken, EmphasisToken),
            ("a_aa", EmphasisToken, StrongToken),
            ("a_uu", EmphasisToken, StrongToken),
            ("u_aa", EmphasisToken, StrongToken),
            ("u_uu", EmphasisToken, StrongToken),
        ] {
            if let (Some(o_tag), Some(i_tag)) = (
                next_tag.name(&format!("{key}O")),
                next_tag.name(&format!("{key}I")),
            ) {
                let o_outer = o_tag.range().offset(offset);
                let i_outer = i_tag.range().offset(offset);
                return vec![
                    Token {
                        token_type: Markdown(o_token_type),
                        inner: o_outer.clone().increase_margin(
                            margin_from_token_type(o_token_type),
                        ),
                        outer: o_outer,
                    },
                    Token {
                        token_type: Markdown(i_token_type),
                        inner: i_outer.clone().increase_margin(
                            margin_from_token_type(i_token_type),
                        ),
                        outer: i_outer,
                    },
                ]
                .into();
            }
        }

        let _ = [
            "aaaT", "aaaI", "uuuT", "uuuI", "aaT", "aaI", "uuT", "uuI", "aT",
            "aI", "uT", "uI", "aa_aO", "aa_a1", "aa_aI", "aa_a2", "aa_a3",
            "aa_uO", "aa_u1", "aa_u2", "aa_u3", "uu_aO", "uu_a1", "uu_aI",
            "uu_a2", "uu_a3", "uu_uO", "uu_u1", "uu_uI", "uu_u2", "uu_u3",
            "a_aaO", "a_aa1", "a_aaI", "a_aa2", "a_aa3", "a_uuO", "a_uu1",
            "a_uuI", "a_uu2", "a_uu3", "u_aaO", "u_aa1", "u_aaI", "u_aa2",
            "u_aa3", "u_uuO", "u_uu1", "u_uuI", "u_uu2", "u_uu3",
        ]; // TODO

        eprintln!("{next_tag:#?}");
        unreachable!("1673881543 - Not part of regex def")
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    #[test]
    fn test1673796820() {
        use crate::tokenizer::{
            prelude::*,
            MarkdownToken::{Emphasis, Strong},
            TokenType::Markdown,
        };
        // GIVEN
        let file = r#"
This has **Bold text** and __bold Text__.
Bold**inside**word__and__word ignored__
This has *Italic text* and _italic Text_.
Italic*inSide*word_AND_word **ignored
Ways to do**_Both_**and__*bOth*__and___boTh___and***botH***and*__BOth__*and_**BoTh**_even
*With __Strong__ inside* or __With *Emphasis* inside__
"#;
        let tokenizer = EmphasisTokenizer::default();
        tokenizer.capture_tokens(file, 0..file.len());

        // WHEN
        let coordinator =
            Coordinator::new(file).with_tokenizer::<EmphasisTokenizer>();

        let actual = coordinator.flatten().collect::<Vec<Token>>();
        let t = |t: TokenType, o: Range<usize>, i: Range<usize>| Token {
            token_type: t,
            outer: o,
            inner: i,
        }; // Pedantic Fix

        // THEN
        actual
            .iter()
            .zip(
                vec![
                    // **Bold text**
                    t(Markdown(Strong), 10..23, 12..21),
                    // __bold Text__
                    t(Markdown(Strong), 28..41, 30..39),
                    // **inside**
                    t(Markdown(Strong), 47..57, 49..55),
                    // __and__
                    t(Markdown(Strong), 61..68, 63..66),
                    // __word ignored__ // TODO: Invalid overlapping token
                    t(Markdown(Strong), 66..82, 68..80),
                    // *Italic text*
                    t(Markdown(Emphasis), 92..105, 93..104),
                    // _italic Text_
                    t(Markdown(Emphasis), 110..123, 111..122),
                    // *inSide*
                    t(Markdown(Emphasis), 131..139, 132..138),
                    // _AND_
                    t(Markdown(Emphasis), 143..148, 144..147),
                    // **_Both_**
                    t(Markdown(Strong), 173..183, 175..181),
                    // _Both_
                    t(Markdown(Emphasis), 175..181, 176..180),
                    // __*bOth*__
                    t(Markdown(Strong), 186..196, 188..194),
                    // *bOth*
                    t(Markdown(Emphasis), 188..194, 189..193),
                    // __and__  // TODO: Invalid overlapping token
                    t(Markdown(Strong), 194..201, 196..199),
                    // ___boTh___
                    t(Markdown(Strong), 199..209, 201..207),
                    // _boTh_
                    t(Markdown(Emphasis), 201..207, 202..206),
                    // ***botH***
                    t(Markdown(Strong), 212..222, 214..220),
                    // *botH*
                    t(Markdown(Emphasis), 214..220, 215..219),
                    // *and*  // TODO: Invalid overlapping token
                    t(Markdown(Emphasis), 221..226, 222..225),
                    // *__BOth__*
                    t(Markdown(Emphasis), 225..235, 226..234),
                    // __BOth__
                    t(Markdown(Strong), 226..234, 228..232),
                    // _**BoTh**_
                    t(Markdown(Emphasis), 238..248, 239..247),
                    // **BoTh**
                    t(Markdown(Strong), 239..247, 241..245),
                    // *With __Strong__ inside*
                    t(Markdown(Emphasis), 253..277, 254..276),
                    // __Strong__
                    t(Markdown(Strong), 259..269, 261..267),
                    // __With *Emphasis* inside__
                    t(Markdown(Strong), 281..307, 283..305),
                    // *Emphasis*
                    t(Markdown(Emphasis), 288..298, 289..297),
                ]
                .iter(),
            )
            .for_each(|(a, e)| assert_eq!(a, e));
    }
}
