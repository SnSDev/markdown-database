use crate::tokenizer::prelude::*;

/// TODO: Missing Documentation
pub struct Link {
    /// TODO: Missing Documentation
    ///
    /// # Errors
    pub regex: Regex,
}

impl Default for Link {
    fn default() -> Self {
        let regex = Regex::new("").expect("1674261126 - RegEx pre-validated");
        Self { regex }
    }
}

impl Tokenizer for Link {
    fn capture_tokens(&self, _file: &str, _in_range: Range<usize>) -> Response {
        todo!("1674261170")
    }
}

// Definition: (?m)^\[(?P<fn>\^)?(?P<def>[^\]]+)\]: ((?P<url1>[^ \<\>\n]+)|(\<(?P<url2>[^ \<\>\n]+)\>))( ((\"(?P<title1>[^\"\n]+)\")|('(?P<title2>[^'\n]+)')|(\((?P<title3>[^\)\n]+)\))))?
// Default: (\[(?P<img>!)?\[(?P<fn>\^)?(?P<caption>[^\[\]\n]+)\]((\((?P<url>[^\) \n]+)( \"(?P<title>[^\"\n]+)\")?\))|( ?\[(?P<ref>[^]\n]+)]))\]\((?P<url1>[^\) \n]+)\))|((?P<img2>!)?\[(?P<fn2>\^)?(?P<caption2>[^\[\]\n]+)\]((\((?P<url2>[^\) \n]+)( \"(?P<title2>[^\"\n]+)\")?\))|( ?\[(?P<ref2>[^]\n]+)])))|(\[\^(?P<fn3>[^\^\]\n]+)])|(<(?P<inline>[^> \n]+)>)

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    #[test]
    #[ignore = "unimplemented"]
    fn test1674261184() {
        use crate::tokenizer::{
            prelude::*,
            MarkdownToken::{
                FootnoteDefinition, FootnoteUse, Link as LinkToken,
                LinkCaption, LinkTitle, RefLinkDefinition, RefLinkUse,
            },
        };

        // GIVEN
        let file = r#"
[`code`](#code)
[Caption1][1]
[Caption2] [2]
[Caption3](https://domain.tld/page.ext#anchor3 "Title3")
[Caption4](https://domain.tld/page.ext#anchor4)
<https://domain.tld/page.ext#anchor5>
<user@domain.tld>
[7]: https://domain.tld/page.ext#anchor7
[8]: https://domain.tld/page.ext#anchor8 "Title8"
[9]: https://domain.tld/page.ext#anchor9 'Title9'
[10]: https://domain.tld/page.ext#anchor10 (Title10)
[11]: <https://domain.tld/page.ext#anchor11> "Title11"
[12]: <https://domain.tld/page.ext#anchor12> 'Title12'
[^13]: <https://domain.tld/page.ext#anchor13> (Title13)
![ImgBackup](./image.jpg "ImgTitle")
[![ImgBackup6](./image6.jpg "ImgTitle6")](/absolute/path6.file)
[^footnote]"#;
        let coordinator =
            Coordinator::new(file).with_tokenizer::<LinkTokenizer>();

        // WHEN
        let actual = coordinator.flatten().collect::<Vec<Token>>();

        // THEN
        let t = |t: MarkdownToken, o: Range<usize>, i: Range<usize>| Token {
            token_type: TokenType::Markdown(t),
            outer: o,
            inner: i,
        }; // Pedantic Fix
        assert_eq!(
            actual,
            vec![
                t(LinkCaption, 1..1, 2..2), // [`code`]
                t(LinkCaption, 1..1, 2..2), // (#code)
                t(LinkCaption, 1..1, 2..2), // [Caption1]
                t(LinkCaption, 1..1, 2..2), // [1]
                t(LinkCaption, 1..1, 2..2), // [Caption2]
                t(LinkCaption, 1..1, 2..2), // [2]
                t(LinkCaption, 1..1, 2..2), // [Caption3]
                t(LinkCaption, 1..1, 2..2), // (https://domain.tld/page.ext#anchor3
                t(LinkCaption, 1..1, 2..2), // "Title3")
                t(LinkCaption, 1..1, 2..2), // [Caption4]
                t(LinkCaption, 1..1, 2..2), // (https://domain.tld/page.ext#anchor4)
                t(LinkCaption, 1..1, 2..2), // <https://domain.tld/page.ext#anchor5>
                t(LinkCaption, 1..1, 2..2), // <user@domain.tld>
                t(LinkCaption, 1..1, 2..2), // [7]:
                t(LinkCaption, 1..1, 2..2), // https://domain.tld/page.ext#anchor7
                t(LinkCaption, 1..1, 2..2), // [8]:
                t(LinkCaption, 1..1, 2..2), // https://domain.tld/page.ext#anchor8
                t(LinkCaption, 1..1, 2..2), // "Title8"
                t(LinkCaption, 1..1, 2..2), // [9]:
                t(LinkCaption, 1..1, 2..2), // https://domain.tld/page.ext#anchor9
                t(LinkCaption, 1..1, 2..2), // 'Title9'
                t(LinkCaption, 1..1, 2..2), // [10]:
                t(LinkCaption, 1..1, 2..2), // https://domain.tld/page.ext#anchor10
                t(LinkCaption, 1..1, 2..2), // (Title10)
                t(LinkCaption, 1..1, 2..2), // [11]:
                t(LinkCaption, 1..1, 2..2), // <https://domain.tld/page.ext#anchor11>
                t(LinkCaption, 1..1, 2..2), // "Title11"
                t(LinkCaption, 1..1, 2..2), // [12]:
                t(LinkCaption, 1..1, 2..2), // <https://domain.tld/page.ext#anchor12>
                t(LinkCaption, 1..1, 2..2), // 'Title12'
                t(LinkCaption, 1..1, 2..2), // [^13]:
                t(LinkCaption, 1..1, 2..2), // <https://domain.tld/page.ext#anchor13>
                t(LinkCaption, 1..1, 2..2), // (Title13)
                t(LinkCaption, 1..1, 2..2), // ![ImgBackup]
                t(LinkCaption, 1..1, 2..2), // !...(./image.jpg
                t(LinkCaption, 1..1, 2..2), // "ImgTitle")
                t(LinkCaption, 1..1, 2..2), // [![ImgBackup6](./image6.jpg "ImgTitle6")]
                t(LinkCaption, 1..1, 2..2), // (/absolute/path6.file)
                t(LinkCaption, 1..1, 2..2), // ![ImgBackup6]
                t(LinkCaption, 1..1, 2..2), // !...(./image6.jpg "ImgTitle6")
                t(LinkCaption, 1..1, 2..2), // [^footnote]
            ]
        );
    }
}
