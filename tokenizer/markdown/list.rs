use crate::tokenizer::prelude::*;

/// TODO
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ItemType {
    /// TODO
    Unordered, // -
    /// TODO
    Ordered, // 1.
}

/// TODO
pub struct List {
    regex: Regex,
}

impl Default for List {
    fn default() -> Self {
        let regex = Regex::new(
            "(?m)^\\s*(?P<tag>([+*-]|(?P<ordered>\\d+)[\\.)])\\s*).*$",
        )
        .expect("1674089616 - Unreachable: RegEx pre-validated");

        Self { regex }
    }
}

impl Tokenizer for List {
    fn capture_tokens(&self, file: &str, in_range: Range<usize>) -> Response {
        let Some(next_tag) = self.regex.captures_iter(&file[in_range.clone()]).next() else {
            return Response::empty();
        };

        let offset = in_range.start;
        let item_type = if next_tag.name("ordered").is_some() {
            ItemType::Ordered
        } else {
            ItemType::Unordered
        };

        let tag = next_tag
            .name("tag")
            .expect("1674189702 - Unreachable: Already matched");

        let tag_end = tag.range().offset(offset).end;

        let outer = next_tag
            .get(0)
            .expect("1674189940 - Unreachable: Already matched")
            .range()
            .offset(offset);

        vec![Token {
            token_type: TokenType::Markdown(MarkdownToken::ListItem(item_type)),
            inner: tag_end..outer.end,
            outer,
        }]
        .into()
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    #[test]
    fn test1674088077() {
        use crate::tokenizer::{
            prelude::*,
            ListItemType::{Ordered, Unordered},
            MarkdownToken::ListItem,
            TokenType::Markdown,
        };

        // GIVEN
        let file = r#"
+ First item
  + Indented 1
    + Indented 2
*  Second item
- Third item
+Fourth item
1.Ordered Item 1
2) Ordered Item 2
3.  Ordered Item 3"#;
        let coordinator =
            Coordinator::new(file).with_tokenizer::<ListTokenizer>();

        // WHEN
        let actual = coordinator.flatten().collect::<Vec<Token>>();

        // THEN
        assert_eq!(
            actual,
            vec![
                Token {
                    // + First item
                    token_type: Markdown(ListItem(Unordered)),
                    outer: 0..13,
                    inner: 3..13,
                },
                Token {
                    // + Indented 1
                    token_type: Markdown(ListItem(Unordered)),
                    outer: 14..28,
                    inner: 18..28,
                },
                Token {
                    // + Indented 2
                    token_type: Markdown(ListItem(Unordered)),
                    outer: 29..45,
                    inner: 35..45,
                },
                Token {
                    //*  Second item
                    token_type: Markdown(ListItem(Unordered)),
                    outer: 46..60,
                    inner: 49..60,
                },
                Token {
                    //- Third item
                    token_type: Markdown(ListItem(Unordered)),
                    outer: 61..73,
                    inner: 63..73,
                },
                Token {
                    //+Fourth item
                    token_type: Markdown(ListItem(Unordered)),
                    outer: 74..86,
                    inner: 75..86,
                },
                Token {
                    //1.Ordered Item 1
                    token_type: Markdown(ListItem(Ordered)),
                    outer: 87..103,
                    inner: 89..103,
                },
                Token {
                    //2) Ordered Item 2
                    token_type: Markdown(ListItem(Ordered)),
                    outer: 104..121,
                    inner: 107..121,
                },
                Token {
                    //3.  Ordered Item 3
                    token_type: Markdown(ListItem(Ordered)),
                    outer: 122..140,
                    inner: 126..140,
                },
            ]
        );
    }
}
