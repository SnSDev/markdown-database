use crate::tokenizer::prelude::*;

/// TODO
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Response(Vec<Token>);

impl From<Vec<Token>> for Response {
    fn from(value: Vec<Token>) -> Self {
        Self(value)
    }
}

impl From<Response> for Vec<Token> {
    fn from(value: Response) -> Self {
        value.0
    }
}

impl Response {
    /// TODO
    #[must_use]
    pub fn min(&self) -> usize {
        self.0
            .iter()
            .fold(usize::MAX, |acc, v| v.outer.start.min(acc))
    }

    /// TODO
    #[must_use]
    pub fn last_inner(&self) -> usize {
        self.0.iter().fold(0, |acc, t| acc.max(t.inner.start))
    }

    /// TODO
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// TODO
    #[must_use]
    pub fn empty() -> Self {
        Self(Vec::new())
    }
}

#[cfg(test)]
mod test {
    use crate::tokenizer::prelude::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test1674178753() {
        // GIVEN
        let response = Response::from(vec![
            Token {
                token_type: TokenType::Markdown(MarkdownToken::String),
                inner: 15..20,
                outer: 5..20,
            },
            Token {
                token_type: TokenType::Markdown(MarkdownToken::String),
                inner: 10..15,
                outer: 10..15,
            },
        ]);

        // WHEN
        let actual = response.min();

        // THEN
        assert_eq!(actual, 5usize);
    }

    #[test]
    fn test1674186536() {
        // GIVEN
        let response = Response::from(vec![
            Token {
                token_type: TokenType::Markdown(MarkdownToken::String),
                inner: 15..20,
                outer: 5..20,
            },
            Token {
                token_type: TokenType::Markdown(MarkdownToken::String),
                inner: 10..15,
                outer: 10..15,
            },
        ]);

        // WHEN
        let actual = response.last_inner();

        // THEN
        assert_eq!(actual, 15usize);
    }

    #[test]
    fn test1674188496() {
        // GIVEN
        let response = Response::empty();

        // WHEN
        let actual = response.is_empty();

        // THEN
        assert_eq!(actual, true);
    }
}
