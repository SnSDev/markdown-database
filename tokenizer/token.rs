use crate::tokenizer::prelude::*;

/// TODO
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Token {
    /// TODO
    pub token_type: TokenType,
    /// TODO
    pub outer: Range<usize>,
    /// TODO
    pub inner: Range<usize>,
}

/// TODO
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Type {
    /// TODO
    Markdown(MarkdownToken),
    /// TODO
    Yaml(YamlToken),
}

/// TODO
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CharacterEscaped {
    /// TODO
    Backslash,
    /// TODO
    Backtick,
    /// TODO
    Asterisk,
    /// TODO
    Underscore,
    /// TODO
    OpenCurlyBrace,
    /// TODO
    CloseCurlyBrace,
    /// TODO
    OpenBracket,
    /// TODO
    CloseBracket,
    /// TODO
    OpenAngleBracket,
    /// TODO
    CloseAngleBracket,
    /// TODO
    OpenParentheses,
    /// TODO
    CloseParentheses,
    /// TODO
    Pound,
    /// TODO
    Plus,
    /// TODO
    Minus,
    /// TODO
    Period,
    /// TODO
    ExclamationPoint,
    /// TODO
    Pipe,
}

/// TODO
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum LinkType {
    /// TODO
    Url,
    /// TODO
    AbsolutePath,
    /// TODO
    RelativePath,
    /// TODO
    Email,
}

/// TODO
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TaskStatus {
    /// TODO
    UnMarked,
    /// TODO
    Marked,
}

/// TODO
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ColumnAlignment {
    /// TODO
    NotSpecified,
    /// TODO
    Left,
    /// TODO
    Center,
    /// TODO
    Right,
}
