---
Title: A Sample MultiMarkdown Database Document
Author: Matthew "Juniper" Barlett <emeraldinspirations+hppb640+u1904@gmail.com>
Date: November 2, 2021
Comment: This is a comment intended to demonstrate
  metadata that spans multiple lines, yet
  is treated as a single value.
CSS: http://example.com/standard.css
Data Type: Transaction, Automotive Maintenance, Oil Change
Tag: Good Service, Public
---

**key 1**
: Value 1\
**key 2**
: Value 2

## Transaction

**Transaction Specific Key**
: How to handle duplicate Field Names for different Data Types

## Embedded Document Title

```ledger
2021/07/01 Test
    Expenses:Hot Dogs                           $         15.00
    Assets:Cash
```

## Paragraph Data

This is data that is not queryable, but is searchable.

Titles are either the first element with Heading 1, or metadata field "Title"

A single document MAY have zero, one or multiple Data Type. If any Data Type is specified, the fields inside the Data Type are considered REQUIRED. If different Data Type have the same field AND the values are the same, only 1 key / value is required to represent both values.
