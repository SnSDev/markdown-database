//! Create markdown database object file based on current unix timestamp

/// Allow glob import of [`touch_event`] and support items
pub mod prelude {
    pub use super::{
        use_case as touch_event, Error as TouchEventError,
        RequestModel as TouchEventRequestModel,
        ResponseModel as TouchEventResponseModel, Result as TouchEventResult,
    };
    pub use crate::gateway::markdown_directory::MarkdownDirectory;
    pub use chrono::prelude::*;
    pub use chrono::Duration;
    pub use vfs::{VfsError, VfsPath, WalkDirIterator};
}

use prelude::*;

/// Failure state for [`touch_event`]
#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    /// Adding the year dir and index at the base dir `~/2024/index.md`
    UnableToTouchYear,

    /// Adding the month dir and index inside year `~/2024/01-jan/index.md`
    UnableToTouchMonth,

    /// Adding the week dir and index inside year `~/2024/week_01/index.md`
    UnableToTouchWeek,
}

/// Parameters for [`touch_event`]
pub struct RequestModel<'a> {
    /// Current time/date, injected for testing
    pub timestamp: DateTime<Local>,

    /// Persistent Storage Plugin
    pub gateway: &'a mut MarkdownDirectory,
}

/// Success state for [`touch_event`]
#[derive(Debug, PartialEq, Eq)]
pub struct ResponseModel {
    week_file: VfsPath,
}

impl From<VfsPath> for ResponseModel {
    fn from(week_file: VfsPath) -> Self {
        ResponseModel { week_file }
    }
}

/// Output from [`touch_event`]
pub type Result = std::result::Result<ResponseModel, Error>;

/// Create file for current week
///
/// See [`crate::touch_event_use_case`]
///
/// # Errors
///
/// File system errors can occur and are returned as [`Error`]
pub fn use_case(RequestModel { timestamp, gateway }: RequestModel) -> Result {
    gateway
        .touch_year(&timestamp)
        .map_err(|_| Error::UnableToTouchYear)?;
    gateway
        .touch_month(&timestamp)
        .map_err(|_| Error::UnableToTouchMonth)?;
    let week_file = gateway
        .touch_week(&timestamp)
        .map_err(|_| Error::UnableToTouchWeek)?;

    Ok(week_file.into())
}

#[cfg(test)]
mod tests {

    #[test]
    fn test1704100306_base() {
        use markdown_database_lib::touch_event_use_case::prelude::*;
        use vfs::MemoryFS;

        // GIVEN
        let dummy_timestamp = Local
            .from_local_datetime(
                &NaiveDate::from_ymd_opt(2024, 1, 1)
                    .unwrap()
                    .and_hms_opt(3, 21, 0)
                    .unwrap(),
            )
            .unwrap();
        let mut gateway: MarkdownDirectory =
            VfsPath::new(MemoryFS::new()).into();
        let request_model = TouchEventRequestModel {
            timestamp: dummy_timestamp, // 2023-12-25 19:54:06
            gateway: &mut gateway,      // Dummy Memory File System
        };

        // WHEN
        let actual = touch_event(request_model);

        // THEN
        assert!(actual.is_ok());
        let base_dir: VfsPath = gateway.into();
        assert!(base_dir
            .join("2024/week_01/index.md")
            .unwrap()
            .is_file()
            .unwrap());
        assert!(base_dir
            .join("2024/01-jan/index.md")
            .unwrap()
            .is_file()
            .unwrap());
        assert!(base_dir.join("2024/index.md").unwrap().is_file().unwrap());
    }
}
