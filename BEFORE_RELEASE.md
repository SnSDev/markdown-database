Before Release
==============

- Advance version in `Cargo.toml`
- Verify `ROADMAP.md` is up-to-date
- Compile list of changes, add to documentation (TBD)
- Make sure all `// TODO:` and `todo!{}` are either resolved or
  scheduled for a future release
- Run `cargo test`
  - Make sure no errors / warnings
- Run `cargo build --release --features="bin_markdown_database"`
  - Make sure no errors / warnings
- Run `cargo doc`
  - Make sure no errors / warnings
- Run `./update_documentation.sh`
- Commit changes and push
- Close respective issue and merge with `master` branch
- Checkout `master` branch, run `git pull`
- Create tag
- Digitally sign the tag (TBD)
- Push tag
