// #![warn(missing_docs)] // TODO
#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]
#![allow(clippy::no_effect_underscore_binding)] // TODO: Rocket macros trigger this
#![allow(unused)] // TODO: To cut down on warning noise

#[macro_use]
extern crate rocket;

use rocket::State;
use rocket::{
    fs::{FileServer, NamedFile},
    Build, Rocket,
};

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[launch]
#[must_use] pub fn rocket() -> _ {
    rocket::build().mount("/", routes![index])
}
