use super::*;

#[derive(Clone)]
pub struct MpscInterThreadSender<T>(mpsc::Sender<T>);

impl<T: Clone + Send> InterThreadSender<T> for MpscInterThreadSender<T> {
    fn send(&mut self, value: T) -> Result<(), InterThreadSenderError> {
        { self.0.send(value) }.map_err(|_| InterThreadSenderError::Unspecified)
    }
}
