use super::*;

pub struct ParsableMatch<'a> {
    pub check_for_match_closure: ParseMatchClosure<'a>,
    pub use_case_to_run: RunUseCaseClosure<'a>,
}
