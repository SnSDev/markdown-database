use clap::{crate_authors, crate_version, ArgMatches, Command};

pub trait Launch: Sized {
    fn sub_command_launch(self) -> Self {
        unimplemented!("1705816506")
    }

    fn matches_launch(self) -> Self {
        unimplemented!("1705816515")
    }
}

pub const SUB_COMMAND_NAME: &str = "launch";

pub const SHORT_DESCRIPTION: &str = "Run web host for database";

impl Launch for Command {
    fn sub_command_launch(self) -> Self {
        self.subcommand(
            Command::new(SUB_COMMAND_NAME)
                .about(SHORT_DESCRIPTION)
                .author(crate_authors!())
                .version(crate_version!()), // .long_about(LONG_DESCRIPTION)
                                            // .arg(
                                            //     Arg::new(ARG_FOR_INDEX)
                                            //         .help(ARG_FOR_INDEX_HELP)
                                            //         .long(ARG_FOR_INDEX),
                                            // ),
        )
    }
}

impl Launch for ArgMatches {
    fn matches_launch(self) -> Self {
        let Some(_matches) = self.subcommand_matches(SUB_COMMAND_NAME) else {
            return self;
        };

        markdown_database_rocket::main();

        self
    }
}
