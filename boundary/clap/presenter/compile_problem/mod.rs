//! Render [CompileProblem] for CLI
//!
//! Example (Manual Test)
//! =====================

mod display;
mod presenter;

pub use super::compile_problem as prelude;
pub use display::CompileProblemDisplay;
pub use presenter::CompileProblemPresenter;

pub use crate::presenter::prelude::*;

use crate::presenter::{
    colorizer::prelude::*, position_in_file::prelude::*,
    relative_file_path::prelude::*,
};
use colored::Colorize;
use markdown_database_lib::data_object::{CompileProblem, ProblemLevel};

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    #[ignore = "Manual Test"]
    #[test]
    fn test1632874073_command_line_output_manual_test() {
        use crate::presenter::compile_problem::prelude::*;
        use markdown_database_lib::data_object::{
            CompileProblem, PositionInFile, ProblemLevel,
        };
        use markdown_database_lib::wrapper::file_path::prelude::*;
        use markdown_database_lib::wrapper::FilePathParseRelativePath;

        let pos = PositionInFile::new(42, 24);
        let mock_file_system = MemoryFS::default();
        let root = FilePath::new(mock_file_system);
        let file_path = root.join("temp/problem/file.wherever").unwrap();
        let problem_error = CompileProblem {
            file_path: file_path.clone(),
            position_in_file: pos,
            message: "This is an example message".to_owned(),
            problem_level: ProblemLevel::Error,
        };
        let problem_warning = CompileProblem {
            file_path: file_path.clone(),
            position_in_file: pos,
            message: "This is an example message".to_owned(),
            problem_level: ProblemLevel::Warning,
        };
        let problem_info = CompileProblem {
            file_path,
            position_in_file: pos,
            message: "This is an example message".to_owned(),
            problem_level: ProblemLevel::Info,
        };

        let base_dir = root.parse_relative_path("/temp/divergent").unwrap();

        let presenter = CompileProblemPresenter {
            path_presenter: &RelativeFilePathPresenter {
                base_dir: &base_dir,
            },
        };

        eprintln!("{}", presenter.render(&problem_error));
        eprintln!("{}", presenter.render(&problem_warning));
        eprintln!("{}", presenter.render(&problem_info));
        eprintln!();

        assert_eq!(1, 2) // Forces test failure so results can be seen
    }
}
