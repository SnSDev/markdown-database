use super::*;

type Colorizer = ThemeMatchVscodeDarkPlus;

pub struct CompileProblemDisplay<'a> {
    pub settings: &'a CompileProblemPresenter<'a>,
    pub data: &'a CompileProblem,
}

impl<'a> Display for CompileProblemDisplay<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        let problem_level = match self.data.problem_level {
            ProblemLevel::Info => Colorizer::Info.apply("Info      "),
            ProblemLevel::Warning => Colorizer::Warning.apply("Warning   "),
            ProblemLevel::Error => Colorizer::Error.apply("Error     "),
        };

        let path = { self.settings.path_presenter }
            .render(&self.data.file_path)
            .to_string();

        let formatted_path = if let Some(index) = path.rfind('/') {
            let (dir, file_name) = path.split_at(index);
            Colorizer::StringLiteral.apply(&format!(
                "{}{}",
                dir,
                file_name.bold()
            ))
        } else {
            Colorizer::StringLiteral.apply(&path.bold().to_string())
        };

        let position_in_file_presenter = PositionInFilePresenter::default();
        let line = position_in_file_presenter
            .render(&self.data.position_in_file)
            .to_string();

        write!(
            fmt,
            "{}{}{}\n{}{}{}{}",
            problem_level,
            Colorizer::EntityName.apply(": "),
            Colorizer::TypeDeclaration.apply(&self.data.message),
            Colorizer::EntityName.apply("  --> "),
            formatted_path,
            Colorizer::EntityName.apply(":"),
            Colorizer::NumberLiteral.apply(&line),
        )
    }
}
