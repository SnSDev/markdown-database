//! Necessary imports to build a [Presenter]

pub use crate::presenter::presenter_trait::Presenter;
pub use std::fmt::{Display, Formatter};
