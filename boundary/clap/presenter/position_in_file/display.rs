use super::*;
pub struct PositionInFileDisplay<'a> {
    pub _settings: &'a PositionInFilePresenter,
    pub data: &'a PositionInFile,
}

impl<'a> Display for PositionInFileDisplay<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        write!(fmt, "{}:{}", self.data.line_number, self.data.character,)
    }
}
