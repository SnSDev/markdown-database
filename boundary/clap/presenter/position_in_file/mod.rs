//! Render [PositionInFile]

mod display;
mod presenter;
pub use super::position_in_file as prelude;

pub use display::PositionInFileDisplay;
pub use presenter::PositionInFilePresenter;

use markdown_database_lib::data_object::PositionInFile;

pub use crate::presenter::prelude::*;

#[cfg(test)]
mod test {

    #[test]
    fn test1632874827_render() {
        use crate::presenter::position_in_file::prelude::*;
        use markdown_database_lib::data_object::PositionInFile;

        let presenter = PositionInFilePresenter {};
        assert_eq!(
            "42:24",
            presenter.render(&PositionInFile::new(42, 24)).to_string()
        )
    }
}
