use super::*;

pub type ThemeMatchVscodeDarkPlus = TokenType;

impl Theme for ThemeMatchVscodeDarkPlus {
    fn css_color_for(&self) -> &str {
        match self {
            Self::FunctionDeclaration => "#DCDCAA", // Function declarations
            Self::TypeDeclaration => "#4EC9B0", // Types declaration and references
            Self::TypeDeclarationTS => "#4EC9B0", // Types declaration and references, TS grammar specific
            Self::ControlFlow => "#C586C0", // Control flow / Special keywords
            Self::VariableName => "#9CDCFE", // Variable and parameter name
            Self::ConstantAndEnum => "#4FC1FF", // Constants and enums
            Self::ObjectKeyTS => "#9CDCFE", // Object keys, TS grammar specific
            Self::CSSProperty => "#CE9178", // CSS property value
            Self::RgExGroup => "#CE9178",   // Regular expression groups
            Self::RegExConst => "#d16969", // constant.character.character-class.regexp
            Self::RegExKeyword => "#DCDCAA", // keyword.operator.or.regexp
            Self::RegExQuantifier => "#d7ba7d", // keyword.operator.quantifier.regexp
            Self::ConstChar => "#569cd6",       // constant.character
            Self::EscapeChar => "#d7ba7d",      // constant.character.escape
            Self::EntityName => "#C8C8C8",      // entity.name.label
            Self::NewOperator => "#C586C0",     // newOperator
            Self::StringLiteral => "#ce9178",   // stringLiteral
            Self::CustomLiteral => "#DCDCAA",   // customLiteral
            Self::NumberLiteral => "#b5cea8",   // numberLiteral
            Self::Error => "#d94545",
            Self::Warning => "b89600",
            Self::Info => "#286cba",
            Self::Success => "#008000",
            Self::AutoFix => "#00ff00",
            Self::FileNotFound => "#AA1818",
        }
    }
}
