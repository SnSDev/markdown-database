use super::*;

pub struct ResultFormatter<'a> {
    response_model: ParseFileResponseModel,
    contains_error: bool,
    file_path: FilePath,
    line_number_calculator: LineNumberCalculator,
    problem_presenter: CompileProblemPresenter<'a>,
    token_presenter: TokenPresenter,
    arena: typed_arena::Arena<TokenEnum>,
}

impl<'a> ResultFormatter<'a> {
    pub fn new(
        response_model: ParseFileResponseModel,
        file_path: FilePath,
        line_number_calculator: LineNumberCalculator,
        problem_presenter: CompileProblemPresenter<'a>,
        token_presenter: TokenPresenter,
    ) -> Self {
        Self {
            response_model,
            contains_error: false,
            file_path,
            line_number_calculator,
            problem_presenter,
            token_presenter,
            arena: typed_arena::Arena::new(),
        }
    }
}

impl<'a> Iterator for ResultFormatter<'a> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.response_model.by_ref().find_map(|token| {
            if token.is_problem_token() {
                self.contains_error = true;

                let p = ProblemTokenEnum::from(token).into_compile_problem(
                    &self.file_path,
                    &self.line_number_calculator,
                );
                eprintln!("{}", self.problem_presenter.render(&p));
                None
            } else {
                let token = self.arena.alloc(token);
                Some(self.token_presenter.render(token).to_string())
            }
        })
    }
}
