mod display;
mod presenter;

pub use super::token as prelude;

pub use display::DatumDisplay;
pub use presenter::DataPresenter;

use crate::presenter::colorizer::{Theme, ThemeMatchVscodeDarkPlus};
use markdown_database_lib::compiler::TokenEnum;

pub use crate::presenter::prelude::*;

type Colorizer = ThemeMatchVscodeDarkPlus;
