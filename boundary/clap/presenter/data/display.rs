use super::*;

pub struct DatumDisplay<'a> {
    pub _settings: &'a DataPresenter,
    pub meta: Option<&'a String>,
    pub namespace: Option<&'a String>,
    pub index: Option<&'a usize>,
    pub key: Option<&'a String>,
    pub value: &'a String,
}

impl<'a> Display for DatumDisplay<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(m) = self.meta {
            write!(
                fmt,
                "{}{}",
                Colorizer::NumberLiteral.apply(m),
                Colorizer::ControlFlow.apply(":")
            )?;
        }

        if let Some(n) = self.namespace {
            write!(
                fmt,
                "{}{}",
                Colorizer::TypeDeclaration.apply(n),
                if self.index.is_some() {
                    Colorizer::ControlFlow.apply("")
                } else if self.key.is_some() {
                    Colorizer::ControlFlow.apply("/")
                } else {
                    Colorizer::ControlFlow.apply(":")
                }
            )?;
        }

        if let Some(i) = self.index {
            write!(
                fmt,
                "{}{}{}",
                Colorizer::ControlFlow.apply("["),
                Colorizer::FunctionDeclaration.apply(&format!("{}", i)),
                if self.key.is_some() {
                    Colorizer::ControlFlow.apply("]/")
                } else {
                    Colorizer::ControlFlow.apply("]:")
                }
            )?;
        }

        if let Some(k) = &self.key {
            write!(
                fmt,
                "{}{}",
                Colorizer::ConstantAndEnum.apply(k),
                Colorizer::ControlFlow.apply(":")
            )?;
        }

        write!(fmt, "{}", Colorizer::StringLiteral.apply(self.value))
    }
}
