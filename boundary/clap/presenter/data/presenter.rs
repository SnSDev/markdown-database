use super::*;

pub struct DataPresenter {
    meta_default: Option<String>,
    meta_none: Option<String>,
    meta_title: Option<String>,
    // title_none: Option<String>,
    namespace_none: Option<String>,
    index_none: Option<usize>,
    key_none: Option<String>,
}

impl Default for DataPresenter {
    fn default() -> Self {
        Self {
            meta_default: Some("META".to_owned()),
            meta_none: None,
            meta_title: Some("Title".to_owned()),
            // title_none: None,
            namespace_none: None,
            index_none: None,
            key_none: None,
        }
    }
}

impl<'a> Presenter<'a> for DataPresenter {
    type Data = TokenEnum;
    type Displayable = DatumDisplay<'a>;

    fn render(&'a self, data: &'a Self::Data) -> Self::Displayable {
        match data {
            TokenEnum::Datum(t) => DatumDisplay {
                _settings: self,
                meta: self.meta_none.as_ref(),
                namespace: t.namespace.as_ref(),
                index: t.index.as_ref(),
                key: t.key.as_ref(),
                value: &t.value,
            },
            TokenEnum::Title(t) => DatumDisplay {
                _settings: self,
                meta: self.meta_title.as_ref(),
                namespace: self.namespace_none.as_ref(),
                index: self.index_none.as_ref(),
                key: self.key_none.as_ref(),
                value: &t.title,
            },
            TokenEnum::YamlKeyValuePair(t) => DatumDisplay {
                _settings: self,
                meta: self.meta_default.as_ref(),
                namespace: self.namespace_none.as_ref(),
                index: self.index_none.as_ref(),
                key: Some(&t.key),
                value: &t.value,
            },
            _ => unreachable!("1638755442 - Token type not supported"),
        }
    }
}
