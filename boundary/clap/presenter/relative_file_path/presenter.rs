use super::*;

/// Format file path relative to [base_dir]
///
pub struct RelativeFilePathPresenter<'a> {
    pub base_dir: &'a FilePath,
}

impl<'a> Presenter<'a> for RelativeFilePathPresenter<'a> {
    type Data = FilePath;
    type Displayable = RelativeFilePathDisplay<'a>;

    fn render(&'a self, data: &'a Self::Data) -> Self::Displayable {
        RelativeFilePathDisplay {
            settings: self,
            data,
        }
    }
}
