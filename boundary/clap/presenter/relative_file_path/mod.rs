//! Render a [FilePath] as a relative path to a specified directory

mod display;
mod presenter;
pub use super::relative_file_path as prelude;

pub use display::RelativeFilePathDisplay;
pub use presenter::RelativeFilePathPresenter;

pub use crate::presenter::prelude::*;

use markdown_database_lib::wrapper::file_path::prelude::*;
