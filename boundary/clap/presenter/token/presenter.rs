use super::*;

#[derive(Default)]
pub struct TokenPresenter {}

impl<'a> Presenter<'a> for TokenPresenter {
    type Data = TokenEnum;
    type Displayable = TokenDisplay<'a>;

    fn render(&'a self, data: &'a Self::Data) -> Self::Displayable {
        TokenDisplay {
            _settings: self,
            data,
        }
    }
}
