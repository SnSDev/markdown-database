use super::*;

pub struct TokenDisplay<'a> {
    pub _settings: &'a TokenPresenter,
    pub data: &'a TokenEnum,
}

impl<'a> Display for TokenDisplay<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        let name = to_variant_name(&self.data).expect(
            "1637116362 - Unreachable: Enum implements serde::Serialize",
        );
        fmt.write_str(name)
    }
}
