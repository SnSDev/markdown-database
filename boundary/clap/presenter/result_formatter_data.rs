use super::*;

pub struct ResultFormatter {
    response_model: ParseDataResponseModel,
    datum_presenter: DataPresenter,
    arena: typed_arena::Arena<TokenEnum>,
}

impl ResultFormatter {
    pub fn new(
        response_model: ParseDataResponseModel,
        datum_presenter: DataPresenter,
    ) -> Self {
        Self {
            response_model,
            datum_presenter,
            arena: typed_arena::Arena::new(),
        }
    }
}

impl Iterator for ResultFormatter {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        let t = self.response_model.next()?;
        let token = self.arena.alloc(t);
        Some(self.datum_presenter.render(token).to_string())
    }
}
