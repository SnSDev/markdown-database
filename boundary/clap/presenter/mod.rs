//! Conversion code from `data_object` to formatted output
//!
//! Each `Presenter` consists of 2 structs:
//! - "[DataObject]Presenter" - the struct implementing
//!   [presenter_trait::Presenter::render], and containing the settings
//!   needed for rendering
//! - "[DataObject]Display" - the struct implementing [std::fmt::Display], and
//!   containing a reference to the `Data Object` being rendered and
//!   "[DataObject]Presenter"
//!
//! Example
//! =======
//!
//! [project_home]/presenter/demo/display.rs
//! ------------------------------------------
//!
//! ```ignore
//! use super::*;
//!
//! pub struct DemoDisplay<'a> {
//!     pub settings: &'a DemoPresenter<'a>,
//!     pub data: &'a DemoData,
//! }
//!
//! impl<'a> Display for DemoDisplay<'a> {
//!     fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
//!         // This is the core of the `presenter`
//!         write!(fmt, "Demo Data {} of {}", self.data.x, self.data.y)
//!     }
//! }
//! ```
//!
//! [project_home]/presenter/demo/mod.rs
//! ------------------------------------
//!
//! ```ignore
//! //! Demo `presenter` documentation
//!
//! mod display;
//! mod presenter;
//!
//! pub use super::demo as prelude;
//! pub use display::DemoDisplay;
//! pub use presenter::DemoPresenter;
//!
//! use crate::data_object::DemoData;
//!
//! pub use crate::presenter::prelude::*;
//! ```
//!
//! [project_name]/presenter/demo/prelude.rs
//! ----------------------------------------
//!
//! ```ignore
//! pub use super::{DemoDisplay, DemoPresenter};
//! ```
//!
//! [project_home]/presenter/demo/presenter.rs
//! ------------------------------------------
//!
//! ```ignore
//! pub struct DemoPresenter<'a> {
//!     pub setting1: &'a bool,
//! }
//!
//! impl<'a> Presenter<'a> for DemoPresenter<'a> {
//!     type Data = DemoData;
//!     type Displayable = DemoDisplay<'a>;
//!
//!     fn render(&'a self, data: &'a Self::Data) -> Self::Displayable {
//!         DemoDisplay {
//!             settings: self,
//!             data,
//!         }
//!     }
//! }
//! ```

pub mod colorizer;
pub mod compile_problem;
pub mod data;
pub mod position_in_file;
pub mod prelude;
mod presenter_trait;
pub mod relative_file_path;
pub mod token;
pub mod url;
