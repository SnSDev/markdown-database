//! Render a [Url]
//!
//! Example
//! =======
//!
//! ```
//! use markdown_database_lib::presenter::url::prelude::*;
//! use url::Url;
//! use std::str::FromStr;
//!
//! const TEST_URL_STR: &str = "http://www.example.com/1630210330";
//! let test_url = Url::from_str(TEST_URL_STR).unwrap();
//! let url_presenter = UrlPresenter::default();
//!
//! assert_eq!(
//!     TEST_URL_STR.to_string(),
//!     url_presenter.render(&test_url).to_string()
//! )
//! ```

mod display;
mod presenter;
pub use super::url as prelude;

pub use display::UrlDisplay;
pub use presenter::UrlPresenter;

pub use crate::presenter::prelude::*;

use url::Url;
