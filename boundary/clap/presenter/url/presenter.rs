use super::*;

#[derive(Default)]
pub struct UrlPresenter {}

impl<'a> Presenter<'a> for UrlPresenter {
    type Data = Url;
    type Displayable = UrlDisplay<'a>;

    fn render(&'a self, data: &'a Self::Data) -> Self::Displayable {
        UrlDisplay { data }
    }
}
