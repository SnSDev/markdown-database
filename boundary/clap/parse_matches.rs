use super::*;

pub fn parse_matches(
    matches: &ArgMatches<'static>,
) -> Option<RunUseCaseClosure<'static>> {
    MATCH_ARRAY.iter().find_map(|parsable_match| {
        (parsable_match.check_for_match_closure)(matches)
            .then_some(parsable_match.use_case_to_run)
    })
}
