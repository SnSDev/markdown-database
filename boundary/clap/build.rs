use std::process::Command;

fn main() {
    let git_commit_describe =
        run_command(Command::new("git").args(["describe", "--tags"]));

    println!("cargo:rustc-env=GIT_COMMIT_DESCRIBE={git_commit_describe}");
}

// Code from vergen v3.1.0
fn run_command(command: &mut Command) -> String {
    if let Ok(o) = command.output() {
        if o.status.success() {
            return String::from_utf8_lossy(&o.stdout).trim().to_owned();
        }
    }
    "UNKNOWN".to_owned()
}
