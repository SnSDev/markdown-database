mod launch_subcommand;
mod sub_command;
mod touch_sub_command;

use clap::{
    crate_authors, crate_description, crate_name, ColorChoice, Command,
};
use launch_subcommand::Launch as _;
use sub_command::prelude::*;
use touch_sub_command::Touch;
use vfs::{PhysicalFS, VfsPath};

fn main() -> std::io::Result<()> {
    let file_system: VfsPath = PhysicalFS::new("/").into();
    let current_dir = std::env::current_dir()
        .expect("1652606287 - Unreachable: Issue loading current directory");
    let base_dir = file_system
        .join(current_dir.to_string_lossy())
        .expect("1703597961 - Unable to reference current directory");

    println!("{}", base_dir.as_str());

    // TODO: -> Main Return
    let app = Command::new(crate_name!())
        .author(crate_authors!())
        .about(crate_description!())
        .sub_command_list()
        .sub_command_touch()
        .sub_command_launch()
        .styles(get_styles())
        .color(ColorChoice::Auto)
        .version(env!("GIT_COMMIT_DESCRIBE"));

    app.get_matches()
        // .matches_list(todo!("Gimme VfsPath")) // TODO
        .matches_touch(&base_dir)
        .matches_launch()
        .matches_default();

    Ok(())
}

pub fn get_styles() -> clap::builder::Styles {
    clap::builder::Styles::styled()
        .usage(
            anstyle::Style::new().bold().underline().fg_color(Some(
                anstyle::Color::Ansi(anstyle::AnsiColor::Yellow),
            )),
        )
        .header(
            anstyle::Style::new().bold().underline().fg_color(Some(
                anstyle::Color::Ansi(anstyle::AnsiColor::Yellow),
            )),
        )
        .literal(
            anstyle::Style::new().fg_color(Some(anstyle::Color::Ansi(
                anstyle::AnsiColor::Green,
            ))),
        )
        .invalid(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Red))),
        )
        .error(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Red))),
        )
        .valid(
            anstyle::Style::new().bold().underline().fg_color(Some(
                anstyle::Color::Ansi(anstyle::AnsiColor::Green),
            )),
        )
        .placeholder(
            anstyle::Style::new().fg_color(Some(anstyle::Color::Ansi(
                anstyle::AnsiColor::White,
            ))),
        )
}
