use super::*;

pub fn build_clap_app<'a, 'b>() -> App<'a, 'b> {
    let mut app = App::new(crate_name!())
        .settings(&[AppSettings::ColorAlways, AppSettings::ColoredHelp])
        .author(crate_authors!())
        .about(crate_description!())
        .version(env!("GIT_COMMIT_DESCRIBE"));

    app = SUB_COMMAND_ARRAY
        .iter()
        .fold(app, |app, add_sub_command_closure| {
            app.subcommand(add_sub_command_closure())
        });

    app
}
