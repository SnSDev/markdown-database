pub use markdown_database_lib::wrapper::file_path::prelude::*;
pub use std::path::{Path, PathBuf};

pub fn root_path() -> FilePath {
    FilePath::new(PhysicalFS::new(Path::new("/").into()))
}

pub fn current_dir() -> PathBuf {
    std::env::current_dir().expect("1638998539 - Current Directory Not Found")
}

pub fn base_dir() -> FilePath {
    let current_dir = current_dir();
    match current_dir.to_string_lossy().as_ref() {
        "/" => root_path(),
        current_dir => root_path()
            .join(current_dir.strip_prefix('/').expect(
                "1638998894 - Unreachable: Previous match handles root case",
            ))
            .expect(
                "1638998897 - Unreachable: std::env::current_dir is always a \
                 valid path",
            ),
    }
}
