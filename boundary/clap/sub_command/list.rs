use clap::{crate_authors, crate_version, Arg, ArgMatches, Command};
use vfs::VfsPath;

pub trait List: Sized {
    fn sub_command_list(self) -> Self {
        unimplemented!("1673512170")
    }

    fn matches_list(self, _base_dir: &VfsPath) -> Self {
        unimplemented!("1673512199")
    }
}

pub const SUB_COMMAND_NAME: &str = "list";
pub const ARG_FOR_INDEX: &str = "for-index";

pub const LONG_DESCRIPTION: &str = r#"
Recursively list all files/folders in the current directory

Paths are relative to the current directory.

Use `--for-index` flag to return only `index.md` files inside all the first-
level child directories (if any), followed by markdown files inside the current
directory.   This is used in the `Index` sub-command.

Currently the only files/folders filtered out are:
- Any starting with ".git"
"#;

pub const SHORT_DESCRIPTION: &str =
    "Recursively list all files/folders in the current directory";

pub const ARG_FOR_INDEX_HELP: &str =
    "Lists files that would be indexed using `INDEX` command";

impl List for Command {
    fn sub_command_list(self) -> Self {
        self.subcommand(
            Command::new(SUB_COMMAND_NAME)
                .about(SHORT_DESCRIPTION)
                .author(crate_authors!())
                .version(crate_version!())
                .long_about(LONG_DESCRIPTION)
                .arg(
                    Arg::new(ARG_FOR_INDEX)
                        .help(ARG_FOR_INDEX_HELP)
                        .long(ARG_FOR_INDEX),
                ),
        )
    }
}

impl List for ArgMatches {
    fn matches_list(self, _base_dir: &VfsPath) -> Self {
        let Some(_matches) = self.subcommand_matches(SUB_COMMAND_NAME) else {
            return self;
        };

        // let for_index = matches.contains_id(ARG_FOR_INDEX);

        // let request_model = ListFilesRequestModel {
        //     base_dir: base_dir.clone(),
        //     for_index,
        // };

        // let presenter = RelativeFilePathPresenter {
        //     base_dir: &base_dir,
        // };

        // list_files(request_model)
        //     .map(|unix_absolute_path| presenter.render(unix_absolute_path))
        //     .pipe_out()
        //     .ok();

        // TODO !("1673742389");

        self
    }
}
