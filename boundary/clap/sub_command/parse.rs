pub mod result_formatter;
pub mod sub_command;

use crate::presenter::{
    compile_problem::prelude::*, relative_file_path::prelude::*,
    token::prelude::*,
};
use crate::sub_command_prelude::*;
use markdown_database_lib::compiler::TokenEnum;
use markdown_database_lib::entity::LineNumberCalculator;
use markdown_database_lib::parse_file_use_case::*;
use markdown_database_lib::wrapper::file_path::prelude::*;
use result_formatter::ResultFormatter;
use std::io::Error;
use std::path::Path;
use stdio_iterators::pipe_out;

pub use sub_command::{build_sub_command, PARSABLE_MATCH};

pub const SHORT_DESCRIPTION: &str = "Parse the elements in a markdown file";

pub const LONG_DESCRIPTION: &str = r#"
Parse all the links, and other elements in the specified markdown file

Paths are relative to the current directory.
"#;

pub const COMMAND_NAME: &str = "parse";

use super::*;

const ARG_FILE_PATH: &str = "FILE_PATH";

pub fn build_sub_command<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(COMMAND_NAME)
        .about(SHORT_DESCRIPTION)
        .author(crate_authors!())
        .version(crate_version!())
        .long_about(LONG_DESCRIPTION)
        .arg(
            Arg::with_name(ARG_FILE_PATH)
                .help("Path to markdown file")
                .required(true)
                .index(1),
        )
}

pub const PARSABLE_MATCH: ParsableMatch = ParsableMatch {
    check_for_match_closure: |matches| -> bool {
        matches.is_present(COMMAND_NAME)
    },
    use_case_to_run: |matches| -> MainReturn {
        let root = FilePath::new(PhysicalFS::new(Path::new("/").into()));
        let current_dir = std::env::current_dir()
            .expect("1632029064 - Current Directory Not Found");

        let base_dir = match current_dir.to_string_lossy().as_ref() {
            "/" => root,
            current_dir => root
                .join(current_dir.strip_prefix('/').expect(
                    "1632525771 - Unreachable: Previous match handles root \
                     case",
                ))
                .expect(
                    "1632453633 - Unreachable: std::env::current_dir is \
                     always a valid path",
                ),
        };

        let matches = matches
            .subcommand_matches(COMMAND_NAME)
            .expect("1630298058 - Unreachable! Required Parameters Missing");

        let file_path = match base_dir.parse_relative_path(
            matches
                .value_of(ARG_FILE_PATH)
                .expect("1630298284 - Unreachable! Required Parameter Missing"),
        ) {
            Ok(p) => p,
            Err(VfsError::InvalidPath { path }) => {
                // TODO: FilePath Error thrown funny
                // ```
                // Error: Custom { kind: Other, error: "WithContext { context: \"Could not get metadata for '/home/Dropbox/Pim/index.md'\", cause: IoError(Os { code: 2, kind: NotFound, message: \"No such file or directory\" }) }" }
                //
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidInput,
                    format!("1632526180 - Invalid path {:?}", path),
                ));
            }
            Err(other) => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("1632526167 - {:?}", other),
                ))
            }
        };
        let markdown_file_contents =
            file_path.read_to_string().map_err(|err| {
                // TODO: Verify correct error handler
                Error::new(std::io::ErrorKind::Other, format!("{:?}", err))
            })?;
        let line_number_calculator =
            LineNumberCalculator::from(markdown_file_contents.as_str());
        let request_model = ParseFileRequestModel {
            file_contents: markdown_file_contents,
            file_path: file_path.clone(),
        };

        //--------------------------------------------------------------

        let response_model: ParseFileResponseModel = parse_file(request_model)
            .map_err(|error| {
                eprintln!("{:#?}", error);
                std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "1628034091 - Failed to parse-urls in file",
                )
            })?;

        //--------------------------------------------------------------

        let relative_path_presenter = RelativeFilePathPresenter {
            base_dir: &base_dir,
        };
        let problem_presenter = CompileProblemPresenter {
            path_presenter: &relative_path_presenter,
        };
        let token_presenter = TokenPresenter::default();

        // let arena_url = typed_arena::Arena::new();
        // let arena_path = typed_arena::Arena::new();

        //--------------------------------------------------------------

        let result_formatter = ResultFormatter::new(
            response_model,
            file_path,
            line_number_calculator,
            problem_presenter,
            token_presenter,
        );
        let pipe_results = pipe_out(result_formatter);

        //--------------------------------------------------------------

        match pipe_results {
            Ok(true) => Ok(()),
            Ok(false) => Err(std::io::Error::new(
                std::io::ErrorKind::BrokenPipe,
                "1628034295 - Broken Pipe",
            )),
            Err(err) => {
                Err(std::io::Error::new(std::io::ErrorKind::Other, err))
            }
        }
    },
};
