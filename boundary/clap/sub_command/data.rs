pub mod result_formatter;
pub mod sub_command;

use crate::presenter::{compile_problem::prelude::*, data::DataPresenter};
use crate::sub_command_prelude::*;
use markdown_database_lib::compiler::TokenEnum;
use markdown_database_lib::parse_data_use_case::*;
use markdown_database_lib::wrapper::file_path::prelude::*;
use result_formatter::ResultFormatter;
use std::io::Error as IoError;
use std::path::Path;
use stdio_iterators::pipe_out;

pub use sub_command::{build_sub_command, PARSABLE_MATCH};

use super::*;

const ARG_FILE_PATH: &str = "FILE_PATH";

pub fn build_sub_command<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(COMMAND_NAME)
        .about(SHORT_DESCRIPTION)
        .author(crate_authors!())
        .version(crate_version!())
        .long_about(LONG_DESCRIPTION)
        .arg(
            Arg::with_name(ARG_FILE_PATH)
                .help("Path to markdown file")
                .required(true)
                .index(1),
        )
}

pub const PARSABLE_MATCH: ParsableMatch = ParsableMatch {
    check_for_match_closure: |matches| -> bool {
        matches.is_present(COMMAND_NAME)
    },
    use_case_to_run: |matches| -> MainReturn {
        let root = FilePath::new(PhysicalFS::new(Path::new("/").into()));
        let current_dir = std::env::current_dir()
            .expect("1638573492 - Current Directory Not Found");

        let base_dir = match current_dir.to_string_lossy().as_ref() {
            "/" => root,
            current_dir => root
                .join(current_dir.strip_prefix('/').expect(
                    "1638573496 - Unreachable: Previous match handles root \
                     case",
                ))
                .expect(
                    "1638573500 - Unreachable: std::env::current_dir is \
                     always a valid path",
                ),
        };

        let arg_path = matches
            .subcommand_matches(COMMAND_NAME)
            .and_then(|matches| matches.value_of(ARG_FILE_PATH))
            .expect("1638573504 - Unreachable! Required Parameters Missing");

        let file_path =
            base_dir.parse_relative_path(arg_path).map_err(|e| {
                if let VfsError::InvalidPath { path } = e {
                    // TODO: FilePath Error thrown funny
                    // ```
                    // Error: Custom { kind: Other, error: "WithContext { context: \"Could not get metadata for '/home/Dropbox/Pim/index.md'\", cause: IoError(Os { code: 2, kind: NotFound, message: \"No such file or directory\" }) }" }
                    //
                    IoError::new(
                        std::io::ErrorKind::InvalidInput,
                        format!("1638573512 - Invalid path {:?}", path),
                    )
                } else {
                    IoError::new(
                        std::io::ErrorKind::Other,
                        format!("1638573517 - {:?}", other),
                    )
                }
            })?;
        let markdown_file_contents =
            file_path.read_to_string().map_err(|err| {
                // TODO: Verify correct error handler
                IoError::new(std::io::ErrorKind::Other, format!("{:?}", err))
            })?;
        let request_model = ParseDataRequestModel {
            file_contents: markdown_file_contents,
            file_path,
        };

        //--------------------------------------------------------------

        let response_model: ParseDataResponseModel = parse_data(&request_model)
            .map_err(|error| {
                eprintln!("{:#?}", error);
                IoError::new(
                    std::io::ErrorKind::InvalidData,
                    "1638573523 - Failed to parse-urls in file",
                )
            })?;

        //--------------------------------------------------------------

        let datum_presenter = DataPresenter::default();

        //--------------------------------------------------------------

        let result_formatter =
            ResultFormatter::new(response_model, datum_presenter);
        let pipe_results = pipe_out(result_formatter);

        //--------------------------------------------------------------

        match pipe_results {
            Ok(true) => Ok(()),
            Ok(false) => Err(IoError::new(
                std::io::ErrorKind::BrokenPipe,
                "1638573530 - Broken Pipe",
            )),
            Err(err) => Err(IoError::new(std::io::ErrorKind::Other, err)),
        }
    },
};
