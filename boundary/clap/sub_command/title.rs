use super::*;

mod std_io_stepper;
mod sub_command;

use crate::file_system::*;
use crate::presenter::{
    compile_problem::CompileProblemPresenter,
    relative_file_path::RelativeFilePathPresenter, token::Presenter,
};
use markdown_database_lib::compiler::ProblemTokenEnum;
use markdown_database_lib::entity::LineNumberCalculator;
use markdown_database_lib::parse_file_use_case::*;
use markdown_database_lib::parse_title_use_case::*;
use std::io::Error;
use stdio_iterators::{pipe_out, PipeIn};

pub use std_io_stepper::StdIOStepper;
pub use sub_command::{build_sub_command, PARSABLE_MATCH};

const SHORT_DESCRIPTION: &str = "Retrieve / Set Title of a markdown file";

const LONG_DESCRIPTION: &str = r#"
Retrieve / Set Title of a markdown file

The title is either
- The "title" value in the YAML Front Matter Block -OR-
- The first-level header inside the content

If --set is specified, the YAML Front Matter Block is updated with the specified
title.  If one exists in the aforementioned spots, it is removed.
"#;

const COMMAND_NAME: &str = "title";
const ARG_FILE_PATH: &str = "FILE_PATH";
const ARG_AS_LINK_NAME: &str = "as-link";
const ARG_AS_LINK_SHORT: &str = "l";
const ARG_AS_LINK_HELP: &str = "Output markdown links vs title";

use markdown_database_lib::compiler::markdown_link::MarkdownLinkLocalFileToken;

use super::*;

pub fn build_sub_command<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(COMMAND_NAME)
        .about(SHORT_DESCRIPTION)
        .author(crate_authors!())
        .version(crate_version!())
        .long_about(LONG_DESCRIPTION)
        .arg(
            Arg::with_name(ARG_FILE_PATH)
                .help("Path to markdown file, will use STDIN if omitted")
                .index(1),
        )
        .arg(
            Arg::with_name(ARG_AS_LINK_NAME)
                .help(ARG_AS_LINK_HELP)
                .long(ARG_AS_LINK_NAME)
                .short(ARG_AS_LINK_SHORT),
        )
}

pub const PARSABLE_MATCH: ParsableMatch = ParsableMatch {
    check_for_match_closure: |matches| -> bool {
        matches.is_present(COMMAND_NAME)
    },
    use_case_to_run: |matches| -> MainReturn {
        let base_dir = base_dir();

        let matches = matches
            .subcommand_matches(COMMAND_NAME)
            .expect("1630298058 - Unreachable! Required Parameters Missing");

        let as_link = matches.is_present(ARG_AS_LINK_NAME);

        let iterator = match matches.value_of(ARG_FILE_PATH) {
            Some(arg) => StdIOStepper::SingleValue(arg.to_owned()),
            None => StdIOStepper::PipeIn(PipeIn::default()),
        };

        for arg in iterator {
            let file_path = match base_dir.parse_relative_path(&arg) {
                Ok(p) => p,
                Err(VfsError::InvalidPath { path }) => {
                    // TODO: FilePath Error thrown funny
                    // ```
                    // Error: Custom { kind: Other, error: "WithContext { context: \"Could not get metadata for '/home/Dropbox/Pim/index.md'\", cause: IoError(Os { code: 2, kind: NotFound, message: \"No such file or directory\" }) }" }
                    //
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidInput,
                        format!("1632526180 - Invalid path {:?}", path),
                    ));
                }
                Err(other) => {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        format!("1632526167 - {:?}", other),
                    ))
                }
            };

            let file_contents = file_path.read_to_string().map_err(|err| {
                // TODO: Verify correct error handler
                Error::new(std::io::ErrorKind::Other, format!("{:?}", err))
            })?;

            let line_number_calculator =
                LineNumberCalculator::from(file_contents.as_str());

            let token_iterator = parse_file(ParseFileRequestModel {
                file_contents,
                file_path: file_path.clone(),
            })
            .map_err(|err| {
                Error::new(
                    std::io::ErrorKind::Other,
                    format!("1639857797 - Unable to parse file: {:?}", err),
                )
            })?;

            let relative_path_presenter = RelativeFilePathPresenter {
                base_dir: &base_dir,
            };

            let problem_presenter = CompileProblemPresenter {
                path_presenter: &relative_path_presenter,
            };

            match parse_title(ParseTitleRequestModel { token_iterator }) {
                Ok(r) if as_link => println!(
                    "{}",
                    MarkdownLinkLocalFileToken {
                        file_path,
                        caption: Some(r.title)
                    }
                    .into_markdown_string(&relative_path_presenter)
                ),
                Ok(r) => println!("{}", r.title),
                Err(ParseTitleError::TitleNotFound(t)) => {
                    eprintln!(
                        "{}",
                        problem_presenter.render(
                            &ProblemTokenEnum::from(t).into_compile_problem(
                                &file_path,
                                &line_number_calculator,
                            )
                        )
                    );
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "1639000469 - Unable to locate Title",
                    ));
                }
                Err(ParseTitleError::TitleParserNotDetected) => {
                    unreachable!("1639857984 - TitleParser Hard-Coded")
                }
                Err(ParseTitleError::UnknownError) => {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "1639000395 - Unanticipated Issue",
                    ));
                }
            };
        }

        Ok(())
    },
};
