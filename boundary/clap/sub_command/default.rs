use clap::ArgMatches;

pub trait Default: Sized {
    fn matches_default(self) -> Self;
}

impl Default for ArgMatches {
    fn matches_default(self) -> Self {
        self
    }
}
