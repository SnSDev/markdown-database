mod sub_command;

pub use sub_command::{build_sub_command, PARSABLE_MATCH};

use crate::presenter::{
    colorizer::{Theme, ThemeMatchVscodeDarkPlus},
    compile_problem::CompileProblemPresenter,
    prelude::Presenter,
    relative_file_path::RelativeFilePathPresenter,
    url::UrlPresenter,
};
use crate::sub_command_prelude::*;
use markdown_database_lib::crawl_database_files_use_case::prelude::*;
use markdown_database_lib::entity::SpiderMessage;
use markdown_database_lib::wrapper::file_path::prelude::*;
use std::{
    io::{Error, ErrorKind},
    sync::mpsc,
};

type Colorizer = ThemeMatchVscodeDarkPlus;

use markdown_database_lib::{
    entity::FilePathCrossReferencer,
    list_files_use_case::{list_files, ListFilesRequestModel},
    wrapper::FilePath,
};

use super::*;

pub const ARG_VERBOSE_NAME: &str = "verbose";
pub const ARG_VERBOSE_SHORT: &str = "v";
pub const ARG_VERBOSE_HELP: &str =
    "Display success messages (Default: Error Only)";

pub fn build_sub_command<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(COMMAND_NAME)
        .about(SHORT_DESCRIPTION)
        .author(crate_authors!())
        .version(crate_version!())
        .long_about(LONG_DESCRIPTION)
        .arg(
            Arg::with_name(ARG_VERBOSE_NAME)
                .short(ARG_VERBOSE_SHORT)
                .long(ARG_VERBOSE_NAME)
                .help(ARG_VERBOSE_HELP),
        )
}

pub const PARSABLE_MATCH: ParsableMatch = ParsableMatch {
    check_for_match_closure: |matches| -> bool {
        matches.is_present(COMMAND_NAME)
    },
    use_case_to_run: |matches| -> MainReturn {
        let base_dir: FilePath =
            PhysicalFS::new(std::env::current_dir().unwrap()).into();

        let is_verbose = matches
            .subcommand_matches(COMMAND_NAME)
            .expect("1638493522 - Unreachable: Sub-Command already matched")
            .is_present(ARG_VERBOSE_NAME);

        let (tx, rx) = mpsc::channel();

        let request_model = CrawlDatabaseFilesRequestModel {
            real_time_output: tx,
            base_dir: base_dir.clone(),
        };

        let local_file_presenter = RelativeFilePathPresenter {
            base_dir: &base_dir,
        };

        let compile_problem_presenter = CompileProblemPresenter {
            path_presenter: &local_file_presenter,
        };

        let url_presenter = UrlPresenter {};

        let mut file_cross_referencer = FilePathCrossReferencer::default();

        // I find this preferable to the map trick
        let response_model = list_files(ListFilesRequestModel {
            base_dir: base_dir.clone(),
            for_index: false,
        })
        .expect("1638498915 - Path already verified");
        file_cross_referencer.populate(response_model.map(FilePath::from));

        let result = std::thread::spawn(|| crawl_database_files(request_model));

        for sig in rx {
            match sig {
                SpiderMessage::CompileProblem(problem) => {
                    eprintln!("{}", compile_problem_presenter.render(&problem))
                }
                SpiderMessage::IoError(text) => {
                    eprintln!(
                        "{} {:?}",
                        Colorizer::Error.apply("Unexpected I/O Error"),
                        text
                    )
                }
                SpiderMessage::FileNotFound(file) => {
                    println!(
                        "{}: {}",
                        Colorizer::FileNotFound.apply("Not Found "),
                        Colorizer::StringLiteral.apply(
                            &local_file_presenter.render(&file).to_string()
                        ),
                    )
                }
                SpiderMessage::Success(file) if is_verbose => {
                    let file: FilePath = file.into();
                    file_cross_referencer.cross_reference(&file);
                    let file: FilePath = file.into();

                    println!(
                        "{}: {}",
                        Colorizer::Success.apply("Parsed    "),
                        Colorizer::StringLiteral.apply(
                            &local_file_presenter.render(&file).to_string()
                        ),
                    )
                }
                SpiderMessage::Url(url) if is_verbose => {
                    println!(
                        "{}: {}",
                        Colorizer::Info.apply("URL Skip  "),
                        Colorizer::FunctionDeclaration
                            .apply(&url_presenter.render(&url).to_string()),
                    )
                }
                SpiderMessage::FileExists(path) if is_verbose => {
                    println!(
                        "{}: {}",
                        Colorizer::Success.apply("Exists    "),
                        local_file_presenter.render(&path),
                    )
                }
                _ => (), // Ignore
            }
        }

        file_cross_referencer.into_iter().for_each(|file| {
            println!(
                "{}: {}",
                Colorizer::FileNotFound.apply("File NoRef"),
                Colorizer::StringLiteral.apply(
                    &local_file_presenter.render(&file.into()).to_string()
                ),
            )
        });

        result.join().map(|_| ()).map_err(|_| {
            Error::new(ErrorKind::Other, "1633478981 - Unknown error")
        })
    },
};
