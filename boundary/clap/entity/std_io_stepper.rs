use super::*;

pub enum StdIOStepper<'a> {
    PipeIn(PipeIn<'a>),
    SingleValue(String),
    Empty,
}

impl<'a> Iterator for StdIOStepper<'a> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        if let Self::PipeIn(mut p) = self {
            return p.next()?.as_str().to_owned();
        }
        match std::mem::replace(self, StdIOStepper::Empty) {
            Self::SingleValue(s) => Some(s),
            _ => None,
        }
    }
}

impl<'a> From<PipeIn<'a>> for StdIOStepper<'a> {
    fn from(pipe_in_iterator: PipeIn<'a>) -> Self {
        Self::PipeIn(pipe_in_iterator)
    }
}
