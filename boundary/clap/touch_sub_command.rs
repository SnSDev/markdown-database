use clap::{crate_authors, crate_version, ArgMatches, Command};
use markdown_database_lib::directory_schema::nested_epoch_timestamp::prelude::*;

use vfs::VfsPath;

pub trait Touch: Sized {
    fn sub_command_touch(self) -> Self {
        unimplemented!("1673512170")
    }

    fn matches_touch(self, _base_dir: &VfsPath) -> Self {
        unimplemented!("1673512199")
    }
}

pub const SUB_COMMAND_NAME: &str = "touch";
// pub const ARG_DOCUMENT_TYPE: &str = "document-type";

// pub const LONG_DESCRIPTION: &str = r#"
// "#;

pub const SHORT_DESCRIPTION: &str =
    "Create object document based on current unix timestamp";

// pub const ARG_FOR_INDEX_HELP: &str =
//     "Lists files that would be indexed using `INDEX` command";

impl Touch for Command {
    fn sub_command_touch(self) -> Self {
        self.subcommand(
            Command::new(SUB_COMMAND_NAME)
                .about(SHORT_DESCRIPTION)
                .author(crate_authors!())
                .version(crate_version!()), // .long_about(LONG_DESCRIPTION)
                                            // .arg(
                                            //     Arg::new(ARG_FOR_INDEX)
                                            //         .help(ARG_FOR_INDEX_HELP)
                                            //         .long(ARG_FOR_INDEX),
                                            // ),
        )
    }
}

impl Touch for ArgMatches {
    fn matches_touch(self, base_dir: &VfsPath) -> Self {
        use markdown_database_lib::touch_use_case::prelude::*;

        let Some(_matches) = self.subcommand_matches(SUB_COMMAND_NAME) else {
            return self;
        };

        let gateway = MarkdownDirectory::from(base_dir.clone());

        let request_model = TouchRequestModel {
            gateway,
            directory_schema: &mut NestedEpochTimestamp::default(),
            parameters: NestedEpochTimestampParameters::from(Local::now()),
        };

        match touch(request_model) {
            Ok(TouchResponseModel { new_path }) => {
                println!("File '{}' added", new_path.as_str());
            }
            Err(TouchError::UnableToCreateDir(dir)) => {
                eprintln!("1703597521 - Unable to create directory `{dir}`");
            }
            Err(TouchError::UnableToCreateIndexFile(file)) => {
                eprintln!("1703597634 - Unable to create index file `{file}`");
            }
        }

        self
    }
}
