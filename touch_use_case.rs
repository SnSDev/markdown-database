//! Create markdown database object file using the supplied schema
//! ///
//! # Example
//!
//! ```
//! # use markdown_database_lib::touch_use_case::prelude::*;
//! # use vfs::MemoryFS;
//! #
//! # #[derive(Default)]
//! # struct MockParams {}
//! #
//! # #[derive(Default)]
//! # struct MockSchema {}
//! #
//! # impl DirectorySchema for MockSchema {
//! #     type Parameters = MockParams;
//! #
//! #     fn calculate_document_path(
//! #         &mut self,
//! #         _: Self::Parameters,
//! #     ) -> Vec<String> {
//! #         vec!["1".to_owned(), "2".to_owned(), "3".to_owned()]
//! #     }
//! # }
//! #
//! # let base_dir = VfsPath::new(MemoryFS::new());
//! #
//! # base_dir.join("1").unwrap().create_dir().unwrap();
//! #
//! # let gateway = MarkdownDirectory::from(base_dir.clone());
//! #
//! // GIVEN
//! let request_model = TouchRequestModel {
//!     // Will create directory structure `~/1/2/3/index.md`
//!     directory_schema: &mut MockSchema::default(),
//!     parameters: MockParams::default(),
//!
//!     // RAM file system (`base_dir`) where directory `~/1` already exists
//!     gateway,
//! };
//!
//! // WHEN
//! let actual = touch(request_model);
//!
//! // THEN
//! assert!(actual.is_ok());
//! assert!(base_dir.join("1/2/3/index.md").unwrap().is_file().unwrap());
//! ```

/// Allow glob import of [`touch`] and support items
pub mod prelude {
    pub use super::{
        use_case as touch, Error as TouchError,
        RequestModel as TouchRequestModel, ResponseModel as TouchResponseModel,
        Result as TouchResult,
    };
    pub use crate::directory_schema::prelude::*;
    pub use crate::gateway::markdown_directory::MarkdownDirectory;
    pub use chrono::prelude::*;
    pub use chrono::Duration;
    pub use vfs::{VfsError, VfsPath, WalkDirIterator};
}

use prelude::*;

/// Parameters for [`touch`]
pub struct RequestModel<'a, DS: DirectorySchema> {
    /// Base directory of markdown database
    pub gateway: MarkdownDirectory,

    /// The business logic for where in the file system to keep a specified key
    pub directory_schema: &'a mut DS,

    /// Parameters needed for [`Self::directory_schema`]
    pub parameters: DS::Parameters,
}

/// Output from [`touch`]
pub type Result = std::result::Result<ResponseModel, Error>;

/// Success state for [`touch`]
pub struct ResponseModel {
    /// New file path
    pub new_path: VfsPath,
}

/// Failure state for [`touch`]
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Error {
    /// Error occurred while trying to create the enclosed directory
    UnableToCreateDir(String),

    /// Error occurred while trying to create the enclosed file
    UnableToCreateIndexFile(String),
}

const INDEX_FILE: &str = "index.md";

/// Create file for current unix timestamp
///
/// See [`crate::touch_use_case`]
///
/// # Errors
///
/// File system errors can occur and are returned as [`Error`]
pub fn use_case<DS: DirectorySchema>(
    RequestModel {
        mut gateway,
        directory_schema,
        parameters,
    }: RequestModel<DS>,
) -> Result {
    let mut path = Vec::new();
    let mut path_ref = Vec::new();
    for node in directory_schema.calculate_document_path(parameters) {
        path.push(node);
        path_ref = path.iter().map(|s| s as &str).collect();
        let Ok(_) = gateway.touch_dir(&path_ref) else {
            return Err(Error::UnableToCreateDir(format!(
                "/{}",
                path.join("/")
            )));
        };
    }

    let Ok(new_path) = gateway.touch_file(&path_ref, INDEX_FILE) else {
        return Err(Error::UnableToCreateIndexFile(format!(
            "/{}/{INDEX_FILE}",
            path.join("/")
        )));
    };

    Ok(ResponseModel { new_path })
}

#[cfg(test)]
mod tests {

    #[test]
    fn test1704205603() {}
}
