//! A NoSql Database using Markdown files as Documents
//!
//! *Project by [SnS Development](https://gitlab.com/SnSDev)*
//!
//! ![Coverage](https://gitlab.com/SnSDev/markdown-database/badges/master/pipeline.svg)
//! ![Pipeline](https://gitlab.com/SnSDev/markdown-database/badges/master/coverage.svg)
//! ![License](https://img.shields.io/static/v1?label=license&message=MIT&color=informational)
//! ![Platform](https://img.shields.io/static/v1?label=platform&message=linux-64&color=informational)
//! ![Rust](https://img.shields.io/static/v1?label=rust+edition&message=2018&color=b94700)
//! ![GitLab](https://img.shields.io/static/v1?label=gitlab+project+id&message=28422734&color=554585)
//!
//! About
//! =====
//!
//! Most modern software development projects use Markdown files for code
//! documentation.  This program will quickly traverse all the markdown files
//! in a directory and verify all the links are valid.
//!
//! This program will also work as a database engine, like MongoDB but using
//! markdown files as documents instead of BSON/JSON files.  Documents are
//! formatted like
//!
//! ```markdown
//! ---
//! DocumentId: 11633625479
//! ---
//! # Document Title
//!
//! **Key1** : Value1\
//! **Key2** : Value2
//!
//! ## Foreign Data Block
//!
//! **Key1** : Value1\
//! **Key2** : Value2
//!
//! ## Categorized List
//!
//! - Item 1
//! - Item 2
//! - Item 3
//!
//! ## Tabular Data
//!
//! | Column A | Column B | Column C |
//! | -------- | -------- | -------: |
//! | Cell 1A  | Cell 1B  |  Cell 1C |
//! | Cell 2A  | Cell 2B  |  Cell 2C |
//! | Cell 3A  | Cell 3B  |  Cell 3C |
//!
//! ## Other Formats
//!
//! Other data formats can be embedded in a document by using fenced code
//! blocks.  This allows a transaction, budget or time-clock punch to be
//! embedded (ledger), or an appointment (ical), or a contact (vcard).
//!
//! markdown-database will traverse these documents and compile them together
//! for meaningful reports.
//!
//! ### Scenario 1
//!
//! User does a 3-day project for Client.  User creates a single markdown file
//! to contain the data pertaining to the project.  The markdown file starts
//! with structured data the User uses to track the project, then the user
//! embeds: the relevant emails, text messages, notes, meeting notes,
//! appointments, Client digital business card, the time worked, invoices,
//! payments, etc.  Each embedded in their respective formats.
//!
//! markdown-database can then query across all the user's project markdown
//! files:
//! - All the time-clock data (to run payroll)
//! - All the expenses / payments (profit & loss statement)
//! - All the invoices and their (unpaid invoice report)
//! - The calendar for the week (conflicting appointments)
//! - The text message history with a single person across projects
//! - The status of all active projects
//! - etc
//!
//! Also, at any time the project markdown file can be printed showing the
//! complete overview of the single project.  This is handy in (for example)
//! prepping for a meeting.
//!
//! ### Scenario 2
//!
//! User wants to track the maintenance on several items in their
//! household.  User stores a single markdown file per appliance / vehicle /
//! tracked element like roof.  User stores when / where item was purchased,
//! link to receipt, repeating calendar event for maintenance tasks, notes
//! about issues experienced, lists of parts replaced, logs of maintenance
//! performed, relevant transactions, etc.
//!
//! markdown-database can then:
//! - Make sure user's calendar is updated with maintenance schedule
//! - Make sure user's budget contains known maintenance expenses (e.g. oil
//!   change, replacement batteries, filters, etc)
//! - Produce detailed log of symptoms for service personnel
//! - Combine similar maintenance across different items reducing trips
//!   (e.g. Shopping list includes batteries for smoke detectors, and furnace
//!   air filter.  Both sold at the same store, cheaper to make 1 trip)
//!
//! ### Scenario 3
//!
//! User is throwing a party for a significant event for a friend.  User stores
//! the guest list, venue info, RSVPs, conversations with vendors, gift
//! registry, etc.
//! ```
//!
//! Indexes have metadata sections containing instructions on how to index
//! the data
//!
//! ```markdown
//! ---
//! NodeType: Foo;
//! Index By: Foo.Bar;
//! Index Algorithm: Numeric(4);
//! Format: [Bar] - [Foo];
//! Title: Foo by Bar;
//! ** Content below is auto-generated, updates will be overwritten **
//! ---
//! Foo by Bar
//! ==========
//! - 0001 - [Foo Item2](Item2.Foo.md)
//! - 1234 - [Foo Test](Test.Foo.md)
//! ```
//!
#![doc = include_str!("ROAD_MAP.md")]
//!
//! Update Documentation
//! ====================
//! [Artifacts on GitLab](https://gitlab.com/SnSDev/markdown-database/-/jobs/artifacts/master/file/public/markdown_database_lib/index.html?job=pages)
//!
//! ```bash
#![doc = include_str!("update_documentation.sh")]
//! ```
//!
//! Dependency
//! ==========
//!
//! Library Dependency
//! ------------------
//!
//! - [once_cell] - Helps avoid rebuilding Regex struct every tokenize
//! - [percent_encoding] - Decode URL character encoding
//! - [rayon] - Multi-Threading Support
//! - [regex] - Parse Strings using Regular Expression
//! - [url] - Parse / Store URL
//! - [vfs] - Emulate and generalize file system access, traverse directory tree
//!
//! Test Dependency
//! ---------------
//!
//! - [🔗pretty_assertions](https://docs.rs/pretty_assertions/1.0.0/pretty_assertions) - Make unit test output easier to read
//!
//! Binary Dependency
//! -----------------
//! - Linux - // TODO: Link to main.rs#Dependency
//! - Windows - *(Not yet supported)*
//! - OS X - *(Not yet supported)*
//!
//! Documentation Dependency
//! ------------------------
//!
//! - [aquamarine] - Parse mermaid flow diagrams in documentation
//!
//! Tools
//! =====
//!
//! - Language: [Rust](https://www.rust-lang.org/tools/install)
//! - Repository: [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
//!
//! Installation - Linux
//! ====================
//!
//! 1. Clone the repo
//!    ```sh
//!    git clone https://gitlab.com/SnSDev/markdown-database.git
//!    cd markdown-database
//!    ```
//! 2. Build the project
//!    ```sh
//!    cargo build --release --features="bin_markdown_database"
//!    ```
//! 3. Copy the executable to `~/bin` folder
//!    ```sh
//!    cp ./target/release/markdown-database ~/bin
//!    ```
//!
#![doc = include_str!("CONTRIBUTING.md")]
//!
#![doc = include_str!("BEFORE_RELEASE.md")]
//!
#![doc = include_str!("PROGRAMMERS_OATH.md")]
//!
#![doc = include_str!("7_RULES_OF_A_GREAT_GIT_COMMIT_MESSAGE.md")]
//!
#![doc = include_str!("LICENSE.md")]
//!
//! Contact
//! =======
//!
//! - [Damian Pound](https://gitlab.com/chinoto)
//! - [Juniper Barlett](https://gitlab.com/emeraldinspirations)

#![warn(missing_docs)]
#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

pub mod compiler;
// pub mod crawl_database_files_use_case;
pub mod data_object;
// pub mod default_use_case;
pub mod entity;
pub mod gateway;
pub mod tokenizer;
// pub mod index_files_use_case;
// pub mod insert_data_use_case;
pub mod list_files_use_case;
// pub mod parse_data_use_case;
// pub mod parse_file_use_case;
// pub mod parse_title_use_case;
// pub mod set_title_use_case;
pub mod directory_schema;
pub mod markdown_token;
pub mod touch_event_use_case; // TODO: Implement use case in Clap boundary
pub mod touch_use_case;
pub mod wrapper;
