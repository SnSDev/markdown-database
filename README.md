# markdown_database_lib

A NoSql Database using Markdown files as Nodes

*Project by [SnS Development](https://gitlab.com/SnSDev)*

![Coverage](https://gitlab.com/SnSDev/markdown-database/badges/master/pipeline.svg)
![Pipeline](https://gitlab.com/SnSDev/markdown-database/badges/master/coverage.svg)
![License](https://img.shields.io/static/v1?label=license&message=MIT&color=informational)
![Platform](https://img.shields.io/static/v1?label=platform&message=linux-64&color=informational)
![Rust](https://img.shields.io/static/v1?label=rust+edition&message=2018&color=b94700)
![GitLab](https://img.shields.io/static/v1?label=gitlab+project+id&message=28422734&color=554585)

About
=====

Most modern software development projects use Markdown files for code
documentation.  This program will quickly traverse all the markdown files
in a directory and verify all the links are valid.

This program will also work as a database engine, like MongoDB but using
markdown files as nodes instead of BSON/JSON files.  Nodes are formatted
like

```markdown
---
DocumentId: 11633625479
---
Node Title
==========

**Key1**
: Value1\
**Key2**
: Value2

Header
------

Other non-indexed data
```

Indexes have metadata sections containing instructions on how to index
the data

```markdown
---
NodeType: Foo;
Index By: Foo.Bar;
Index Algorithm: Numeric(4);
Format: [Bar] - [Foo];
Title: Foo by Bar;
** Content below is auto-generated, updates will be overwritten **
---
Foo by Bar
==========
- 0001 - [Foo Item2](Item2.Foo.md)
- 1234 - [Foo Test](Test.Foo.md)
```

Roadmap
=======

- [x] Deploy MVP - ([Issue](https://gitlab.com/SnSDev/markdown-database/-/issues/3))
- [ ] Only display "Parsed" or "Verified" if `-v` / `-verbose` flag set -
      ([Issue](https://gitlab.com/SnSDev/markdown-database/-/issues/12))
- [ ] Summary at end of `CRAWL`
- [ ] `.gitignore` as filter by default
- [ ] error for orphaned markdown files
- [ ] `--no-orphans` tag to error on ANY orphaned file
- [ ] Windows support
- [ ] OSX support
- [ ] Parse indexable data in markdown files
- [ ] Parse index file metadata and fill contents
- [ ] `CREATE <Template>` opens text editor with contents of template,
      updates indexes
- [ ] `ASSERT <key> = <value>, ...` returns updates needed and prompts to
      auto-update
- [ ] `DELETE <NodeId>` deletes node and updates index
- [ ] `INDEX <NodeType> BY <key>` creates and updates index file
- [ ] Build query language
- [ ] Integrate into Language Server Protocol
- [ ] Add FFI support for all use-cases

See the
[open issues](https://gitlab.com/SnSDev/markdown-database/-/issues)
for a full list of proposed features (and known issues).

Update Documentation
====================
[Artifacts on GitLab](https://gitlab.com/SnSDev/markdown-database/-/jobs/artifacts/master/file/public/markdown_database_lib/index.html?job=pages)

```bash
# Run in the root of project
./update_documentation.sh
```

Dependency
==========

Library Dependency
------------------

- [once_cell] - Helps avoid rebuilding Regex struct every tokenize
- [percent_encoding] - Decode URL character encoding
- [rayon] - Multi-Threading Support
- [regex] - Parse Strings using Regular Expression
- [url] - Parse / Store URL
- [vfs] - Emulate and generalize file system access, traverse directory tree

Test Dependency
---------------

- [🔗pretty_assertions](https://docs.rs/pretty_assertions/1.0.0/pretty_assertions) - Make unit test output easier to read

Binary Dependency
-----------------
- Linux - // TODO: Link to main.rs#Dependency
- Windows - *(Not yet supported)*
- OS X - *(Not yet supported)*

Documentation Dependency
------------------------

- [aquamarine] - Parse mermaid flow diagrams in documentation

Tools
=====

- Language: [Rust](https://www.rust-lang.org/tools/install)
- Repository: [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Installation - Linux
====================

1. Clone the repo
   ```sh
   git clone https://gitlab.com/SnSDev/markdown-database.git
   cd markdown-database
   ```
2. Build the project
   ```sh
   cargo build --release --features="bin_markdown_database"
   ```
3. Copy the executable to `~/bin` folder
   ```sh
   cp ./target/release/markdown-database ~/bin
   ```


License: MIT
