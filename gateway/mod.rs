//! Interface to data outside the application
//!
//! The traits are defined at the root of this module.  The sub-modules contain
//! specific implementations of the traits.  The correct implementation is
//! supplied at compile time by the `boundary`

pub mod markdown_directory;
