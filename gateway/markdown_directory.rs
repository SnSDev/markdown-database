//! The `MarkdownDirectory` is a folder that contains the database
//!
//! The database is a directory structure containing markdown files

pub mod prelude {
    pub use super::{
        Error as MarkdownDirectoryError, MarkdownDirectory,
        Result as MarkdownDirectoryResult,
    };
    pub use chrono::prelude::*;
    pub use chrono::Duration;
    pub use vfs::{VfsError, VfsPath, WalkDirIterator};
}

use prelude::*;

pub struct MarkdownDirectory {
    base_dir: VfsPath,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Error {
    UnableToCreateYearDir(String),
    UnableToCreateYearFile(String),
    UnableToCreateDir(String),
    UnableToCreateFile(String),
    UnableToCreateMonthDir(String),
    UnableToCreateMonthFile(String),
    UnableToCreateWeekDir(String),
    UnableToCreateWeekFile(String),
}

pub type Result<T> = std::result::Result<T, Error>;

impl MarkdownDirectory {
    pub fn touch_year<TZ: TimeZone>(
        &mut self,
        timestamp: &DateTime<TZ>,
    ) -> Result<VfsPath> {
        let year = timestamp.year().to_string();
        let year_path = [year.as_str()];

        self.touch_dir(&year_path)
            .map_err(|_| Error::UnableToCreateYearDir(year.clone()))?;

        let year_file = self
            .touch_file(&year_path, "index.md")
            .map_err(|_| Error::UnableToCreateYearFile(year.clone()))?;

        Ok(year_file)
    }

    pub fn touch_month<TZ: TimeZone>(
        &mut self,
        timestamp: &DateTime<TZ>,
    ) -> Result<VfsPath> {
        let year = timestamp.year().to_string();
        let month = match timestamp.month() {
            1 => "01-jan",
            2 => "02-feb",
            3 => "03-mar",
            4 => "04-apr",
            5 => "05-may",
            6 => "06-jun",
            7 => "07-jul",
            8 => "08-aug",
            9 => "09-sep",
            10 => "10-oct",
            11 => "11-nov",
            12 => "12-dec",
            _ => unreachable!("1704128258"),
        }
        .to_owned();

        let month_path = [year.as_str(), month.as_str()];
        let month_path_string =
            || ["", year.as_str(), month.as_str()].join("/");

        self.touch_dir(&month_path)
            .map_err(|_| Error::UnableToCreateMonthDir(month_path_string()))?;

        let year_file = self
            .touch_file(&month_path, "index.md")
            .map_err(|_| Error::UnableToCreateMonthFile(month_path_string()))?;

        Ok(year_file)
    }

    pub fn touch_dir(&mut self, path: &[&str]) -> Result<VfsPath> {
        path.iter()
            .try_fold(
                (self.base_dir.clone(), vec![String::new()]),
                |(dir, mut error_path), node| {
                    error_path.push((*node).to_owned());
                    let new_dir =
                        dir.join(node).map_err(|_| error_path.clone())?;
                    if !new_dir.exists().map_err(|_| error_path.clone())? {
                        new_dir.create_dir().map_err(|_| error_path.clone())?;
                    }
                    Ok((new_dir, error_path))
                },
            )
            .map_err(|error_path: Vec<String>| {
                Error::UnableToCreateDir(error_path.join("/"))
            })
            .map(|(dir, _)| dir)
    }

    pub fn touch_file(
        &mut self,
        path: &[&str],
        file_name: &str,
    ) -> Result<VfsPath> {
        let error = |_| {
            Error::UnableToCreateFile(
                [&[""], path, &[file_name]].concat().join("/"),
            )
        };
        let parent_dir = self.base_dir.join(path.join("/")).map_err(error)?;

        let new_file = parent_dir.join(file_name).map_err(error)?;

        if !new_file.exists().map_err(error)? {
            new_file.create_file().map_err(error)?;
        }

        Ok(new_file)
    }

    pub fn touch_week<TZ: TimeZone>(
        &mut self,
        timestamp: &DateTime<TZ>,
    ) -> Result<VfsPath> {
        let year = timestamp.year().to_string();
        let week = format!("week_{:02}", timestamp.iso_week().week());

        let week_path = [year.as_str(), week.as_str()];
        let week_path_string = || ["", year.as_str(), week.as_str()].join("/");

        self.touch_dir(&week_path)
            .map_err(|_| Error::UnableToCreateWeekDir(week_path_string()))?;

        let week_file = self
            .touch_file(&week_path, "index.md")
            .map_err(|_| Error::UnableToCreateWeekFile(week_path_string()))?;

        Ok(week_file)
    }
}

impl From<VfsPath> for MarkdownDirectory {
    fn from(base_dir: VfsPath) -> Self {
        MarkdownDirectory { base_dir }
    }
}

// Used mostly for tests
impl From<MarkdownDirectory> for VfsPath {
    fn from(wrapper: MarkdownDirectory) -> Self {
        wrapper.base_dir
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn test1704101345_touch_year() {
        use markdown_database_lib::gateway::markdown_directory::prelude::*;
        use vfs::MemoryFS;

        // GIVEN
        let dummy_timestamp = Local
            .from_local_datetime(
                &NaiveDate::from_ymd_opt(2024, 1, 1)
                    .unwrap()
                    .and_hms_opt(3, 21, 0)
                    .unwrap(),
            )
            .unwrap();
        let mut gateway: MarkdownDirectory =
            VfsPath::new(MemoryFS::new()).into();

        // WHEN
        let actual = gateway.touch_year(&dummy_timestamp);

        // THEN
        let new_file = actual.unwrap();
        assert_eq!(new_file.as_str(), "/2024/index.md");
        assert!(new_file.is_file().unwrap());
    }

    #[test]
    fn test1704104144_touch_dir() {
        use markdown_database_lib::gateway::markdown_directory::prelude::*;
        use vfs::MemoryFS;

        // GIVEN
        let mut gateway: MarkdownDirectory =
            VfsPath::new(MemoryFS::new()).into();

        // WHEN
        let actual = gateway.touch_dir(&["a", "b", "c"]);

        // THEN
        let new_dir = actual.unwrap();
        assert_eq!(new_dir.as_str(), "/a/b/c");
        assert!(new_dir.is_dir().unwrap());

        // WHEN
        let actual2 = gateway.touch_dir(&["a", "d", "e"]);

        // THEN
        let new_dir2 = actual2.unwrap();
        assert_eq!(new_dir2.as_str(), "/a/d/e");
        assert!(new_dir2.is_dir().unwrap());
    }

    #[test]
    fn test1704126759_touch_file() {
        use markdown_database_lib::gateway::markdown_directory::prelude::*;
        use vfs::MemoryFS;

        // GIVEN
        let mut gateway: MarkdownDirectory =
            VfsPath::new(MemoryFS::new()).into();

        // WHEN
        let actual = gateway.touch_file(&[], "file_name.ext");

        // THEN
        let new_file = actual.unwrap();
        assert_eq!(new_file.as_str(), "/file_name.ext");
        assert!(new_file.is_file().unwrap());

        // WHEN
        let actual2 = gateway.touch_file(&[], "file_name.ext");

        // THEN
        let new_file2 = actual2.unwrap();
        assert_eq!(new_file2.as_str(), "/file_name.ext");
        assert!(new_file2.is_file().unwrap());
    }

    #[test]
    fn test1704127982_touch_month() {
        use markdown_database_lib::gateway::markdown_directory::prelude::*;
        use vfs::MemoryFS;

        // GIVEN
        let dummy_timestamp = Local
            .from_local_datetime(
                &NaiveDate::from_ymd_opt(2024, 1, 1)
                    .unwrap()
                    .and_hms_opt(3, 21, 0)
                    .unwrap(),
            )
            .unwrap();
        let mut gateway: MarkdownDirectory =
            VfsPath::new(MemoryFS::new()).into();

        // WHEN
        let actual = gateway.touch_month(&dummy_timestamp);

        // THEN
        let new_file = actual.unwrap();
        assert_eq!(new_file.as_str(), "/2024/01-jan/index.md");
        assert!(new_file.is_file().unwrap());
    }

    #[test]
    fn test1704128698_touch_week() {
        use markdown_database_lib::gateway::markdown_directory::prelude::*;
        use vfs::MemoryFS;

        // GIVEN
        let dummy_timestamp = Local
            .from_local_datetime(
                &NaiveDate::from_ymd_opt(2024, 1, 1)
                    .unwrap()
                    .and_hms_opt(3, 21, 0)
                    .unwrap(),
            )
            .unwrap();
        let mut gateway: MarkdownDirectory =
            VfsPath::new(MemoryFS::new()).into();

        // WHEN
        let actual = gateway.touch_week(&dummy_timestamp);

        // THEN
        let new_file = actual.unwrap();
        assert_eq!(new_file.as_str(), "/2024/week_01/index.md");
        assert!(new_file.is_file().unwrap());
    }
}
