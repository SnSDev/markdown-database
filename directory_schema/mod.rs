//! Locate markdown file in directory structure based on document id

mod _trait;

/// Glob import necessary to implement or use a directory schema
pub mod prelude {
    pub use crate::directory_schema::_trait::_Trait as DirectorySchema;
}

pub mod nested_epoch_timestamp;
