// Reason: "Having the same name is accepted" https://github.com/rust-lang/rust-clippy/blob/master/clippy_lints/src/enum_variants.rs#L266-L267
// TODO: Get rid of this when fixed upstream
#[allow(clippy::module_name_repetitions)]
/// TODO
pub trait _Trait {
    /// The struct containing parameters needed for [`_Trait::calculate_document_path`]
    type Parameters;

    ///
    ///
    /// # Errors
    ///
    /// // TODO
    fn calculate_document_path(
        &mut self,
        parameters: Self::Parameters,
    ) -> Vec<String>;
}
