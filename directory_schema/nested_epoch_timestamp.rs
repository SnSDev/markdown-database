//! The business logic to split out a unix timestamp into triplets
//!
//! # Example
//!
//! ```
//! # use markdown_database_lib::directory_schema::nested_epoch_timestamp::prelude::*;
//! # use chrono::{Local, NaiveDate, TimeZone, DateTime};
//! #
//! // GIVEN
//! let timestamp: DateTime<Local> = // 1703555646
//!     Local
//!         .from_local_datetime(
//!             &NaiveDate::from_ymd_opt(2023, 12, 25)
//!                 .unwrap()
//!                 .and_hms_opt(19, 54, 6)
//!                 .unwrap(),
//!         )
//!         .unwrap()
//!         .into();
//! let params = NestedEpochTimestampParameters::from(timestamp);
//! let mut directory_schema = NestedEpochTimestamp::default();
//!
//! // WHEN
//! let actual = directory_schema.calculate_document_path(params);
//!
//! // THEN
//! assert_eq!(
//!     vec![
//!         "170".to_owned(),
//!         "355".to_owned(),
//!         "5646".to_owned()
//!     ], actual);
//! ```

use chrono::{DateTime, TimeZone};

/// Allow glob import of [`NestedEpochTimestamp`] and support items
pub mod prelude {
    pub use super::{
        NestedEpochTimestamp, Parameters as NestedEpochTimestampParameters,
    };
    pub use crate::directory_schema::prelude::*;
}

use prelude::*;

/// The business logic to split out a unix timestamp into triplets
///
/// See [`crate::directory_schema::nested_epoch_timestamp`]
#[derive(Default)]
pub struct NestedEpochTimestamp();

impl DirectorySchema for NestedEpochTimestamp {
    type Parameters = Parameters;

    fn calculate_document_path(
        &mut self,
        parameters: Self::Parameters,
    ) -> Vec<String> {
        vec![
            first_triplet(&parameters.unix_timestamp),
            second_triplet(&parameters.unix_timestamp),
            final_quadruplet(&parameters.unix_timestamp),
        ]
    }
}

/// The unix timestamp to be parsed, stored as a string.
///
/// See [`NestedEpochTimestamp`]
pub struct Parameters {
    unix_timestamp: String,
}

impl<TZ: TimeZone> From<DateTime<TZ>> for Parameters {
    fn from(date_time: DateTime<TZ>) -> Self {
        Parameters {
            unix_timestamp: date_time.timestamp().to_string(),
        }
    }
}

#[must_use]
fn first_triplet(unix_timestamp: &str) -> String {
    unix_timestamp[0..3].to_owned()
}

#[must_use]
fn second_triplet(unix_timestamp: &str) -> String {
    unix_timestamp[3..6].to_owned()
}

#[must_use]
fn final_quadruplet(unix_timestamp: &str) -> String {
    unix_timestamp[6..10].to_owned()
}
