use super::*;

pub struct DefaultUseCaseRequestModel {
    pub table: Table,
    pub filter_vec: Vec<Filter>,
}
