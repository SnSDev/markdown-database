// markdown-database CREATE template
//   New Template
//   ============
//
//   **Field**
//   : Value

// markdown-database --data-type=transaction --output=|Index.md --Title="Index of 123" .

// markdown-database --field="Test 1" --field="Test 2" --unique --output=TSV .

// markdown-database ./Get_Data.query

// markdown-database ASSERT --where="hymn:Song;hymnal:M3;" --then="number:123;" -f ./Sheet\ Music
//  MATCH  : ./file1
//  SET    : ./file2
//    ~number:null;~
//     number:123;
//  UPDATE : ./file3
//    ~number:321;~
//     number:123;
//  ERROR  : ./file4
//     number:222;number:223; -- Duplicate Keys

// markdown-database DELETE -f

// markdown-database REMOVE --data-type=transaction ./file.md

// markdown-database ADVANCE ./Lead/123.md
//   Lead will advance to Proposal

// markdown-database ADVANCE --to=Closed ./Lead/123.md
//   Fields to update
//   ================
//
//   **Status**
//   : Closed ~Lead~
//
//   Closed
//   ------
//
//   **Date**
//   : 2021-11-02\
//   **Reason**
//   :

// markdown-database MERGE

// markdown-database PARSE -f

mod error;
mod request_model;
mod response_model;
mod use_case;

pub use error::DefaultUseCaseError;
pub use request_model::DefaultUseCaseRequestModel;
pub use response_model::DefaultUseCaseResponseModel;
pub use use_case::default_use_case;
pub type DefaultUseCaseResult =
    Result<DefaultUseCaseResponseModel, DefaultUseCaseError>;

use crate::data_object::Record;
use crate::wrapper::file_path::prelude::*;

pub use crate::data_object::{Datum, Table};

pub type Filter = fn(&Record) -> bool;
