use super::*;

pub fn default_use_case(
    request_model: DefaultUseCaseRequestModel,
) -> DefaultUseCaseResult {
    if !request_model.table.base_path.exists().unwrap_or(false) {
        return Err(DefaultUseCaseError::UnableToReadPath(
            request_model.table.base_path,
        ));
    }
    unimplemented!("1635965417")
}

#[cfg(test)]
mod test {
    use crate::wrapper::file_path::prelude::*;
    use crate::{
        data_object::{Datum, Record, Table},
        default_use_case::{
            default_use_case, DefaultUseCaseError, DefaultUseCaseRequestModel,
        },
    };

    fn dummy_root() -> FilePath {
        FilePath::new(MemoryFS::default())
    }

    #[test]
    fn test1635981542_bad_path() {
        let path = dummy_root().join("non_existing_path").unwrap();
        let request_model = DefaultUseCaseRequestModel {
            filter_vec: Default::default(),
            table: Table {
                base_path: path.clone(),
                field_vec: Default::default(),
                template: Default::default(),
            },
        };

        let result = default_use_case(request_model);

        assert_eq!(
            DefaultUseCaseError::UnableToReadPath(path),
            result.err().unwrap()
        )
    }

    #[test]
    fn test1635982869_is_file() {
        let path = dummy_root().join("existing.path").unwrap();
        path.create_file().unwrap();

        let request_model = DefaultUseCaseRequestModel {
            filter_vec: Default::default(),
            table: Table {
                base_path: path,
                field_vec: Default::default(),
                template: Default::default(),
            },
        };

        let result = default_use_case(request_model);

        assert_eq!(
            [Record::from(vec![Datum {
                namespace: Default::default(),
                key: "key".to_string(),
                value: Default::default(),
            }])],
            result.ok().unwrap().collect::<Vec<Record>>()
        )
    }
}
