use super::*;

pub struct DefaultUseCaseResponseModel {
    field_vec: Vec<String>,
    filter_vec: Vec<Filter>,
    record_iter: Box<dyn Iterator<Item = Record>>,
}

impl Iterator for DefaultUseCaseResponseModel {
    type Item = Record;

    fn next(&mut self) -> Option<Self::Item> {
        self.record_iter.by_ref().find_map(|record: Record| {
            { self.filter_vec.iter().all(|filter| filter(&record)) }
                .then(|| record.join(&self.field_vec))
        })
    }
}

#[cfg(test)]
mod test {
    use crate::{
        data_object::{Datum, Record},
        default_use_case::DefaultUseCaseResponseModel,
    };
    use pretty_assertions::assert_eq;
    use std::vec::IntoIter;

    fn dummy_record_vec() -> Vec<Record> {
        vec![
            Record::from(vec![Datum {
                namespace: Default::default(),
                key: "Key1".to_owned(),
                value: Some("Value1".to_owned()),
            }]),
            Record::from(vec![Datum {
                namespace: Default::default(),
                key: "Key2".to_owned(),
                value: Some("Value2".to_owned()),
            }]),
        ]
    }

    fn dummy_record_iterator() -> IntoIter<Record> {
        dummy_record_vec().into_iter()
    }

    #[test]
    fn test1635976686_result_iter_no_filter() {
        let response_model = DefaultUseCaseResponseModel {
            field_vec: vec!["Key2".to_owned()],
            filter_vec: Vec::new(),
            record_iter: Box::new(dummy_record_iterator()),
        };

        assert_eq!(
            [
                Record::from(vec![Datum {
                    namespace: Default::default(),
                    key: "Key2".to_owned(),
                    value: None,
                }]),
                Record::from(vec![Datum {
                    namespace: Default::default(),
                    key: "Key2".to_owned(),
                    value: Some("Value2".to_owned()),
                }]),
            ],
            response_model.collect::<Vec<Record>>()
        )
    }

    #[test]
    fn test1635979849_result_iter_with_filter() {
        let response_model = DefaultUseCaseResponseModel {
            field_vec: vec!["Key1".to_owned(), "Key2".to_owned()],
            filter_vec: vec![|record: &Record| record.value("Key2").is_some()],
            record_iter: Box::new(dummy_record_iterator()),
        };

        assert_eq!(
            [Record::from(vec![
                Datum {
                    namespace: Default::default(),
                    key: "Key1".to_owned(),
                    value: None,
                },
                Datum {
                    namespace: Default::default(),
                    key: "Key2".to_owned(),
                    value: Some("Value2".to_owned()),
                }
            ]),],
            response_model.collect::<Vec<Record>>()
        )
    }
}
