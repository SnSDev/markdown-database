use super::*;

#[derive(Debug, PartialEq)]
pub enum DefaultUseCaseError {
    UnableToReadPath(FilePath),
}
