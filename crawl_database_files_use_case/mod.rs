//! Recursively parse all links in `index.md`
//!
//! Paths are relative to the current directory.
//!
//! Use Case
//! ========
//!
//! See [\ApplicationArchitecture::UseCase]
//!
//! Data
//! ----
//!
//! - [CrawlDatabaseFilesRequestModel::base_dir]
//! - [CrawlDatabaseFilesRequestModel::real_time_output]
//!
//! Primary Course
//! --------------
//!
//! 1. User issues [crate::crawl_database_files_use_case]
//! 2. System crates a list of unparsed markdown files containing only
//!    `index.md`
//! - - -
//! 3. System runs [crate::parse_markdown_file_use_case] on the list of unparsed
//!    markdown files (This is designed to run in parallel)
//! 4. System informs User of successful completion of each
//!    [crate::parse_markdown_file_use_case] and any errors that occurred while
//!    running
//! 5. System collects new list of markdown files to parse from the links inside
//!    the parsed markdown files, removes already parsed files
//! 6. If any markdown files remain to be parsed System goes back to step 3
//! - - -
//! 7. System returns summary
//!    - [CrawlDatabaseFilesResponseModel::result]

mod error;
mod request_model;
mod response_model;
mod use_case;

pub use super::crawl_database_files_use_case as prelude;
pub use error::CrawlDatabaseFilesError;
pub use request_model::CrawlDatabaseFilesRequestModel;
pub use response_model::CrawlDatabaseFilesResponseModel;
pub use use_case::crawl_database_files;

use crate::entity::{MarkdownLinkSpider, SpiderMessage};
use crate::wrapper::file_path::prelude::*;
use std::sync::mpsc::Sender;

pub const SHORT_DESCRIPTION: &str = "Recursively parse all links in `index.md`";

pub const LONG_DESCRIPTION: &str = r#"
Recursively parse all links in `index.md`

Paths are relative to the current directory.
"#;

pub const COMMAND_NAME: &str = "crawl";

pub type CrawlDatabaseFilesResult =
    Result<CrawlDatabaseFilesResponseModel, CrawlDatabaseFilesError>;
