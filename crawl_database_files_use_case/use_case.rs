// use crate::entity::MarkdownLinkSpider;

use super::*;

pub fn crawl_database_files(
    request_model: CrawlDatabaseFilesRequestModel,
) -> CrawlDatabaseFilesResult {
    let response_model = CrawlDatabaseFilesResponseModel::default();
    let index_path = find_index_path(&request_model);
    let mut spider = MarkdownLinkSpider::new(index_path);
    spider.crawl(request_model.real_time_output);
    Ok(response_model)
}

fn find_index_path(request_model: &CrawlDatabaseFilesRequestModel) -> FilePath {
    let index = request_model.base_dir.join("index.md").expect(
        "1633552878 - Unreachable: index.md should always be a valid path",
    );
    let read_me = request_model.base_dir.join("README.md").expect(
        "1633552972 - Unreachable: README.md should always be a valid path",
    );
    if index.exists().unwrap_or(false) {
        index // First option `index.md`
    } else if read_me.exists().unwrap_or(false) {
        read_me // Second option `README.md`
    } else {
        index // Neither exists, will error based on `index.md`
    }
}
