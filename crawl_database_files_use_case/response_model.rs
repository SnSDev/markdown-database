#[derive(Debug, PartialEq)]
pub struct CrawlDatabaseFilesResponseModel {
    pub result: Result<(), ()>,
    // pub parsed_file_count: usize,
    // pub file_with_error_count: usize,
    // pub markdown_file_ignored_count: usize,
    // pub unreferenced_markdown_file_count: usize,
    // pub file_with_error_vec: Vec<FilePath>,
}

impl Default for CrawlDatabaseFilesResponseModel {
    fn default() -> Self {
        Self {
            result: Ok(()),
            // parsed_file_count: Default::default(),
            // file_with_error_count: Default::default(),
            // markdown_file_ignored_count: Default::default(),
            // unreferenced_markdown_file_count: Default::default(),
            // file_with_error_vec: Default::default(),
        }
    }
}
