use super::*;

pub struct CrawlDatabaseFilesRequestModel {
    pub base_dir: FilePath,
    pub real_time_output: Sender<SpiderMessage>,
}
