use pretty_assertions::assert_eq;

use crate::entity::compiler::{FileDataInner, Token};
use crate::entity::Url;
use crate::wrapper::file_path::prelude::*;
use crate::{
    compiler::{
        definition_list::DefinitionListPairToken,
        markdown_fenced_code_block::MarkdownFencedCodeBlockToken,
        markdown_file::coordinator,
        markdown_heading::{MarkdownHeadingLevel, MarkdownHeadingToken},
        markdown_link::{MarkdownLinkLocalFileToken, MarkdownLinkUrlToken},
        markdown_link_or_alias::MarkdownLinkOrAliasToken,
        markdown_unordered_list::MarkdownUnorderedListItemToken,
        title::{TitleToken, TitleType},
        yaml_front_matter_block::{
            YamlFrontMatterBlockToken, YamlKeyValuePair,
        },
        TokenEnum,
    },
    entity::compiler::FileCaret,
};

#[test]
fn test1636493861_coordinator() {
    // Test Environment
    let test_file_path_str = "test_file.md";
    let test_file_contents = include_str!("./test_file.md");
    let factory = FilePath::memory_fs_factory().add_file(
        test_file_path_str,
        "",
        test_file_contents,
    );
    let test_file_path =
        factory.shortcut_hashmap.remove(test_file_path_str).unwrap();
    let file_data = FileDataInner::from_opened_file(
        test_file_contents.to_string(),
        test_file_path,
    );

    // Given
    let coordinator = coordinator(file_data);

    // When
    let actual = coordinator
        .filter(|token| {
            matches!(
                token,
                TokenEnum::YamlFrontMatterBlock(_)
                    | TokenEnum::MarkdownHeading(_)
                    | TokenEnum::Title(_)
                    | TokenEnum::MarkdownUnorderedListItem(_)
                    | TokenEnum::MarkdownLinkOrAlias(_)
                    | TokenEnum::MarkdownLinkLocalFile(_)
                    | TokenEnum::MarkdownFencedCodeBlock(_)
                    | TokenEnum::DefinitionListPair(_)
                    | TokenEnum::MarkdownLinkUrl(_)
            )
        })
        .collect::<Vec<TokenEnum>>();

    // Then
    assert_eq!(
        [
            TokenEnum::from(
                YamlFrontMatterBlockToken {
                    metadata: vec![YamlKeyValuePair {
                        depth: 0,
                        key: "YAML Front Matter Block Contents".to_string(),
                        value: "Valid Metadata Test Page".to_string(),
                    }],
                }
                .from_file(FileCaret {
                    range: 0..67,
                    inner_range: None,
                    file_data
                },)
            ),
            MarkdownHeadingToken {
                level: MarkdownHeadingLevel::Heading1,
                caption: "Heading 1".to_string(),
            }
            .from_file(FileCaret {
                range: 67..86,
                inner_range: None,
                file_data
            },)
            .into(),
            TitleToken {
                title: "Heading 1".to_owned(),
                title_type: TitleType::HeadingLevel1,
            }
            .from_file(FileCaret {
                range: 67..86,
                inner_range: None,
                file_data
            },)
            .into(),
            MarkdownUnorderedListItemToken {
                content: "Unordered List Item 1".to_string(),
            }
            .from_file(FileCaret {
                range: 87..111,
                inner_range: Some(90..111),
                file_data
            },)
            .into(),
            MarkdownUnorderedListItemToken {
                content: "Unordered List Item 2".to_string(),
            }
            .from_file(FileCaret {
                range: 111..135,
                inner_range: Some(114..135),
                file_data
            },)
            .into(),
            MarkdownHeadingToken {
                level: MarkdownHeadingLevel::Heading2,
                caption: "Heading 2".to_string(),
            }
            .from_file(FileCaret {
                range: 137..156,
                inner_range: None,
                file_data
            },)
            .into(),
            MarkdownLinkOrAliasToken {
                labeless_path: None,
                label: Some("LinkDescription".to_string()),
                path: None,
                alias_name: Some("Alias1".to_string()),
                alias_path: None,
            }
            .from_file(FileCaret {
                range: 158..183,
                inner_range: None,
                file_data
            },)
            .into(),
            MarkdownLinkOrAliasToken {
                labeless_path: None,
                label: Some("LocalLink".to_string()),
                path: Some(".".to_string()),
                alias_name: None,
                alias_path: None,
            }
            .from_file(FileCaret {
                range: 184..198,
                inner_range: None,
                file_data
            },)
            .into(),
            MarkdownLinkLocalFileToken {
                file_path: file_data.borrow_parent_dir().clone(),
                caption: Some("LocalLink".to_string()),
            }
            .from_file(FileCaret {
                range: 184..198,
                inner_range: None,
                file_data
            },)
            .into(),
            MarkdownHeadingToken {
                level: MarkdownHeadingLevel::Heading6,
                caption: "Heading 6".to_string(),
            }
            .from_file(FileCaret {
                range: 200..216,
                inner_range: None,
                file_data
            },)
            .into(),
            MarkdownFencedCodeBlockToken {
                language: Some("language".to_string()),
                code_block: "fenced_code_block_code".to_string(),
            }
            .from_file(FileCaret {
                range: 218..257,
                inner_range: None,
                file_data
            },)
            .into(),
            DefinitionListPairToken {
                key: "Definition List Key 1".to_string(),
                value: "Definition List Value 1".to_string(),
            }
            .from_file(FileCaret {
                range: 263..315,
                inner_range: None,
                file_data
            },)
            .into(),
            DefinitionListPairToken {
                key: "Definition List Key 2".to_string(),
                value: "Definition List Value 2".to_string(),
            }
            .from_file(FileCaret {
                range: 316..368,
                inner_range: None,
                file_data
            },)
            .into(),
            MarkdownLinkOrAliasToken {
                labeless_path: None,
                label: Some("Alias1".to_string()),
                path: None,
                alias_name: None,
                alias_path: Some("http://example.com".to_string()),
            }
            .from_file(FileCaret {
                range: 370..399,
                inner_range: None,
                file_data
            },)
            .into(),
            MarkdownLinkUrlToken {
                url: Url::parse("http://example.com").unwrap(),
            }
            .from_file(FileCaret {
                range: 370..399,
                inner_range: None,
                file_data
            },)
            .into(),
        ],
        actual
    );
}
