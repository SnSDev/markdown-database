use super::*;

pub fn coordinator(
    file_data: FileData,
) -> Coordinator<TokenEnum, TokenizerEnum, ParserEnum> {
    new_coordinator(
        vec![
            DefinitionListTokenizer::default().into(),
            MarkdownFencedCodeBlockTokenizer::default().into(),
            MarkdownHeadingTokenizer::default().into(),
            MarkdownLinkOrAliasTokenizer::default().into(),
            MarkdownUnorderedListTokenizer::default().into(),
            YamlFrontMatterBlockTokenizer::default().into(),
        ],
        vec![
            DatumParser::default().into(),
            MarkdownLinkAliasParser::default().into(),
            MarkdownLinkParser::new(file_data).into(),
            TitleParser::default().into(),
        ],
        file_data.borrow_contents().to_string(),
    )
}
