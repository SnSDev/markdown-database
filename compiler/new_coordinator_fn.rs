use super::*;

pub fn new_coordinator(
    tokenizer_vec: Vec<TokenizerEnum>,
    parser_vec: Vec<ParserEnum>,
    haystack: String,
) -> Coordinator<TokenEnum, TokenizerEnum, ParserEnum> {
    Coordinator::new(tokenizer_vec, parser_vec, haystack)
}
