use pretty_assertions::assert_eq;

use crate::entity::compiler::{FileCaret, Token};
use crate::wrapper::file_path::prelude::*;
use crate::{
    compiler::{
        definition_list::{DefinitionListPairToken, DefinitionListTokenizer},
        markdown_heading::MarkdownHeadingTokenizer,
        new_coordinator, TokenEnum, TokenizerEnum,
    },
    entity::compiler::FileDataInner,
};

#[test]
fn test1636081557_tokenizer() {
    let test_file = include_str!("./test_file.md").to_string();
    let coordinator = new_coordinator(
        vec![
            TokenizerEnum::MarkdownHeading(MarkdownHeadingTokenizer::default()),
            TokenizerEnum::DefinitionList(DefinitionListTokenizer::default()),
        ],
        Vec::new(),
        test_file,
    );

    let file_data = FileDataInner::from_opened_file(
        "".to_string(),
        FilePath::memory_fs_factory().root,
    );

    assert_eq!(
        [
            TokenEnum::from(
                DefinitionListPairToken {
                    key: "Key 1".to_owned(),
                    value: "Value 1 C".to_owned()
                }
                .from_file(FileCaret {
                    range: 53..79,
                    inner_range: None,
                    file_data,
                })
            ),
            DefinitionListPairToken {
                key: "Key 2".to_owned(),
                value: "Value 2 A".to_owned(),
            }
            .from_file(FileCaret {
                range: 80..101,
                inner_range: None,
                file_data
            })
            .into(),
            DefinitionListPairToken {
                key: "key 2".to_owned(),
                value: "Value 2 B".to_owned(),
            }
            .from_file(FileCaret {
                range: 128..152,
                inner_range: None,
                file_data
            })
            .into(),
            DefinitionListPairToken {
                key: "Key 3".to_owned(),
                value: "Value 3".to_owned(),
            }
            .from_file(FileCaret {
                range: 153..172,
                inner_range: None,
                file_data
            })
            .into(),
            DefinitionListPairToken {
                key: "Key 1".to_owned(),
                value: "Value 1 D".to_owned(),
            }
            .from_file(FileCaret {
                range: 199..220,
                inner_range: None,
                file_data
            })
            .into(),
            DefinitionListPairToken {
                key: "Key 4".to_owned(),
                value: "Value 4".to_owned(),
            }
            .from_file(FileCaret {
                range: 222..241,
                inner_range: None,
                file_data
            })
            .into()
        ],
        coordinator
            .filter(|token| matches!(token, TokenEnum::DefinitionListPair(_)))
            .collect::<Vec<TokenEnum>>()
    )
}

#[test]
fn test1636238653_trim_newline_escape() {
    assert_eq!(
        ["Test Without", "Test With"],
        ["Test Without", "Test With\\"]
            .into_iter()
            .map(DefinitionListPairToken::trim_newline_escape)
            .collect::<Vec<&str>>()
    )
}
