use super::*;

pub type DefinitionListTokenizer = RegexTokenizer<
    DefinitionListTokenizerRegex,
    DefinitionListPairToken,
    TokenEnum,
>;

impl From<DefinitionListTokenizer> for TokenizerEnum {
    fn from(tokenizer: DefinitionListTokenizer) -> Self {
        TokenizerEnum::DefinitionList(tokenizer)
    }
}

impl From<TokenizerEnum> for DefinitionListTokenizer {
    fn from(tokenizer: TokenizerEnum) -> Self {
        match tokenizer {
            TokenizerEnum::DefinitionList(z) => z,
            _ => {
                panic!("{}", TokenizerEnum::from_panic("1636685134"))
            }
        }
    }
}
