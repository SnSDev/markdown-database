use super::*;

pub type DefinitionListPairToken = TokenSource<DefinitionListPairTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct DefinitionListPairTokenInner {
    pub key: String,
    pub value: String,
}

impl Token for DefinitionListPairTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl FromCaptures<TokenEnum> for DefinitionListPairTokenInner {
    fn from_captures(
        regex_captures: Captures,
        range: Range<usize>,
        file_data: FileData,
    ) -> TokenEnum {
        let key = regex_captures.name(MATCH_KEY).unwrap_or_else(|| {
            panic!("{}", Self::error_valid_match("1636240184"))
        });

        let value = regex_captures.name(MATCH_VALUE).unwrap_or_else(|| {
            panic!("{}", Self::error_valid_match("1636240546"))
        });

        Self {
            key: Self::handle_markdown_whitespace(key.as_str()),
            value: Some(value.as_str())
                .map(Self::trim_newline_escape)
                .map(Self::handle_markdown_whitespace)
                .unwrap(),
        }
        .from_file(FileCaret {
            range: Self::range_from_captures(&regex_captures, &range),
            inner_range: None,
            file_data,
        })
        .into()
    }
}

impl DefinitionListPairTokenInner {
    pub fn trim_newline_escape(string: &str) -> &str {
        string.trim_end_matches('\\')
    }
}

impl From<DefinitionListPairTokenInner> for TokenEnum {
    fn from(token: DefinitionListPairTokenInner) -> Self {
        TokenEnum::DefinitionListPair(token)
    }
}

impl From<TokenEnum> for DefinitionListPairTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::DefinitionListPair(t) => t,
            _ => {
                panic!("{} - {:?}", TokenEnum::from_panic("1636589840"), token) // TODO: 1636920754 Put Back
            }
        }
    }
}
