use super::*;

pub const MATCH_KEY: &str = "key";
pub const MATCH_VALUE: &str = "value";
pub const REG_EX: &str =
    r#"\*\*(?P<key>[^*\n]+)\*\*\W*\n?\W*:\W+(?P<value>[^\n]+)"#;

#[derive(Debug, Default)]
pub struct DefinitionListTokenizerRegex {}

impl GetRegex for DefinitionListTokenizerRegex {
    fn get_regex() -> Regex {
        Self::regex_from_const(REG_EX, "1636168761")
    }
}
