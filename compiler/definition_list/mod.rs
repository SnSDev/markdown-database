//! ???
//!
//! A Definition List is not a standard Markdown concept, but is part of the
//! markdown-database project.
//!
//! Here is an example:
//!

mod token;
mod tokenizer;
mod tokenizer_regex;

#[cfg(test)]
mod test;

pub use token::DefinitionListPairToken;
pub use tokenizer::DefinitionListTokenizer;
pub use tokenizer_regex::DefinitionListTokenizerRegex;

use crate::compiler::{TokenEnum, TokenizerEnum};
use crate::entity::compiler::prelude::*;
use tokenizer_regex::{MATCH_KEY, MATCH_VALUE};
