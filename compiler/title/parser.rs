use super::*;

pub struct TitleParser {
    status: TitleStatus,
}

impl Default for TitleParser {
    fn default() -> Self {
        Self {
            status: TitleStatus::None,
        }
    }
}

impl TitleParser {
    pub fn set_token(&mut self, token: TokenEnum) {
        self.status = TitleStatus::Pending(token);
    }

    pub fn take_token(&mut self) -> Option<TokenEnum> {
        if matches!(self.status, TitleStatus::None | TitleStatus::Sent) {
            return None;
        }

        let mut status = std::mem::replace(&mut self.status, TitleStatus::Sent);

        match status {
            TitleStatus::Pending(t) => Some(t),
            _ => None,
        }
    }

    pub fn is_none(&self) -> bool {
        matches!(self.status, TitleStatus::None)
    }
}

impl Parser<TokenEnum> for TitleParser {
    fn next_token(&mut self) -> Option<TokenEnum> {
        self.take_token().map(Into::into)
    }

    fn parse_token(&mut self, token: &TokenEnum) {
        if !self.is_none() {
            return; // No need to check for title if already found
        }

        match &token {
            TokenEnum::MarkdownHeading(t)
                if t.level == MarkdownHeadingLevel::Heading1 =>
            {
                self.set_token(
                    TitleToken {
                        source: t.token_source().clone(),
                        title: t.caption.clone(),
                        title_type: TitleType::HeadingLevel1,
                    }
                    .into(),
                );
            }
            TokenEnum::YamlFrontMatterBlock(t) => {
                if let Some(title) = t.find_title() {
                    // TODO: if let Should be included in match block "experimental"
                    self.set_token(
                        TitleToken {
                            source: t.token_source().clone(),
                            title,
                            title_type: TitleType::YamlFrontMatterBlock,
                        }
                        .into(),
                    );
                }
            }
            t => self.set_token(
                TitleNotFoundToken {
                    source: t.token_source().clone(),
                }
                .into(),
            ),
        }
    }

    fn push_eof(&mut self, eof: FileCaret) {
        if self.is_none() {
            self.set_token(TitleNotFoundToken {}.from_file(eof).into())
        }
    }
}

impl From<TitleParser> for ParserEnum {
    fn from(parser: TitleParser) -> Self {
        ParserEnum::Title(parser)
    }
}

impl From<ParserEnum> for TitleParser {
    fn from(parser: ParserEnum) -> Self {
        match parser {
            ParserEnum::Title(p) => p,
            _ => {
                panic!("{}", ParserEnum::from_panic("1638750500"))
            }
        }
    }
}
