use pretty_assertions::assert_eq;

use crate::wrapper::file_path::prelude::*;
use crate::{
    compiler::{
        markdown_file::coordinator,
        title::{TitleNotFoundToken, TitleToken, TitleType},
        TokenEnum,
    },
    entity::compiler::FileDataInner,
};

#[test]
fn test1638749335_parser_yaml() {
    // Given
    let file_contents = include_str!("./test_file_yaml.md");

    // When
    let actual = coordinator(FileDataInner::from_opened_file(
        file_contents.to_string(),
        FilePath::new(MemoryFS::default()),
    ));

    // Then
    assert_eq!(
        [TitleToken {
            range: 4..30,
            title: "TestTitle1638749391".to_string(),
            title_type: TitleType::YamlFrontMatterBlock,
        }],
        actual
            .filter_map(|token| match token {
                TokenEnum::Title(t) => Some(t),
                _ => None,
            })
            .collect::<Vec<TitleToken>>()
    )
}

#[test]
fn test1638752301_parser_h1() {
    // Given
    let file_contents = include_str!("./test_file_h1.md");

    // When
    let actual = coordinator(FileDataInner::from_opened_file(
        file_contents.to_string(),
        FilePath::new(MemoryFS::default()),
    ));

    // Then
    assert_eq!(
        [TitleToken {
            range: 26..65,
            title: "TestTitle1638749448".to_string(),
            title_type: TitleType::HeadingLevel1,
        }],
        actual
            .filter_map(|token| match token {
                TokenEnum::Title(t) => Some(t),
                _ => None,
            })
            .collect::<Vec<TitleToken>>()
    )
}

#[test]
fn test1639856038_parser_no_title() {
    // Given
    let file_contents = include_str!("./test_file_title_not_found.md");

    // When
    let actual = coordinator(FileDataInner::from_opened_file(
        file_contents.to_string(),
        FilePath::new(MemoryFS::default()),
    ));

    // Then
    assert_eq!(
        [TitleNotFoundToken { range: 40..75 }],
        actual
            .filter_map(|token| match token {
                TokenEnum::TitleNotFound(t) => Some(t),
                _ => None,
            })
            .collect::<Vec<TitleNotFoundToken>>()
    )
}

#[test]
fn test1639856038_parser_empty_file() {
    // Given
    let file_contents = "    ";

    // When
    let actual = coordinator(FileDataInner::from_opened_file(
        file_contents.to_string(),
        FilePath::new(MemoryFS::default()),
    ));

    // Then
    assert_eq!(
        [TitleNotFoundToken { range: 4..4 }],
        actual
            .filter_map(|token| match token {
                TokenEnum::TitleNotFound(t) => Some(t),
                _ => None,
            })
            .collect::<Vec<TitleNotFoundToken>>()
    )
}
