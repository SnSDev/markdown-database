use super::*;

pub type TitleToken = TokenSource<TitleTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct TitleTokenInner {
    pub title: String,
    pub title_type: TitleType,
}

impl Token for TitleTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl From<TitleTokenInner> for TokenEnum {
    fn from(token: TitleTokenInner) -> Self {
        Self::Title(token)
    }
}

impl From<TokenEnum> for TitleTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::Title(t) => t,
            _ => {
                panic!("1638748874 - Unable to unwrap to specified token type")
            }
        }
    }
}
