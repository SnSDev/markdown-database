use super::*;

pub type TitleNotFoundToken = TokenSource<TitleNotFoundTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct TitleNotFoundTokenInner {}

impl Token for TitleNotFoundTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &Some(ProblemData {
            problem_level: ProblemLevel::Warning,
            message: "Title Not Found".to_string(),
        })
    }
}

impl From<TitleNotFoundTokenInner> for TokenEnum {
    fn from(token: TitleNotFoundTokenInner) -> Self {
        Self::TitleNotFound(token)
    }
}

impl From<TokenEnum> for TitleNotFoundTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::TitleNotFound(t) => t,
            _ => {
                panic!("1639855165 - Unable to unwrap to specified token type")
            }
        }
    }
}
