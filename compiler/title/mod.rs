mod parser;
mod status;
mod title_not_found_token;
mod title_type;
mod token;

#[cfg(test)]
mod test;

pub use parser::TitleParser;
pub use status::TitleStatus;
pub use title_not_found_token::TitleNotFoundToken;
pub use title_type::TitleType;
pub use token::TitleToken;

use crate::compiler::{
    markdown_heading::MarkdownHeadingLevel, ParserEnum, TokenEnum,
};
use crate::entity::compiler::prelude::*;
