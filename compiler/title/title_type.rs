#[derive(Debug, PartialEq, Clone, serde::Serialize)]
pub enum TitleType {
    YamlFrontMatterBlock,
    HeadingLevel1,
    TBD,
}
