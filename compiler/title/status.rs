use super::*;

#[derive(Debug)]
pub enum TitleStatus {
    None,
    Pending(TokenEnum),
    Sent,
}
