//! A set of Parser, Token, Tokenizer, Director structs to decode a format
//!
//! Parser      - A struct that compiles multiple [Token] into more complete
//!               [Token].  Some validation occurs here.
//! Token       - A representation of the data as found in the source (before
//!               validation and canonicalization)
//! Tokenizer   - Returns the next token inside a specified area of a string
//! Coordinator - The struct that directs the [Token] from the [Tokenizer]
//!               through the [Parser] and returns the results as available.  A
//!               single [Coordinator] struct does this for all [\Compiler], and
//!               is defined in each [\Compiler] as a Type
//! Validator   - Struct that validates a token and returns a
//!               [`crate::data_object`]
//! Compiler    - The namespace that everything is wrapped in

// mod new_coordinator_fn;
// mod parser_enum;
// mod token_enum;
// mod tokenizer_enum;

//
// pub mod datum;
// pub mod definition_list;
// pub mod markdown_fenced_code_block;
// pub mod markdown_file;
// pub mod markdown_heading;
// pub mod markdown_link;
// pub mod markdown_link_alias;
// pub mod markdown_link_or_alias;
// pub mod markdown_unordered_list;
// pub mod title;
// pub mod yaml_front_matter_block;
//
// use crate::entity::compiler::prelude::*;
//
// pub use new_coordinator_fn::new_coordinator;
// pub use parser_enum::ParserEnum;
// pub use token_enum::TokenEnum;
// pub use tokenizer_enum::TokenizerEnum;
//
