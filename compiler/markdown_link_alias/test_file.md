Markdown Link Alias Test Page
=============================

<http://www.example.com/no-alias>
[Title1][Alias1]
[Title2][Alias2]

[Alias2] :www.example.com/valid_alias
[Alias3] : www.example.com/unused_alias
[Alias3] :  www.example.com/duplicate_definition

[Title4](http://www.example.com/no_alias)
