use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Entry {
    unused_definition: Option<TokenEnum>,
    missing_definition: Option<TokenEnum>,
}
