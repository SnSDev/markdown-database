mod duplicate_definition_token;
mod entry;
mod missing_definition_token;
mod parser;
mod unused_definition_token;

#[cfg(test)]
mod test;

pub use duplicate_definition_token::MarkdownLinkAliasDuplicateDefinitionToken;
pub use entry::Entry;
pub use missing_definition_token::MarkdownLinkAliasMissingDefinitionToken;
pub use parser::MarkdownLinkAliasParser;
pub use unused_definition_token::MarkdownLinkAliasUnusedDefinitionToken;

use crate::compiler::{
    markdown_link_or_alias::MarkdownLinkOrAliasToken, ParserEnum, TokenEnum,
};
use crate::data_object::ProblemLevel;
use crate::entity::compiler::prelude::*;
use std::collections::HashMap;
