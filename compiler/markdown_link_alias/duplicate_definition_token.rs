use super::*;

pub type MarkdownLinkAliasDuplicateDefinitionToken =
    TokenSource<MarkdownLinkAliasDuplicateDefinitionTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownLinkAliasDuplicateDefinitionTokenInner {
    pub range2: Range<usize>,
    pub alias_name: String,
}

impl Token for MarkdownLinkAliasDuplicateDefinitionTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &Some(ProblemData {
            message: format!(
                "Alias `{}` has already been defined",
                self.alias_name
            ),
            problem_level: ProblemLevel::Error,
        })
    }
}

impl From<MarkdownLinkAliasDuplicateDefinitionTokenInner> for TokenEnum {
    fn from(token: MarkdownLinkAliasDuplicateDefinitionTokenInner) -> Self {
        TokenEnum::MarkdownLinkAliasDuplicate(token)
    }
}

impl From<TokenEnum> for MarkdownLinkAliasDuplicateDefinitionTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownLinkAliasDuplicate(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636669076"))
            }
        }
    }
}
