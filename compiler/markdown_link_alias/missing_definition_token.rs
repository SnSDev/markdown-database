use super::*;

pub type MarkdownLinkAliasMissingDefinitionToken =
    TokenSource<MarkdownLinkAliasMissingDefinitionTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownLinkAliasMissingDefinitionTokenInner {
    pub alias_name: String,
}

impl MarkdownLinkAliasMissingDefinitionTokenInner {
    pub fn is_same_alias(&self, alias: &str) -> bool {
        self.alias_name == alias
    }
}

impl Token for MarkdownLinkAliasMissingDefinitionTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &Some(ProblemData {
            problem_level: ProblemLevel::Error,
            message: format!(
                "Unable to locate definition for Alias `{}`",
                self.alias_name
            ),
        })
    }
}

impl From<MarkdownLinkAliasMissingDefinitionTokenInner> for TokenEnum {
    fn from(token: MarkdownLinkAliasMissingDefinitionTokenInner) -> Self {
        TokenEnum::MarkdownLinkAliasMissingDefinition(token)
    }
}

impl From<TokenEnum> for MarkdownLinkAliasMissingDefinitionTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownLinkAliasMissingDefinition(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636589904"))
            }
        }
    }
}
