use super::*;

#[derive(Debug, PartialEq, Clone, Default)]
pub struct MarkdownLinkAliasParser {
    pub definition_hashmap: HashMap<String, Entry>,
    pub queued_token: Vec<TokenEnum>,
}

impl MarkdownLinkAliasParser {
    pub fn push_alias_definition(
        &mut self,
        token: MarkdownLinkAliasUnusedDefinitionToken,
    ) {
        let Entry {
            ref mut unused_definition,
            missing_definition,
        }: &mut Entry = self
            .definition_hashmap
            .entry(token.alias_name.clone())
            .or_default();

        if let Some(d) = unused_definition {
            let file_carrot = token.token_source().file_carrot();

            self.queued_token.push(
                MarkdownLinkAliasDuplicateDefinitionToken {
                    source: d.token_source().clone(),
                    range2: file_carrot.range.clone(),
                    alias_name: token.alias_name,
                }
                .into(),
            );
        } else {
            *unused_definition = Some(token.into());
        }
    }

    pub fn push_alias_use(
        &mut self,
        token: MarkdownLinkAliasMissingDefinitionToken,
        range: Range<usize>,
    ) {
        let alias = token.alias_name.clone();
        let &mut Entry {
            unused_definition,
            missing_definition,
        } = self.definition_hashmap.entry(alias).or_default();

        if missing_definition.is_none() {
            missing_definition = Some(token.into());
        }
    }

    pub fn parse_alias(
        &mut self,
        token: MarkdownLinkOrAliasToken,
    ) {
        match token {
            // TODO: Mailto [Label](mailto:user@domain.tld)
            MarkdownLinkOrAliasToken {
                // Alias Use [label][alias_name]
                source,
                labeless_path: None,
                label: Some(_label),
                path: None,
                alias_name: Some(alias_name),
                alias_path: None,
            } => {
                let range = source.file_carrot().range;
                self.push_alias_use(
                    MarkdownLinkAliasMissingDefinitionToken {
                        source,
                        alias_name,
                    },
                    range,
                );
            }
            MarkdownLinkOrAliasToken {
                // Alias Definition [label]: alias_path
                source,
                labeless_path: None,
                label: Some(alias_name),
                path: None,
                alias_name: None,
                alias_path: Some(_alias_path),
            } => self.push_alias_definition(
                MarkdownLinkAliasUnusedDefinitionToken { source, alias_name }
                    .into(),
            ),
            _ => (),
        }
    }
}

impl Parser<TokenEnum> for MarkdownLinkAliasParser {
    fn next_token(&mut self) -> Option<TokenEnum> {
        self.queued_token.pop()
    }

    fn parse_token(&mut self, token: &TokenEnum) {
        if let TokenEnum::MarkdownLinkOrAlias(t) = &token {
            self.parse_alias(t.clone());
        }
    }

    fn push_eof(&mut self, _eof: FileCaret) {
        self.queued_token
            .extend(self.definition_hashmap.drain().filter_map(
                |(_, entry)| {
                    match entry {
                        Entry {
                            unused_definition: Some(t),
                            missing_definition: None,
                        } => Some(t.into()),
                        Entry {
                            unused_definition: None,
                            missing_definition: Some(t),
                        } => Some(t.into()),
                        _ => None, // Ignore matches
                    }
                },
            ));
    }
}

impl From<MarkdownLinkAliasParser> for ParserEnum {
    fn from(parser: MarkdownLinkAliasParser) -> Self {
        ParserEnum::MarkdownLinkAlias(parser)
    }
}

impl From<ParserEnum> for MarkdownLinkAliasParser {
    fn from(parser: ParserEnum) -> Self {
        match parser {
            ParserEnum::MarkdownLinkAlias(p) => p,
            _ => {
                panic!(
                    "1636743032 - Unable to unwrap to specified tokenizer type"
                )
            }
        }
    }
}
