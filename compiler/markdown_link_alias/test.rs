use pretty_assertions::assert_eq;

use crate::{
    compiler::{
        markdown_link_alias::{
            MarkdownLinkAliasDuplicateDefinitionToken,
            MarkdownLinkAliasMissingDefinitionToken, MarkdownLinkAliasParser,
            MarkdownLinkAliasUnusedDefinitionToken,
        },
        markdown_link_or_alias::{
            MarkdownLinkOrAliasToken, MarkdownLinkOrAliasTokenizer,
        },
        new_coordinator_fn::new_coordinator,
        TokenEnum,
    },
    entity::{compiler::prelude::*, vec_xor},
};
use std::collections::HashMap;

#[test]
fn test1636667341_push_alias_definition() {
    let mut parser = MarkdownLinkAliasParser::default();

    assert_eq!(
        MarkdownLinkAliasParser {
            definition_hashmap: Default::default(),
            queued_token: Vec::new(),
        },
        parser
    );

    let range1 = 123..321;
    let alias_name = "Alias1636668095".to_string();
    let file_path = Rc::new(FilePath::memory_fs_factory().root);
    let first_token = MarkdownLinkAliasUnusedDefinitionToken {
        file_carrot: Some(FileCaret {
            range: range1.clone(),
            inner_range: None,
            file_path,
        }),
        alias_name: alias_name.clone(),
    };

    parser.push_alias_definition(first_token.clone());

    let mut expected = HashMap::new();
    expected.insert(alias_name.clone(), (Some(first_token), None));

    assert_eq!(
        MarkdownLinkAliasParser {
            definition_hashmap: expected.clone(),
            queued_token: Vec::new(),
        },
        parser
    );

    let range2 = 234..432;
    let second_token = MarkdownLinkAliasUnusedDefinitionToken {
        range: range2.clone(),
        alias_name: alias_name.clone(),
    };

    parser.push_alias_definition(second_token);

    assert_eq!(
        MarkdownLinkAliasParser {
            definition_hashmap: expected,
            queued_token: vec![MarkdownLinkAliasDuplicateDefinitionToken {
                range1,
                range2,
                alias_name
            }
            .into()],
        },
        parser
    );
}

#[test]
fn test1636922030_push_alias_use() {
    let mut parser = MarkdownLinkAliasParser::default();

    assert_eq!(MarkdownLinkAliasParser::default(), parser);

    let range1 = 123..321;
    let alias_name = "Alias1636922062".to_string();
    let first_token = MarkdownLinkAliasMissingDefinitionToken {
        range: range1,
        alias_name: alias_name.clone(),
    };

    parser.push_alias_use(first_token.clone());

    let mut expected = HashMap::new();
    expected.insert(alias_name.clone(), (None, Some(first_token)));

    assert_eq!(
        MarkdownLinkAliasParser {
            definition_hashmap: expected.clone(),
            queued_token: Vec::new(),
        },
        parser
    );

    let range2 = 234..432;
    let second_token = MarkdownLinkAliasMissingDefinitionToken {
        range: range2,
        alias_name,
    };

    parser.push_alias_use(second_token);

    assert_eq!(
        MarkdownLinkAliasParser {
            definition_hashmap: expected,
            queued_token: Vec::new(),
        },
        parser
    );
}

#[test]
fn test1636677209_parser() {
    let test_file = include_str!("./test_file.md").to_string();
    let mut coordinator = new_coordinator(
        vec![MarkdownLinkOrAliasTokenizer::default().into()],
        vec![MarkdownLinkAliasParser::default().into()],
        test_file,
    )
    .filter(|token| {
        matches!(
            token,
            TokenEnum::MarkdownLinkAliasDuplicate(_)
                | TokenEnum::MarkdownLinkAliasMissingDefinition(_)
                | TokenEnum::MarkdownLinkAliasUnused(_)
                | TokenEnum::MarkdownLinkOrAlias(_)
        )
    });

    let actual_tokens: Vec<TokenEnum> = coordinator.by_ref().take(8).collect();
    let mut error_tokens: Vec<TokenEnum> = coordinator.collect();

    assert_eq!(
        [
            TokenEnum::from(MarkdownLinkOrAliasToken {
                range: 61..94,
                labeless_path: Some(
                    "http://www.example.com/no-alias".to_string()
                ),
                label: None,
                path: None,
                alias_name: None,
                alias_path: None,
            }),
            MarkdownLinkOrAliasToken {
                range: 95..111,
                labeless_path: None,
                label: Some("Title1".to_string()),
                path: None,
                alias_name: Some("Alias1".to_string()),
                alias_path: None,
            }
            .into(),
            MarkdownLinkOrAliasToken {
                range: 112..128,
                labeless_path: None,
                label: Some("Title2".to_string()),
                path: None,
                alias_name: Some("Alias2".to_string()),
                alias_path: None,
            }
            .into(),
            MarkdownLinkOrAliasToken {
                range: 130..167,
                labeless_path: None,
                label: Some("Alias2".to_string()),
                path: None,
                alias_name: None,
                alias_path: Some("www.example.com/valid_alias".to_string()),
            }
            .into(),
            MarkdownLinkOrAliasToken {
                range: 168..207,
                labeless_path: None,
                label: Some("Alias3".to_string()),
                path: None,
                alias_name: None,
                alias_path: Some("www.example.com/unused_alias".to_string()),
            }
            .into(),
            MarkdownLinkOrAliasToken {
                range: 208..256,
                labeless_path: None,
                label: Some("Alias3".to_string()),
                path: None,
                alias_name: None,
                alias_path: Some(
                    "www.example.com/duplicate_definition".to_string()
                ),
            }
            .into(),
            MarkdownLinkAliasDuplicateDefinitionToken {
                range1: 168..207,
                range2: 208..256,
                alias_name: "Alias3".to_string(),
            }
            .into(),
            MarkdownLinkOrAliasToken {
                range: 258..299,
                labeless_path: None,
                label: Some("Title4".to_string()),
                path: Some("http://www.example.com/no_alias".to_string()),
                alias_name: None,
                alias_path: None,
            }
            .into(),
        ],
        actual_tokens
    );

    let mut expected = vec![
        MarkdownLinkAliasMissingDefinitionToken {
            range: 95..111,
            alias_name: "Alias1".to_string(),
        }
        .into(),
        MarkdownLinkAliasUnusedDefinitionToken {
            range: 168..207,
            alias_name: "Alias3".to_string(),
        }
        .into(),
    ];

    vec_xor(&mut expected, &mut error_tokens);

    assert_eq!(expected, error_tokens)
}
