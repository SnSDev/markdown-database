use super::*;

pub type MarkdownLinkAliasUnusedDefinitionToken =
    TokenSource<MarkdownLinkAliasUnusedDefinitionTokenInner>;

/// Token that may contain usage of alias built in [\LinkAliasLexer]
#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownLinkAliasUnusedDefinitionTokenInner {
    pub alias_name: String,
}

impl MarkdownLinkAliasUnusedDefinitionTokenInner {
    pub fn is_same_alias(&self, alias: &str) -> bool {
        self.alias_name == alias
    }
}

impl Token for MarkdownLinkAliasUnusedDefinitionTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &Some(ProblemData {
            problem_level: ProblemLevel::Warning,
            message: format!(
                "Unable to locate a use for Alias `{}`",
                self.alias_name
            ),
        })
    }
}

impl From<MarkdownLinkAliasUnusedDefinitionTokenInner> for TokenEnum {
    fn from(token: MarkdownLinkAliasUnusedDefinitionTokenInner) -> Self {
        TokenEnum::MarkdownLinkAliasUnused(token)
    }
}

impl From<TokenEnum> for MarkdownLinkAliasUnusedDefinitionTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownLinkAliasUnused(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636589899"))
            }
        }
    }
}
