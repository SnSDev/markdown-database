use std::rc::Rc;

use pretty_assertions::assert_eq;

use crate::{
    compiler::{
        markdown_heading::{
            MarkdownHeadingLevel, MarkdownHeadingToken,
            MarkdownHeadingTokenizer,
        },
        new_coordinator,
    },
    entity::compiler::prelude::*,
};

#[test]
fn test1636249854_tokenizer() {
    let test_file = include_str!("./test_file.md").to_string();
    let file_path = Rc::new(FilePath::memory_fs_factory().root);
    let coordinator = new_coordinator(
        vec![MarkdownHeadingTokenizer::default().into()],
        Vec::new(),
        test_file,
    );

    assert_eq!(
        [
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 0..23,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading1,
                caption: "Heading 1 A".to_owned()
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 25..48,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading2,
                caption: "Heading 2 A".to_owned(),
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 50..63,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading1,
                caption: "Heading 1 B".to_owned(),
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 64..78,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading2,
                caption: "Heading 2 B".to_owned(),
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 79..94,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading3,
                caption: "Heading 3 B".to_owned(),
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 95..111,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading4,
                caption: "Heading 4 B".to_owned(),
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 112..129,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading5,
                caption: "Heading 5 B".to_owned(),
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 130..148,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading6,
                caption: "Heading 6 B".to_owned(),
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 150..168,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading2,
                caption: "Heading 2 C".to_owned(),
            },
            MarkdownHeadingToken {
                file_carrot: Some(FileCaret {
                    range: 170..202,
                    inner_range: None,
                    file_path
                }),
                level: MarkdownHeadingLevel::Heading2,
                caption: "Heading 2 D".to_owned(),
            }
        ],
        coordinator
            .map(Into::into)
            .collect::<Vec<MarkdownHeadingToken>>()
    )
}
