use super::*;

#[derive(Debug, Clone, PartialEq)]
pub enum MarkdownHeadingLevel {
    Heading1,
    Heading2,
    Heading3,
    Heading4,
    Heading5,
    Heading6,
}

pub type MarkdownHeadingToken = TokenSource<MarkdownHeadingTokenInner>;

#[derive(Debug, Clone, PartialEq)]
pub struct MarkdownHeadingTokenInner {
    pub level: MarkdownHeadingLevel,
    pub caption: String,
}

impl Token for MarkdownHeadingTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl FromCaptures<TokenEnum> for MarkdownHeadingTokenInner {
    fn from_captures(
        regex_captures: Captures,
        range: Range<usize>,
        file_data: FileData,
    ) -> TokenEnum {
        let caption_pound = regex_captures.name(MATCH_CAPTION_POUND);
        let caption_underline = regex_captures.name(MATCH_CAPTION_UNDERLINE);

        let caption = { caption_pound.or(caption_underline).as_ref() }
            .or_else(|| panic!("{}", Self::error_valid_match("1636325590")))
            .map(Match::as_str)
            .map(Self::handle_markdown_whitespace)
            .unwrap();

        let level = match match (
            regex_captures.name(MATCH_LEVEL_POUND),
            regex_captures.name(MATCH_LEVEL_UNDERLINE),
        ) {
            (Some(m), None) => m,
            (None, Some(m)) => m,
            _ => unreachable!(&Self::error_valid_match("1636325632")),
        }
        .as_str()
        {
            "#" => MarkdownHeadingLevel::Heading1,
            "##" => MarkdownHeadingLevel::Heading2,
            "###" => MarkdownHeadingLevel::Heading3,
            "####" => MarkdownHeadingLevel::Heading4,
            "#####" => MarkdownHeadingLevel::Heading5,
            "######" => MarkdownHeadingLevel::Heading6,
            str if &str[0..1] == "=" => MarkdownHeadingLevel::Heading1,
            str if &str[0..1] == "-" => MarkdownHeadingLevel::Heading2,
            _ => unreachable!(&Self::error_valid_match("1636326381")),
        };

        Self { caption, level }
            .from_file(FileCaret {
                range: Self::range_from_captures(&regex_captures, &range),
                inner_range: None,
                file_data,
            })
            .into()
    }
}

impl From<MarkdownHeadingTokenInner> for TokenEnum {
    fn from(token: MarkdownHeadingTokenInner) -> Self {
        TokenEnum::MarkdownHeading(token)
    }
}

impl From<TokenEnum> for MarkdownHeadingTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownHeading(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636589892"))
            }
        }
    }
}
