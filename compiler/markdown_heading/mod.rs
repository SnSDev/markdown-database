mod token;
mod tokenizer;
mod tokenizer_regex;

#[cfg(test)]
mod test;

pub use token::{MarkdownHeadingLevel, MarkdownHeadingToken};
pub use tokenizer::MarkdownHeadingTokenizer;
pub use tokenizer_regex::MarkdownHeadingTokenizerRegex;

use crate::compiler::{TokenEnum, TokenizerEnum};
use crate::entity::compiler::prelude::*;

use tokenizer_regex::{
    MATCH_CAPTION_POUND, MATCH_CAPTION_UNDERLINE, MATCH_LEVEL_POUND,
    MATCH_LEVEL_UNDERLINE,
};
