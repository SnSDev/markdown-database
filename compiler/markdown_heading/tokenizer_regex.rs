use super::*;

const REG_EX: &str = r#"(((?P<level_pound>#{1,6})\W+(?P<caption_pound>.+))|((?P<caption_underline>.+)\n(?P<level_underline>={3,}|-{3,})))"#;
pub const MATCH_LEVEL_POUND: &str = "level_pound";
pub const MATCH_CAPTION_POUND: &str = "caption_pound";
pub const MATCH_CAPTION_UNDERLINE: &str = "caption_underline";
pub const MATCH_LEVEL_UNDERLINE: &str = "level_underline";

pub struct MarkdownHeadingTokenizerRegex {}

impl GetRegex for MarkdownHeadingTokenizerRegex {
    fn get_regex() -> Regex {
        Self::regex_from_const(REG_EX, "1636251657")
    }
}
