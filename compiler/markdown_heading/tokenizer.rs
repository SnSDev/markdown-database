use super::*;

pub type MarkdownHeadingTokenizer = RegexTokenizer<
    MarkdownHeadingTokenizerRegex,
    MarkdownHeadingToken,
    TokenEnum,
>;

impl From<MarkdownHeadingTokenizer> for TokenizerEnum {
    fn from(tokenizer: MarkdownHeadingTokenizer) -> Self {
        TokenizerEnum::MarkdownHeading(tokenizer)
    }
}

impl From<TokenizerEnum> for MarkdownHeadingTokenizer {
    fn from(tokenizer: TokenizerEnum) -> Self {
        match tokenizer {
            TokenizerEnum::MarkdownHeading(z) => z,
            _ => {
                panic!("{}", TokenizerEnum::from_panic("1636684921"))
            }
        }
    }
}
