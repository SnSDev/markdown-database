use crate::compiler::{
    datum::DatumToken,
    definition_list::DefinitionListPairToken,
    markdown_fenced_code_block::MarkdownFencedCodeBlockToken,
    markdown_heading::MarkdownHeadingToken,
    markdown_link::{
        MarkdownLinkInvalidToken, MarkdownLinkLocalFileToken,
        MarkdownLinkUrlToken,
    },
    markdown_link_alias::{
        MarkdownLinkAliasDuplicateDefinitionToken,
        MarkdownLinkAliasMissingDefinitionToken,
        MarkdownLinkAliasUnusedDefinitionToken,
    },
    markdown_link_or_alias::MarkdownLinkOrAliasToken,
    markdown_unordered_list::MarkdownUnorderedListItemToken,
    title::{TitleNotFoundToken, TitleToken},
    yaml_front_matter_block::YamlFrontMatterBlockToken,
};
use crate::entity::compiler::prelude::*;
use std::ops::Deref;

#[derive(Debug, PartialEq, Clone)]
pub enum TokenEnum {
    Datum(DatumToken),
    DefinitionListPair(DefinitionListPairToken),
    MarkdownFencedCodeBlock(MarkdownFencedCodeBlockToken),
    MarkdownHeading(MarkdownHeadingToken),
    MarkdownLinkAliasDuplicate(MarkdownLinkAliasDuplicateDefinitionToken),
    MarkdownLinkAliasMissingDefinition(MarkdownLinkAliasMissingDefinitionToken),
    MarkdownLinkAliasUnused(MarkdownLinkAliasUnusedDefinitionToken),
    MarkdownLinkInvalid(MarkdownLinkInvalidToken),
    MarkdownLinkLocalFile(MarkdownLinkLocalFileToken),
    MarkdownLinkOrAlias(MarkdownLinkOrAliasToken),
    MarkdownLinkUrl(MarkdownLinkUrlToken),
    MarkdownUnorderedListItem(MarkdownUnorderedListItemToken),
    Title(TitleToken),
    TitleNotFound(TitleNotFoundToken),
    YamlFrontMatterBlock(YamlFrontMatterBlockToken),
}

impl TokenEnum {
    pub const fn from_panic(error_number: &str) -> String {
        format!("{} - Code missing", error_number) // TODO 1641082565
    }
}

impl Token for TokenEnum {
    fn problem_data(&self) -> &Option<ProblemData> {
        self.deref().problem_data()
    }
}

impl Deref for TokenEnum {
    type Target = dyn Token;

    fn deref(&self) -> &Self::Target {
        match &self {
            TokenEnum::Datum(t) => t,
            TokenEnum::DefinitionListPair(t) => t,
            TokenEnum::MarkdownFencedCodeBlock(t) => t,
            TokenEnum::MarkdownHeading(t) => t,
            TokenEnum::MarkdownLinkAliasDuplicate(t) => t,
            TokenEnum::MarkdownLinkAliasMissingDefinition(t) => t,
            TokenEnum::MarkdownLinkAliasUnused(t) => t,
            TokenEnum::MarkdownLinkInvalid(t) => t,
            TokenEnum::MarkdownLinkLocalFile(t) => t,
            TokenEnum::MarkdownLinkOrAlias(t) => t,
            TokenEnum::MarkdownLinkUrl(t) => t,
            TokenEnum::MarkdownUnorderedListItem(t) => t,
            TokenEnum::Title(t) => t,
            TokenEnum::TitleNotFound(t) => t,
            TokenEnum::YamlFrontMatterBlock(t) => t,
        }
    }
}
