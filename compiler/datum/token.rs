use super::*;

pub type DatumToken = TokenSource<DatumTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct DatumTokenInner {
    pub namespace: Option<String>, // H2
    pub index: Option<usize>,      // unordered list item / table row
    pub key: Option<String>,       // key / table column
    pub value: String,
}

impl Token for DatumTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl From<DatumTokenInner> for TokenEnum {
    fn from(token: DatumTokenInner) -> Self {
        TokenEnum::Datum(token)
    }
}

impl From<TokenEnum> for DatumTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::Datum(t) => t,
            _ => {
                panic!("{} - {:?}", TokenEnum::from_panic("1638567392"), token) // TODO: 1636920754 Put Back
            }
        }
    }
}
