use std::ops::Range;

use pretty_assertions::assert_eq;

use crate::entity::compiler::{FileDataInner, Token};
use crate::wrapper::file_path::prelude::*;
use crate::{
    compiler::{
        datum::{DatumParser, DatumToken},
        definition_list::DefinitionListTokenizer,
        markdown_heading::MarkdownHeadingTokenizer,
        markdown_unordered_list::MarkdownUnorderedListTokenizer,
        new_coordinator,
        yaml_front_matter_block::YamlFrontMatterBlockTokenizer,
        ParserEnum, TokenEnum, TokenizerEnum,
    },
    entity::compiler::FileCaret,
};

#[test]
fn test1638569783_coordinator() {
    let test_file = include_str!("./test_file.md").to_string();
    let coordinator = new_coordinator(
        vec![
            TokenizerEnum::MarkdownHeading(MarkdownHeadingTokenizer::default()),
            TokenizerEnum::DefinitionList(DefinitionListTokenizer::default()),
            TokenizerEnum::MarkdownUnorderedList(
                MarkdownUnorderedListTokenizer::default(),
            ),
            TokenizerEnum::YamlFrontMatterBlock(
                YamlFrontMatterBlockTokenizer::default(),
            ),
        ],
        vec![ParserEnum::Datum(DatumParser::default())],
        test_file,
    );
    let root = FilePath::memory_fs_factory().root;
    let file_data = FileDataInner::from_opened_file(
        "".to_owned(),
        root.join("test_file").unwrap().into(),
    );
    let new_token = |range: Range<usize>,
                     inner_range: Option<Range<usize>>,
                     namespace: Option<String>,
                     index: Option<usize>,
                     key: Option<String>,
                     value: String|
     -> TokenEnum {
        DatumToken {
            namespace,
            index,
            key,
            value,
        }
        .from_file(FileCaret {
            range,
            inner_range,
            file_data,
        })
        .into()
    };

    assert_eq!(
        [
            new_token(
                98..124,
                None,
                None,
                None,
                Some("Key 1".to_string()),
                "Value 1 C".to_string(),
            ),
            new_token(
                125..146,
                None,
                None,
                None,
                Some("Key 2".to_string()),
                "Value 2 A".to_string(),
            ),
            new_token(
                147..169,
                None,
                None,
                Some(1),
                None,
                "Un-namespaced Item1".to_string(),
            ),
            new_token(
                169..191,
                None,
                None,
                Some(2),
                None,
                "Un-namespaced Item2".to_string(),
            ),
            new_token(
                394..418,
                None,
                Some("Namespace 1".to_string()),
                None,
                Some("key 2".to_string()),
                "Value 2 B".to_string(),
            ),
            new_token(
                419..438,
                None,
                Some("Namespace 1".to_string()),
                None,
                Some("Key 3".to_string()),
                "Value 3".to_string(),
            ),
            new_token(
                465..486,
                None,
                Some("Namespace 2".to_string()),
                None,
                Some("Key 1".to_string()),
                "Value 1 D".to_string(),
            ),
            new_token(
                488..507,
                None,
                Some("Namespace 2".to_string()),
                None,
                Some("Key 4".to_string()),
                "Value 4".to_string(),
            ),
            new_token(
                533..552,
                None,
                Some("Namespace 3".to_string()),
                Some(1),
                None,
                "Namespaced Item1".to_string(),
            ),
            new_token(
                552..571,
                None,
                Some("Namespace 3".to_string()),
                Some(2),
                None,
                "Namespaced Item2".to_string(),
            ),
        ],
        coordinator
            .filter(|token| { matches!(token, TokenEnum::Datum(_)) })
            .collect::<Vec<TokenEnum>>()
    );
}
