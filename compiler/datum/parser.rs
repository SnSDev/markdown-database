use super::*;

#[derive(Debug, Default)]
pub struct DatumParser {
    current_namespace: Option<String>,
    list_index: usize,
    pending_token: Option<TokenEnum>,
}

impl Parser<TokenEnum> for DatumParser {
    fn next_token(&mut self) -> Option<TokenEnum> {
        self.pending_token.take()
    }

    fn parse_token(&mut self, token: &TokenEnum) {
        match &token {
            TokenEnum::MarkdownHeading(t)
                if t.level == MarkdownHeadingLevel::Heading2 =>
            {
                self.current_namespace = Some(t.caption.to_owned());
                self.list_index = 0;
            }
            TokenEnum::DefinitionListPair(t) => {
                self.pending_token = Some(
                    DatumToken {
                        source: t.token_source().clone(),
                        namespace: self.current_namespace.clone(),
                        key: Some(t.key.clone()),
                        value: t.value.clone(),
                        index: None,
                    }
                    .into(),
                );
            }
            TokenEnum::MarkdownUnorderedListItem(t) => {
                self.list_index += 1;
                self.pending_token = Some(
                    DatumToken {
                        source: t.token_source().clone(),
                        namespace: self.current_namespace.clone(),
                        key: None,
                        value: t.content.clone(),
                        index: Some(self.list_index),
                    }
                    .into(),
                );
            }
            _ => (),
        }
    }

    fn push_eof(&mut self, _eof: FileCaret) {
        // Nothing to do
    }
}

impl From<DatumParser> for ParserEnum {
    fn from(parser: DatumParser) -> Self {
        ParserEnum::Datum(parser)
    }
}

impl From<ParserEnum> for DatumParser {
    fn from(parser: ParserEnum) -> Self {
        match parser {
            ParserEnum::Datum(z) => z,
            _ => {
                panic!("{}", ParserEnum::from_panic("1638569596"))
            }
        }
    }
}
