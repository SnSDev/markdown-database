use pretty_assertions::assert_eq;

use crate::compiler::{
    markdown_link_or_alias::{
        MarkdownLinkOrAliasToken, MarkdownLinkOrAliasTokenizer,
    },
    markdown_unordered_list::{
        MarkdownUnorderedListItemToken, MarkdownUnorderedListTokenizer,
    },
    new_coordinator, TokenEnum,
};
use crate::entity::compiler::prelude::*;

#[test]
fn test1636065452_find_next_dash_bullet() {
    let haystack = include_str!("./test_file.md");
    let range = 0..haystack.len();
    let tokenizer = MarkdownUnorderedListTokenizer::default();

    assert_eq!(
        MarkdownUnorderedListItemToken {
            range: 68..92,
            inner_range: 71..92,
            content: "test1\ntest2 test3".to_owned(),
        },
        tokenizer
            .find_next(&range, haystack)
            .map(MarkdownUnorderedListItemToken::from)
            .unwrap()
    );
}

#[test]
fn test1636145109_find_next_asterisk_bullet() {
    let haystack = include_str!("./test_file.md");
    let range = 70..haystack.len();
    let tokenizer = MarkdownUnorderedListTokenizer::default();

    assert_eq!(
        MarkdownUnorderedListItemToken {
            range: 99..109,
            inner_range: 104..109,
            content: "test5".to_owned(),
        },
        tokenizer
            .find_next(&range, haystack)
            .map(MarkdownUnorderedListItemToken::from)
            .unwrap()
    );
}

#[test]
fn test1636146346_find_next_none() {
    let haystack = include_str!("./test_file.md");
    let range = 115..haystack.len();
    let tokenizer = MarkdownUnorderedListTokenizer::default();

    assert_eq!(None, tokenizer.find_next(&range, haystack));
}

#[test]
fn test1636051382_tokenizer() {
    let test_file = include_str!("./test_file.md").to_string();
    let coordinator = new_coordinator(
        vec![MarkdownUnorderedListTokenizer::default().into()],
        Vec::new(),
        test_file,
    );

    let expected = vec![
        MarkdownUnorderedListItemToken {
            range: 68..92,
            inner_range: 71..92,
            content: "test1\ntest2 test3".to_owned(),
        },
        MarkdownUnorderedListItemToken {
            range: 99..109,
            inner_range: 104..109,
            content: "test5".to_owned(),
        },
        MarkdownUnorderedListItemToken {
            range: 109..121,
            inner_range: 116..121,
            content: "test6".to_owned(),
        },
    ];

    let actual = coordinator
        .map(Into::into)
        .collect::<Vec<MarkdownUnorderedListItemToken>>();

    assert_eq!(expected, actual);
}

#[test]
fn test1638395825_child_token() {
    let test_file = include_str!("./child_token_test_file.md").to_string();
    let coordinator = new_coordinator(
        vec![
            MarkdownUnorderedListTokenizer::default().into(),
            MarkdownLinkOrAliasTokenizer::default().into(),
        ],
        Vec::new(),
        test_file,
    );

    let expected = vec![
        TokenEnum::MarkdownUnorderedListItem(MarkdownUnorderedListItemToken {
            range: 44..94,
            inner_range: 47..94,
            content: "<http://example.com/1> (<http://example.com/2>)"
                .to_string(),
        }),
        MarkdownLinkOrAliasToken {
            range: 47..69,
            labeless_path: Some("http://example.com/1".to_string()),
            alias_name: None,
            alias_path: None,
            label: None,
            path: None,
        }
        .into(),
        MarkdownLinkOrAliasToken {
            range: 71..93,
            labeless_path: Some("http://example.com/2".to_string()),
            alias_name: None,
            alias_path: None,
            label: None,
            path: None,
        }
        .into(),
        MarkdownUnorderedListItemToken {
            range: 94..102,
            inner_range: 97..102,
            content: "test3".to_string(),
        }
        .into(),
    ];

    assert_eq!(expected, coordinator.collect::<Vec<TokenEnum>>())
}

#[test]
fn test1638403185_inner_range() {
    let test_file = include_str!("./child_token_test_file.md").to_string();
    let range = 0..test_file.len();
    let tokenizer = MarkdownUnorderedListTokenizer::default();

    let token = tokenizer.find_next(&range, &test_file).unwrap();

    assert_eq!(Some(&(47..94)), token.inner_range())
}
