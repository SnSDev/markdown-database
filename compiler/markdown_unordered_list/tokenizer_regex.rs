use super::*;

pub const REG_EX: &str =
    r#"\n *[-\*] (?P<Inner>[^-\* \n].*(\n *[^-\* \n].*)*)"#;
pub const MATCH_INNER: &str = "Inner";

pub struct MarkdownUnorderedListTokenizerRegex {}

impl GetRegex for MarkdownUnorderedListTokenizerRegex {
    fn get_regex() -> Regex {
        Self::regex_from_const(REG_EX, "1636058519")
    }
}
