use super::*;

pub type MarkdownUnorderedListItemToken =
    TokenSource<MarkdownUnorderedListItemTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownUnorderedListItemTokenInner {
    pub content: String,
}

impl Token for MarkdownUnorderedListItemTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl FromCaptures<TokenEnum> for MarkdownUnorderedListItemTokenInner {
    fn from_captures(
        regex_captures: Captures,
        range: Range<usize>,
        file_data: FileData,
    ) -> TokenEnum {
        let mut range = range;
        let start = range.start;
        let regex_match = regex_captures
            .get(0)
            .expect("1636240090 - Unreachable: Only matches should be passed");

        range.start = start + regex_match.start();
        range.end = start + regex_match.end();

        let inner = regex_captures.name(MATCH_INNER).unwrap_or_else(|| {
            panic!("{}", Self::error_valid_match("1638404167"))
        });

        let content = Some(inner.as_str())
            .map(Self::handle_markdown_whitespace)
            .unwrap();

        Self { content }
            .from_file(FileCaret {
                range: Self::range_from_captures(&regex_captures, &range),
                inner_range: Some(
                    (start + inner.start())..(start + inner.end()),
                ),
                file_data,
            })
            .into()
    }
}

impl From<MarkdownUnorderedListItemTokenInner> for TokenEnum {
    fn from(token: MarkdownUnorderedListItemTokenInner) -> Self {
        Self::MarkdownUnorderedListItem(token)
    }
}

impl From<TokenEnum> for MarkdownUnorderedListItemTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownUnorderedListItem(token) => token,
            _ => {
                panic!("1636589911 - Unable to unwrap to specified token type")
            }
        }
    }
}
