use super::*;

pub type MarkdownUnorderedListTokenizer = RegexTokenizer<
    MarkdownUnorderedListTokenizerRegex,
    MarkdownUnorderedListItemToken,
    TokenEnum,
>;

impl From<MarkdownUnorderedListTokenizer> for TokenizerEnum {
    fn from(tokenizer: MarkdownUnorderedListTokenizer) -> Self {
        Self::MarkdownUnorderedList(tokenizer)
    }
}

impl From<TokenizerEnum> for MarkdownUnorderedListTokenizer {
    fn from(tokenizer: TokenizerEnum) -> Self {
        match tokenizer {
            TokenizerEnum::MarkdownUnorderedList(z) => z,
            _ => {
                panic!(
                    "1636683063 - Unable to unwrap to specified tokenizer type"
                )
            }
        }
    }
}
