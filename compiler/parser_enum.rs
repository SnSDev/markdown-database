use super::*;

use crate::compiler::{
    datum::DatumParser, markdown_link::MarkdownLinkParser,
    markdown_link_alias::MarkdownLinkAliasParser, title::TitleParser,
};
use std::ops::{Deref, DerefMut};

pub enum ParserEnum {
    Datum(DatumParser),
    MarkdownLinkAlias(MarkdownLinkAliasParser),
    MarkdownLink(MarkdownLinkParser),
    Title(TitleParser),
}

impl Deref for ParserEnum {
    type Target = dyn Parser<TokenEnum>;

    fn deref(&self) -> &Self::Target {
        match self {
            Self::Datum(p) => p,
            Self::MarkdownLinkAlias(p) => p,
            Self::MarkdownLink(p) => p,
            Self::Title(p) => p,
        }
    }
}
impl DerefMut for ParserEnum {
    fn deref_mut(&mut self) -> &mut Self::Target {
        match self {
            Self::Datum(p) => p,
            Self::MarkdownLinkAlias(p) => p,
            Self::MarkdownLink(p) => p,
            Self::Title(p) => p,
        }
    }
}

impl Parser<TokenEnum> for ParserEnum {
    fn next_token(&mut self) -> Option<TokenEnum> {
        self.deref_mut().next_token()
    }

    fn parse_token(&mut self, token: &TokenEnum) {
        self.deref_mut().parse_token(token)
    }

    fn push_eof(&mut self, eof: FileCaret) {
        self.deref_mut().push_eof(eof)
    }
}

impl ParserEnum {
    pub fn from_panic(error_number: &str) -> String {
        format!(
            "{} - Unable to unwrap to specified parser type",
            error_number
        )
    }
}
