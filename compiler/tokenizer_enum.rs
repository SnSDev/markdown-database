use std::ops::Deref;

use super::*;
use crate::compiler::{
    definition_list::DefinitionListTokenizer,
    markdown_fenced_code_block::MarkdownFencedCodeBlockTokenizer,
    markdown_heading::MarkdownHeadingTokenizer,
    markdown_link_or_alias::MarkdownLinkOrAliasTokenizer,
    markdown_unordered_list::MarkdownUnorderedListTokenizer,
    yaml_front_matter_block::YamlFrontMatterBlockTokenizer,
};

pub enum TokenizerEnum {
    DefinitionList(DefinitionListTokenizer),
    MarkdownFencedCodeBlock(MarkdownFencedCodeBlockTokenizer),
    MarkdownHeading(MarkdownHeadingTokenizer),
    MarkdownLinkOrAlias(MarkdownLinkOrAliasTokenizer),
    MarkdownUnorderedList(MarkdownUnorderedListTokenizer),
    YamlFrontMatterBlock(YamlFrontMatterBlockTokenizer),
}

impl TokenizerEnum {
    pub fn from_panic(error_number: &str) -> String {
        format!(
            "{} - Unable to unwrap to specified tokenizer type",
            error_number
        )
    }
}

impl Deref for TokenizerEnum {
    type Target = dyn Tokenizer<TokenEnum>;

    fn deref(&self) -> &Self::Target {
        match self {
            Self::DefinitionList(z) => z,
            Self::MarkdownFencedCodeBlock(z) => z,
            Self::MarkdownHeading(z) => z,
            Self::MarkdownLinkOrAlias(z) => z,
            Self::MarkdownUnorderedList(z) => z,
            Self::YamlFrontMatterBlock(z) => z,
        }
    }
}

impl Tokenizer<TokenEnum> for TokenizerEnum {
    fn find_next(
        &self,
        range: &Range<usize>,
        file_data: FileData,
    ) -> Option<TokenEnum> {
        self.deref().find_next(range, file_data)
    }
}
