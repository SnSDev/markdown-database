use super::*;

pub type MarkdownLinkOrAliasTokenizer = RegexTokenizer<
    MarkdownLinkOrAliasTokenizerRegex,
    MarkdownLinkOrAliasToken,
    TokenEnum,
>;

impl From<MarkdownLinkOrAliasTokenizer> for TokenizerEnum {
    fn from(tokenizer: MarkdownLinkOrAliasTokenizer) -> Self {
        Self::MarkdownLinkOrAlias(tokenizer)
    }
}

impl From<TokenizerEnum> for MarkdownLinkOrAliasTokenizer {
    fn from(tokenizer: TokenizerEnum) -> Self {
        match tokenizer {
            TokenizerEnum::MarkdownLinkOrAlias(z) => z,
            _ => {
                panic!(
                    "1636683063 - Unable to unwrap to specified tokenizer type"
                )
            }
        }
    }
}
