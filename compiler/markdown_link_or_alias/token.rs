use super::*;

pub type MarkdownLinkOrAliasToken = TokenSource<MarkdownLinkOrAliasTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownLinkOrAliasTokenInner {
    pub labeless_path: Option<String>,
    pub label: Option<String>,
    pub path: Option<String>,
    pub alias_name: Option<String>,
    pub alias_path: Option<String>,
}

impl Token for MarkdownLinkOrAliasTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl FromCaptures<TokenEnum> for MarkdownLinkOrAliasTokenInner {
    fn from_captures(
        regex_captures: Captures,
        range: Range<usize>,
        file_data: FileData,
    ) -> TokenEnum {
        let match_to_string =
            |name: &str| Self::match_to_string(&regex_captures, name);

        Self {
            labeless_path: match_to_string(MATCH_LABELESS_PATH),
            label: match_to_string(MATCH_LABEL),
            path: match_to_string(MATCH_PATH),
            alias_name: match_to_string(MATCH_ALIAS_NAME),
            alias_path: match_to_string(MATCH_ALIAS_PATH),
        }
        .from_file(FileCaret {
            range: Self::range_from_captures(&regex_captures, &range),
            inner_range: None,
            file_data,
        })
        .into()
    }
}

impl From<MarkdownLinkOrAliasTokenInner> for TokenEnum {
    fn from(token: MarkdownLinkOrAliasTokenInner) -> Self {
        TokenEnum::MarkdownLinkOrAlias(token)
    }
}

impl From<TokenEnum> for MarkdownLinkOrAliasTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownLinkOrAlias(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636754112"))
            }
        }
    }
}
