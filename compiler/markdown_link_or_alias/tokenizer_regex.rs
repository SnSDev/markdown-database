use super::*;

pub const REG_EX: &str = r"(?P<Link><(?P<LabelessPath>[^\s]*://.*?)>|!?\[(?P<Label>.*?)\](?:\((?P<Path>.*?)\)|\[(?P<AliasName>.*?)\]|\s:\s*(?P<AliasPath>.+)))";
pub const MATCH_LINK: &str = "Link";
pub const MATCH_LABELESS_PATH: &str = "LabelessPath";
pub const MATCH_LABEL: &str = "Label";
pub const MATCH_PATH: &str = "Path";
pub const MATCH_ALIAS_NAME: &str = "AliasName";
pub const MATCH_ALIAS_PATH: &str = "AliasPath";

pub struct MarkdownLinkOrAliasTokenizerRegex {}

impl GetRegex for MarkdownLinkOrAliasTokenizerRegex {
    fn get_regex() -> Regex {
        Self::regex_from_const(REG_EX, "1636753872")
    }
}
