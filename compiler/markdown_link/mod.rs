mod invalid_token;
mod local_file_token;
mod parser;
mod url_token;

#[cfg(test)]
mod test;

pub use invalid_token::MarkdownLinkInvalidToken;
pub use local_file_token::MarkdownLinkLocalFileToken;
pub use parser::MarkdownLinkParser;
pub use url_token::MarkdownLinkUrlToken;

use crate::compiler::{ParserEnum, TokenEnum};
use crate::data_object::ProblemLevel;
use crate::entity::{compiler::prelude::*, Presenter, Url};
use crate::wrapper::file_path::prelude::*;
use percent_encoding::percent_decode_str;
use std::borrow::Borrow;
