use super::*;

pub type MarkdownLinkInvalidToken = TokenSource<MarkdownLinkInvalidTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownLinkInvalidTokenInner {
    pub path: String,
    pub error: String,
}

impl Token for MarkdownLinkInvalidTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &Some(ProblemData {
            problem_level: ProblemLevel::Error,
            message: format!(
                "Unable to parse path\n\"{}\"\ndue to error:\n\"{}\".",
                self.path, self.error
            ),
        })
    }
}

impl From<MarkdownLinkInvalidTokenInner> for TokenEnum {
    fn from(token: MarkdownLinkInvalidTokenInner) -> Self {
        TokenEnum::MarkdownLinkInvalid(token)
    }
}

impl From<TokenEnum> for MarkdownLinkInvalidTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownLinkInvalid(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636748434"))
            }
        }
    }
}
