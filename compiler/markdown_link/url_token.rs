use super::*;

pub type MarkdownLinkUrlToken = TokenSource<MarkdownLinkUrlTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownLinkUrlTokenInner {
    pub url: Url,
}

impl Token for MarkdownLinkUrlTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl From<MarkdownLinkUrlTokenInner> for TokenEnum {
    fn from(token: MarkdownLinkUrlTokenInner) -> Self {
        TokenEnum::MarkdownLinkUrl(token)
    }
}

impl From<TokenEnum> for MarkdownLinkUrlTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownLinkUrl(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636589914"))
            }
        }
    }
}
