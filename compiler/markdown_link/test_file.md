Test Markdown Link Parser File
==============================

Used Alias
----------

[AliasUsed][AliasName1630272938]

[AliasName1630272938] : http://www.example.com/1630273060

Data Url
--------

[1627794511Embedded](data:image/png;base64,1627794511=)

Unlabeled Url
-------------

<http://www.example.com/1633035550>

Local File With Spaces
----------------------

[Label](local%20file.md)
