use super::*;
use crate::compiler::{
    markdown_link_or_alias::MarkdownLinkOrAliasToken, TokenEnum,
};

#[derive(Debug)]
pub struct MarkdownLinkParser {
    pub token_buffer_vec: Vec<TokenEnum>,
    pub file_data: FileData,
}

impl MarkdownLinkParser {
    pub fn new(file_data: FileData) -> Self {
        Self {
            token_buffer_vec: Default::default(),
            file_data,
        }
    }
}

impl Parser<TokenEnum> for MarkdownLinkParser {
    fn next_token(&mut self) -> Option<TokenEnum> {
        self.token_buffer_vec.pop()
    }

    fn parse_token(&mut self, token: &TokenEnum) {
        let link_or_alias_token = match &token {
            TokenEnum::MarkdownLinkOrAlias(token) => token,
            _ => return,
        };

        let (caption, path) = match &link_or_alias_token {
            MarkdownLinkOrAliasToken {
                source: _,
                labeless_path: Some(path),
                label: None,
                path: None,
                alias_name: None,
                alias_path: None,
            } => (None, path),
            MarkdownLinkOrAliasToken {
                source: _,
                labeless_path: None,
                label: Some(label),
                path: Some(path),
                alias_name: None,
                alias_path: None,
            }
            | MarkdownLinkOrAliasToken {
                source: _,
                labeless_path: None,
                label: Some(label),
                path: None,
                alias_name: None,
                alias_path: Some(path),
            } => (Some(label.to_string()), path),
            _ => return,
        };

        match Url::parse(path) {
            Ok(url) if url.scheme() == "data" => {
                return; // Ignore data urls
            }
            Ok(url) => {
                self.token_buffer_vec.push(
                    MarkdownLinkUrlToken {
                        source: link_or_alias_token.token_source().clone(),
                        url,
                    }
                    .into(),
                );
                return;
            }
            Err(url::ParseError::RelativeUrlWithoutBase) => (),
            Err(e) => {
                self.token_buffer_vec.push(
                    MarkdownLinkInvalidToken {
                        source: link_or_alias_token.token_source().clone(),
                        path: path.to_owned(),
                        error: format!("{:?}", e),
                    }
                    .into(),
                );
                return;
            }
        }

        match self
            .file_data
            .borrow_parent_dir()
            .join(percent_decode_str(path).decode_utf8_lossy().borrow())
        {
            Ok(file_path) => {
                self.token_buffer_vec.push(
                    MarkdownLinkLocalFileToken {
                        source: link_or_alias_token.token_source().clone(),
                        file_path,
                        caption,
                    }
                    .into(),
                );
            }
            Err(e) => {
                self.token_buffer_vec.push(
                    MarkdownLinkInvalidToken {
                        source: link_or_alias_token.token_source().clone(),
                        path: path.to_owned(),
                        error: format!("{:?}", e),
                    }
                    .into(),
                );
            }
        }
    }

    fn push_eof(&mut self, _eof: FileCaret) {
        // Nothing to do
    }
}

impl From<MarkdownLinkParser> for ParserEnum {
    fn from(parser: MarkdownLinkParser) -> Self {
        ParserEnum::MarkdownLink(parser)
    }
}

impl From<ParserEnum> for MarkdownLinkParser {
    fn from(parser: ParserEnum) -> Self {
        match parser {
            ParserEnum::MarkdownLink(p) => p,
            _ => {
                panic!("{}", ParserEnum::from_panic("1636929223"))
            }
        }
    }
}
