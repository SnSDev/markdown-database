use super::*;

pub type MarkdownLinkLocalFileToken =
    TokenSource<MarkdownLinkLocalFileTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownLinkLocalFileTokenInner {
    pub file_path: FilePath,
    pub caption: Option<String>,
}

impl MarkdownLinkLocalFileTokenInner {
    pub fn into_markdown_string<'a, P: Presenter<'a, FilePath>>(
        &'a self,
        presenter: &'a P,
    ) -> String {
        let rendered_path = presenter.render(&self.file_path);
        if let Some(caption) = &self.caption {
            return format!("[{}]({})", caption, rendered_path);
        }
        return format!("<{}>", rendered_path);
    }
}

impl Token for MarkdownLinkLocalFileTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl From<MarkdownLinkLocalFileTokenInner> for TokenEnum {
    fn from(token: MarkdownLinkLocalFileTokenInner) -> Self {
        TokenEnum::MarkdownLinkLocalFile(token)
    }
}

impl From<TokenEnum> for MarkdownLinkLocalFileTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownLinkLocalFile(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636589907"))
            }
        }
    }
}

impl WritableToken for MarkdownLinkLocalFileTokenInner {}
