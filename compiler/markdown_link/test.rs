use std::rc::Rc;

use pretty_assertions::assert_eq;

use crate::compiler::{
    markdown_link::{
        MarkdownLinkLocalFileToken, MarkdownLinkParser, MarkdownLinkUrlToken,
    },
    markdown_link_or_alias::{
        MarkdownLinkOrAliasToken, MarkdownLinkOrAliasTokenizer,
    },
    new_coordinator, TokenEnum,
};
use crate::entity::{compiler::prelude::*, Url};
use crate::wrapper::file_path::prelude::*;

#[test]
fn test1636749851_parser() {
    let test_file = include_str!("./test_file.md").to_string();
    let factory =
        FilePath::memory_fs_factory().add_file("test_file.md", "", "");
    let root_path = factory.root;
    let base_path = factory.shortcut_hashmap.remove("/test_file.md").unwrap();
    let coordinator = new_coordinator(
        vec![MarkdownLinkOrAliasTokenizer::default().into()],
        vec![MarkdownLinkParser::new(base_path.clone()).into()],
        test_file,
    );
    let file_path = Rc::new(FilePath::memory_fs_factory().finalize());

    assert_eq!(
        [
            MarkdownLinkOrAliasToken {
                labeless_path: None,
                label: Some("AliasUsed".to_string()),
                path: None,
                alias_name: Some("AliasName1630272938".to_string()),
                alias_path: None,
            }
            .into()
            .into_from_file(86..118, None, file_path),
            MarkdownLinkOrAliasToken {
                labeless_path: None,
                label: Some("AliasName1630272938".to_string()),
                path: None,
                alias_name: None,
                alias_path: Some(
                    "http://www.example.com/1630273060".to_string()
                ),
            }
            .into()
            .into_from_file(120..177, None, file_path),
            MarkdownLinkUrlToken {
                url: Url::parse("http://www.example.com/1630273060").unwrap(),
            }
            .into()
            .into_from_file(120..177, None, file_path),
            MarkdownLinkOrAliasToken {
                labeless_path: None,
                label: Some("1627794511Embedded".to_string()),
                path: Some("data:image/png;base64,1627794511=".to_string()),
                alias_name: None,
                alias_path: None,
            }
            .into()
            .into_from_file(198..253, None, file_path),
            MarkdownLinkOrAliasToken {
                labeless_path: Some(
                    "http://www.example.com/1633035550".to_string()
                ),
                label: None,
                path: None,
                alias_name: None,
                alias_path: None,
            }
            .into()
            .into_from_file(284..319, None, file_path),
            MarkdownLinkUrlToken {
                url: Url::parse("http://www.example.com/1633035550").unwrap(),
            }
            .into()
            .into_from_file(284..319, None, file_path),
            MarkdownLinkOrAliasToken {
                labeless_path: None,
                label: Some("Label".to_string()),
                path: Some("local%20file.md".to_string()),
                alias_name: None,
                alias_path: None,
            }
            .into()
            .into_from_file(368..392, None, file_path),
            MarkdownLinkLocalFileToken {
                file_path: base_path.join("local file.md").unwrap(),
                caption: Some("Label".to_string())
            }
            .into()
            .into_from_file(368..392, None, file_path),
        ],
        coordinator.collect::<Vec<TokenEnum>>()
    );
}

#[test]
fn test1640046191_new_writable_token() {
    // Given
    let file_path = FilePath::new(MemoryFS::default());
    let caption = "Test Caption 1640046322";

    // When
    let token = MarkdownLinkLocalFileToken::new(
        file_path.clone(),
        Some(caption.to_owned()),
    );

    // Then
    let _: &dyn WritableToken = &token;
    assert_eq!(
        MarkdownLinkLocalFileToken {
            file_path,
            caption: Some(caption.to_string()),
        },
        token
    );
}
