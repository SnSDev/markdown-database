use super::*;

pub type YamlFrontMatterBlockToken =
    TokenSource<YamlFrontMatterBlockTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct YamlFrontMatterBlockTokenInner {
    pub metadata: Vec<YamlKeyValuePair>, // Not HashMap (yet) b/c of `level` field and future bulleted list support
}

impl Token for YamlFrontMatterBlockTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl FromCaptures<TokenEnum> for YamlFrontMatterBlockTokenInner {
    fn from_captures(
        regex_captures: Captures,
        range: Range<usize>,
        file_data: FileData,
    ) -> TokenEnum {
        Self {
            metadata: YamlKeyValuePair::parse_yaml_data(
                regex_captures
                    .name(MATCH_METADATA)
                    .map(|m| m.as_str())
                    .unwrap_or(""),
            ),
        }
        .from_file(FileCaret {
            range: Self::range_from_captures(&regex_captures, &range),
            inner_range: None,
            file_data,
        })
        .into()
    }
}

impl From<YamlFrontMatterBlockTokenInner> for TokenEnum {
    fn from(token: YamlFrontMatterBlockTokenInner) -> Self {
        Self::YamlFrontMatterBlock(token)
    }
}

impl From<TokenEnum> for YamlFrontMatterBlockTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::YamlFrontMatterBlock(token) => token,
            _ => {
                panic!("1636589917 - Unable to unwrap to specified token type")
            }
        }
    }
}

impl YamlFrontMatterBlockTokenInner {
    pub fn find_title(&self) -> Option<String> {
        self.metadata
            .iter()
            .find(|YamlKeyValuePair { depth, key, value }| {
                key.to_uppercase() == "TITLE"
            })
            .map(|token| token.value.to_owned())
    }
}

// TODO: Write test for find_title
