use pretty_assertions::assert_eq;

use crate::compiler::{
    new_coordinator_fn::new_coordinator,
    yaml_front_matter_block::{
        YamlFrontMatterBlockToken, YamlFrontMatterBlockTokenizer,
        YamlKeyValuePair,
    },
    TokenEnum,
};
use crate::wrapper::FilePath;
use std::rc::Rc;

#[test]
fn test1636407504_tokenizer_invalid_file() {
    let test_file = include_str!("./test_file_invalid.md").to_string();
    let coordinator = new_coordinator(
        vec![YamlFrontMatterBlockTokenizer::default().into()],
        Vec::new(),
        test_file,
    );

    assert_eq!(
        Vec::<YamlFrontMatterBlockToken>::new(),
        coordinator
            .map(Into::into)
            .collect::<Vec<YamlFrontMatterBlockToken>>()
    )
}

#[test]
fn test1633804859_tokenizer_valid_file() {
    let test_file = include_str!("./test_file_valid.md").to_string();
    let coordinator = new_coordinator(
        vec![YamlFrontMatterBlockTokenizer::default().into()],
        Vec::new(),
        test_file,
    );
    let file_path = Rc::new(FilePath::memory_fs_factory().root);

    assert_eq!(
        [YamlFrontMatterBlockToken {
            metadata: vec![YamlKeyValuePair {
                depth: 0,
                key: "Title".to_string(),
                value: "Valid Metadata Test Page".to_string(),
            }],
        }
        .into()
        .into_from_file(0..40, None, file_path)],
        coordinator.collect::<Vec<TokenEnum>>()
    )
}

#[test]
fn test1638667106_tokenizer() {
    // Test Environment

    // Given
    let yaml_file = include_str!("./test_file.yaml");

    // When
    let actual: Vec<YamlKeyValuePair> =
        YamlKeyValuePair::parse_yaml_data(yaml_file.to_string());

    // Then
    assert_eq!(
        [
            YamlKeyValuePair {
                depth: 0,
                key: "title".to_string(),
                value: "My Blog".to_string(),
            },
            YamlKeyValuePair {
                depth: 0,
                key: "description".to_string(),
                value: "Like I said, my blog!".to_string(),
            },
            YamlKeyValuePair {
                depth: 1,
                key: "name".to_string(),
                value: "John Smith".to_string(),
            },
            YamlKeyValuePair {
                depth: 1,
                key: "age".to_string(),
                value: "33".to_string(),
            },
            YamlKeyValuePair {
                depth: 0,
                key: "morePeople".to_string(),
                value: "{name: Grace Jones, age: 21}".to_string(),
            },
        ],
        actual
    )
}
