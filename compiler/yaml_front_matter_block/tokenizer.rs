use super::*;

pub const MATCH_METADATA: &str = "metadata";
const REG_EX: &str = r#"(?m)^---\n(?P<metadata>[\s\S]*?)\n---\n"#;

#[derive(Debug, Default)]
pub struct YamlFrontMatterBlockTokenizer {}

impl Tokenizer<TokenEnum> for YamlFrontMatterBlockTokenizer {
    fn find_next<'a>(
        &self,
        range: &Range<usize>,
        file_data: FileData,
    ) -> Option<TokenEnum> {
        if range.start != 0 {
            return None; // YamlFrontMatterBlockToken can only exist at the
                         // start of a file
        }

        // Regex is only definable once per file, so no internal storage is
        // needed
        let captures = regex_from_const(REG_EX, "1636408599")
            .captures(&file_data.borrow_contents()[range.clone()]);

        captures
            .filter(|c| {
                c.get(0)
                    .expect(
                        "1636410360 - Unreachable: All matches should have 0 \
                         index",
                    )
                    .start()
                    == 0
            })
            .map(|c| {
                YamlFrontMatterBlockToken::from_captures(
                    c,
                    range.clone(),
                    file_data,
                )
                .into()
            });
    }
}

impl From<YamlFrontMatterBlockTokenizer> for TokenizerEnum {
    fn from(tokenizer: YamlFrontMatterBlockTokenizer) -> Self {
        Self::YamlFrontMatterBlock(tokenizer)
    }
}

impl From<TokenizerEnum> for YamlFrontMatterBlockTokenizer {
    fn from(tokenizer: TokenizerEnum) -> Self {
        match tokenizer {
            TokenizerEnum::YamlFrontMatterBlock(z) => z,
            _ => {
                panic!(
                    "1636685560 - Unable to unwrap to specified tokenizer type"
                )
            }
        }
    }
}
