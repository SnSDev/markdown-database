use super::*;

pub const REG_EX: &str =
    r#"(?m)^(?P<Depth> *)(?P<Key>[^\n: ][^\n:]*): (?P<Value>.*)$"#;
pub const MATCH_KEY: &str = "Key";
pub const MATCH_VALUE: &str = "Value";
pub const MATCH_DEPTH: &str = "Depth";

#[derive(Debug, PartialEq, Clone, serde::Serialize)]
pub struct YamlKeyValuePair {
    pub depth: usize,
    pub key: String,
    pub value: String,
}

impl<'a> From<Captures<'a>> for YamlKeyValuePair {
    fn from(regex_captures: Captures<'a>) -> Self {
        let key = regex_captures
            .name(MATCH_KEY)
            .expect("1638668377 - Unreachable: Already Matched")
            .as_str()
            .to_owned();

        let value = regex_captures
            .name(MATCH_VALUE)
            .expect("1638668414 - - Unreachable: Already Matched")
            .as_str()
            .to_owned();

        let depth = { regex_captures.name(MATCH_DEPTH) }
            .map_or(0, |m| m.as_str().len() / 2);

        Self { depth, key, value }
    }
}

impl YamlKeyValuePair {
    pub fn parse_yaml_data(yaml_file: &str) -> Vec<Self> {
        todo!("1640132943")
    }
}
