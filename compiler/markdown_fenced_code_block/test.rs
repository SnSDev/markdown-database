use pretty_assertions::assert_eq;

use crate::entity::compiler::{FileCaret, FileDataInner};
use crate::wrapper::file_path::prelude::*;
use crate::{
    compiler::{
        markdown_fenced_code_block::{
            MarkdownFencedCodeBlockToken, MarkdownFencedCodeBlockTokenizer,
        },
        new_coordinator, TokenEnum,
    },
    entity::compiler::Token,
};

#[test]
fn test1634270216_tokenizer() {
    let test_file = include_str!("./test_file.md").to_string();
    let coordinator = new_coordinator(
        vec![MarkdownFencedCodeBlockTokenizer::default().into()],
        Vec::new(),
        test_file,
    );

    let file_data = FileDataInner::from_opened_file(
        "".to_string(),
        FilePath::memory_fs_factory().root,
    );

    assert_eq!(
        [
            TokenEnum::from(
                MarkdownFencedCodeBlockToken {
                    language: None,
                    code_block: "code1".to_owned(),
                }
                .from_file(FileCaret {
                    range: 148..162,
                    inner_range: None,
                    file_data,
                },)
            ),
            MarkdownFencedCodeBlockToken {
                language: Some("language2".to_owned()),
                code_block: "code2\nmulti-line code".to_owned(),
            }
            .from_file(FileCaret {
                range: 248..287,
                inner_range: None,
                file_data,
            },)
            .into(),
            MarkdownFencedCodeBlockToken {
                language: Some("multi word language".to_owned()),
                code_block: "code3".to_owned(),
            }
            .from_file(FileCaret {
                range: 395..428,
                inner_range: None,
                file_data,
            },)
            .into(),
        ],
        coordinator.collect::<Vec<TokenEnum>>()
    )
}
