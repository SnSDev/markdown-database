mod token;
mod tokenizer;
mod tokenizer_regex;

#[cfg(test)]
mod test;

pub use token::MarkdownFencedCodeBlockToken;
pub use tokenizer::MarkdownFencedCodeBlockTokenizer;
pub use tokenizer_regex::MarkdownFencedCodeBlockTokenizerRegex;

use crate::compiler::{TokenEnum, TokenizerEnum};
use crate::entity::compiler::prelude::*;

use tokenizer_regex::{MATCH_CODE, MATCH_LANGUAGE};
