use super::*;

pub type MarkdownFencedCodeBlockToken =
    TokenSource<MarkdownFencedCodeBlockTokenInner>;

#[derive(Debug, PartialEq, Clone)]
pub struct MarkdownFencedCodeBlockTokenInner {
    pub language: Option<String>,
    pub code_block: String,
}

impl Token for MarkdownFencedCodeBlockTokenInner {
    fn problem_data(&self) -> &Option<ProblemData> {
        &None
    }
}

impl FromCaptures<TokenEnum> for MarkdownFencedCodeBlockTokenInner {
    fn from_captures(
        regex_captures: Captures,
        range: Range<usize>,
        file_data: FileData,
    ) -> TokenEnum {
        let language = regex_captures
            .name(MATCH_LANGUAGE)
            .map(|m| m.as_str().to_owned());

        let code_block = regex_captures
            .name(MATCH_CODE)
            .unwrap_or_else(|| {
                panic!("{}", Self::error_valid_match("1636403862"))
            })
            .as_str()
            .to_owned();

        Self {
            language,
            code_block,
        }
        .from_file(FileCaret {
            range: Self::range_from_captures(&regex_captures, &range),
            inner_range: None,
            file_data,
        })
        .into()
    }
}

impl From<MarkdownFencedCodeBlockTokenInner> for TokenEnum {
    fn from(token: MarkdownFencedCodeBlockTokenInner) -> Self {
        TokenEnum::MarkdownFencedCodeBlock(token)
    }
}

impl From<TokenEnum> for MarkdownFencedCodeBlockTokenInner {
    fn from(token: TokenEnum) -> Self {
        match token {
            TokenEnum::MarkdownFencedCodeBlock(t) => t,
            _ => {
                panic!("{}", TokenEnum::from_panic("1636589890"))
            }
        }
    }
}
