use super::*;

pub const MATCH_LANGUAGE: &str = "language";
pub const MATCH_CODE: &str = "code";
const REG_EX: &str =
    r#"(?m)^```(?P<language>[^`\n].*)?\n(?P<code>[\s\S]*?)\n```\n"#;

pub struct MarkdownFencedCodeBlockTokenizerRegex {}

impl GetRegex for MarkdownFencedCodeBlockTokenizerRegex {
    fn get_regex() -> Regex {
        Self::regex_from_const(REG_EX, "1636402438")
    }
}
