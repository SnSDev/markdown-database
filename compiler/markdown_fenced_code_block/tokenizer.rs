use super::*;

pub type MarkdownFencedCodeBlockTokenizer = RegexTokenizer<
    MarkdownFencedCodeBlockTokenizerRegex,
    MarkdownFencedCodeBlockToken,
    TokenEnum,
>;

impl From<MarkdownFencedCodeBlockTokenizer> for TokenizerEnum {
    fn from(tokenizer: MarkdownFencedCodeBlockTokenizer) -> Self {
        TokenizerEnum::MarkdownFencedCodeBlock(tokenizer)
    }
}

impl From<TokenizerEnum> for MarkdownFencedCodeBlockTokenizer {
    fn from(tokenizer: TokenizerEnum) -> Self {
        match tokenizer {
            TokenizerEnum::MarkdownFencedCodeBlock(z) => z,
            _ => {
                panic!("{}", TokenizerEnum::from_panic("1636685260"))
            }
        }
    }
}
