/// The severity of a [crate::data_object::CompileProblem]
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ProblemLevel {
    /// Unable to complete the compilation
    Error,
    /// Source is not up-to-standard, not preventing compilation
    Warning,
    /// Suggested change to source (e.g. Spelling Error)
    Info,
}
