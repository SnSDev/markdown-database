use crate::compiler::TokenEnum; // TODO: Should be removed

use super::*;

/// An error / warning / problem from a compiler / parser
///
/// Modeled after Language Server Protocol.  Eventually this application
/// will implement this standard so it can be integrated into VSCode.
///
/// <https://github.com/Microsoft/language-server-protocol>
///
/// Example output from [Ledger](https://www.ledger-cli.org/)
///
/// ```text
/// Warning: "/home/emeraldinspirations/Dropbox/Pim/Financial/2021/2021-08.ledger", line 65: Unknown account 'A:Cash:Petty'
/// ```
#[derive(Debug, PartialEq, Clone)]
pub struct CompileProblem {
    pub problem_level: ProblemLevel,
    pub file_data: FileData,
    pub position_in_file: PositionInFile,
    pub message: String,
}

impl CompileProblem {
    pub fn from_problem_token(
        ProblemData {
            message,
            problem_level,
        }: ProblemData,
        token: TokenEnum,
    ) -> CompileProblem {
        let file_carrot = token.token_source().file_carrot();
        let file_data = file_carrot.file_data;

        CompileProblem {
            problem_level,
            file_data,
            position_in_file: file_data
                .calculate_position_in_file(file_carrot.range.start),
            message,
        }
    }
}
