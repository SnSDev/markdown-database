mod data_object;

pub use data_object::MarkdownLink;

use crate::wrapper::file_path::prelude::*;
use url::Url;
