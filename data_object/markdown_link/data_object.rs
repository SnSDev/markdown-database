use super::*;

/// The contents of a link in a markdown file
///
/// The contents may be:
/// - a URL [Url]
/// - a path to a local file [FilePath]
#[derive(Debug, PartialEq, Clone)]
pub enum MarkdownLink {
    Url(Url),
    LocalFile(FilePath),
    None,
}

impl From<Url> for MarkdownLink {
    fn from(url: Url) -> Self {
        Self::Url(url)
    }
}

impl From<FilePath> for MarkdownLink {
    fn from(path: FilePath) -> Self {
        Self::LocalFile(path)
    }
}

impl MarkdownLink {
    pub fn is_none(&self) -> bool {
        self == &Self::None
    }
}
