/// The position of a token in a file
#[derive(PartialEq, Clone, Copy, Debug, Default)]
pub struct PositionInFile {
    pub line_number: usize,
    pub character: usize,
}

impl PositionInFile {
    pub const fn new(line_number: usize, character: usize) -> Self {
        Self {
            line_number,
            character,
        }
    }
}
