//! Structure or Enum used for data storage (i.e. no business logic)
//!
//! The only code included in a `data_object` is:
//! - Validation (assuming the validation is done entirely inside the
//!   `data_object` without accessing system resources, other crates, persistent
//!   storage, or any other part of the project.
//! - Transformation to/from other `data_object`
//!
//! A `data_object` should be used in more than one `entity`, `use_case`,
//! 'gateway' etc.  If only used in one place it should be in the same module.

// mod compile_problem;
// mod markdown_link;
// mod position_in_file;
mod tokenizer_response_rank;

// pub use compile_problem::{CompileProblem, ProblemLevel};
// pub use markdown_link::MarkdownLink;
// pub use position_in_file::PositionInFile;
// pub use url::Url;
pub use tokenizer_response_rank::TokenizerResponseRank;
