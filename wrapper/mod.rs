//! Add additional functionality to a dependency
//!
//! Typically this code would be in a respective [`crate::plug_in`], but the
//! dependency has an exception to being decoupled from the source
//! code. (See [Coupled Dependencies](`crate::port#coupled-dependencies`))

// #[doc(hidden)]
// pub mod file_path;

// pub use file_path::FilePath;
