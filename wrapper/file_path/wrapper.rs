use super::*;

/// Add ability to [VfsPath] to be hashed
///
/// - - -
///
/// Add ability to [VfsPath] to output the relative path to a second path
///
/// Problem
/// =======
///
/// No method in [VfsPath] to get a relative path string
///
/// Solution
/// ========
///
/// Use a trait to add this functionality and implement it for [VfsPath] inside
/// the same module.
///
/// - - -
///
/// Add ability to [VfsPath] to parse both absolute and relative paths
///
/// Problem 1
/// =========
///
/// The code:
///
/// ```ignore
/// VfsPath::join(path, "/absolute/path.file")
/// ```
///
/// will fail because [VfsPath] does not support absolute paths.
///
/// Problem 2
/// =========
///
/// [VfsPath] does not have a `root()` function, or equivalent
///
/// Solution
/// ========
///
/// Use a trait to add this functionality and implement it for [VfsPath] inside
/// the same module.
#[derive(Debug, Clone, Eq)]
pub struct FilePath {
    path_string: String,
    inner: VfsPath,
}

impl Hash for FilePath {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.path_string.hash(state);
    }
}

impl PartialEq for FilePath {
    fn eq(&self, other: &Self) -> bool {
        self.path_string == other.path_string
    }
}

impl From<VfsPath> for FilePath {
    fn from(file_path: VfsPath) -> Self {
        Self {
            path_string: file_path.as_str().to_owned(),
            inner: file_path,
        }
    }
}

impl From<FilePath> for VfsPath {
    fn from(hashable_file_path: FilePath) -> Self {
        hashable_file_path.inner
    }
}

impl PartialOrd for FilePath {
    fn partial_cmp(
        &self,
        other: &Self,
    ) -> std::option::Option<std::cmp::Ordering> {
        self.path_string.partial_cmp(&other.path_string)
    }
}

impl Ord for FilePath {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.path_string.cmp(&other.path_string)
    }
}

impl Deref for FilePath {
    type Target = VfsPath;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl FilePath {
    pub fn memory_fs_factory() -> VfsMemoryFSFactory {
        VfsMemoryFSFactory::default()
    }

    pub fn from_vec(vec: Vec<VfsPath>) -> Vec<FilePath> {
        vec.into_iter().map(FilePath::from).collect()
    }

    pub fn into_vec(vec: Vec<FilePath>) -> Vec<VfsPath> {
        vec.into_iter().map(FilePath::into).collect()
    }

    pub fn sort_iter<I: Iterator<Item = VfsPath>>(iter: I) -> Vec<VfsPath> {
        Self::sort_vec(iter.collect())
    }

    pub fn sort_vec(vec: Vec<VfsPath>) -> Vec<VfsPath> {
        let mut sortable_vec = Self::from_vec(vec);
        sortable_vec.sort();
        Self::into_vec(sortable_vec)
    }

    pub fn join(&self, path: &str) -> Result<Self, VfsError> {
        self.inner.join(path).map(Into::into)
    }

    pub fn parent(&self) -> Option<Self> {
        self.inner.parent().map(Into::into)
    }

    /// Take a path (relative or absolute) and apply it to the existing path
    ///
    /// Example
    /// -------
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::wrapper::vfs_path_parse_relative_path::test_prelude::*;
    /// let TestEnvironment { root, base_dir, .. } = TestEnvironment::default();
    ///
    /// // Given
    /// vec![
    ///     TestCase {
    ///         relative_path: "",
    ///         expected: Err(VfsError::InvalidPath {
    ///             path: "".to_owned(),
    ///         }),
    ///     },
    ///     TestCase {
    ///         relative_path: "/A/local_file.image",
    ///         expected: Ok(root.join("A/local_file.image").unwrap()),
    ///     },
    ///     TestCase {
    ///         relative_path: "B/local_file.image",
    ///         expected: Ok(root
    ///             .join("tmp/1629257040/B/local_file.image")
    ///             .unwrap()),
    ///     },
    ///     TestCase {
    ///         relative_path: "./C/local_file.image",
    ///         expected: Ok(root
    ///             .join("tmp/1629257040/C/local_file.image")
    ///             .unwrap()),
    ///     },
    ///     TestCase {
    ///         relative_path: "../D/local_file.image",
    ///         expected: Ok(root.join("tmp/D/local_file.image").unwrap()),
    ///     },
    ///     TestCase {
    ///         relative_path: "../../E/local_file.image",
    ///         expected: Ok(root.join("E/local_file.image").unwrap()),
    ///     },
    ///     TestCase {
    ///         relative_path: "../../../F/local_file.image",
    ///         expected: Err(VfsError::InvalidPath {
    ///             path: "../../../F/local_file.image".to_string(),
    ///         }),
    ///     },
    ///     TestCase {
    ///         relative_path: "~/G/local_file.image",
    ///         expected: Err(VfsError::InvalidPath {
    ///             path: "~/G/local_file.image".to_string(),
    ///         }),
    ///     },
    ///     TestCase {
    ///         relative_path: "H/I/./J//K/L/../M/local_file.image",
    ///         expected: Err(VfsError::InvalidPath {
    ///             path: "H/I/./J//K/L/../M/local_file.image".to_string(),
    ///         }),
    ///     },
    /// ]
    /// .into_iter()
    /// .for_each(
    ///     |TestCase {
    ///          relative_path,
    ///          expected,
    ///      }| {
    ///
    /// // When
    /// let actual = base_dir.parse_relative_path(relative_path);
    ///
    /// // Then
    ///         assert_eq!(
    ///             ComparableResults::from(expected),
    ///             ComparableResults::from(actual)
    ///         );
    ///     },
    /// );
    /// ```
    fn parse_relative_path(
        &self,
        relative_path: &str,
    ) -> Result<Self, VfsError> {
        // Empty Path not caught by `vfs`
        if relative_path.is_empty() {
            return Err(VfsError::InvalidPath {
                path: "".to_string(),
            });
        }

        // Absolute path not supported by `vfs`
        if let Some(absolute_path) = relative_path.strip_prefix('/') {
            return Self::from(self.go_to_root())
                .parse_relative_path(absolute_path);
        }

        // Home based paths not caught by `vfs`
        if relative_path.starts_with('~') {
            return Err(VfsError::InvalidPath {
                path: relative_path.to_string(),
            });
        }

        self.join(relative_path)
    }

    /// Traverse up the file tree to the root directory
    ///
    /// Example
    /// =======
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::wrapper::vfs_path_parse_relative_path::test_prelude::*;
    /// let TestEnvironment { root, .. } = TestEnvironment::default();
    ///
    /// // Given
    /// let base_dir = root.join("some/place/in/the/file/system").unwrap();
    ///
    /// // When
    /// let actual = base_dir.go_to_root();
    ///
    /// // Then
    /// assert_eq!(root, actual)
    /// ```
    fn go_to_root(&self) -> Self {
        if let Some(parent) = self.parent() {
            parent.go_to_root()
        } else {
            self.clone()
        }
        // TODO: See if this works
        // self.parent().map_or_else(|| self.clone(), FilePath::go_to_root)
    }

    /// Calculate the relative path from the supplied [VfsPath] this [VfsPath]
    ///
    /// Example
    /// -------
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::wrapper::VfsPathToRelativePath;
    /// use markdown_database_lib::wrapper::file_path::prelude::*;
    ///
    /// let memory_fs = MemoryFS::default();
    /// let root = VfsPath::new(memory_fs);
    ///
    /// let from_path = root.join("level1/level2/original/base_dir").unwrap();
    ///
    /// struct TestCase<'a> {
    ///     to_path: &'a str,
    ///     expected: &'a str,
    /// }
    ///
    /// // Given
    /// vec![
    ///     TestCase {
    ///         to_path: "level1/level2/original/base_dir",
    ///         expected: ".",
    ///     },
    ///     TestCase {
    ///         to_path: "level1/level2/divergent/file.name",
    ///         expected: "../../divergent/file.name",
    ///     },
    ///     TestCase {
    ///         to_path: "level1/level2/original/base_dir/child",
    ///         expected: "./child",
    ///     },
    /// ]
    /// .into_iter()
    /// .for_each(|TestCase { to_path, expected }| {
    ///     let new_path = root.join(to_path).unwrap();
    ///
    /// // When
    ///     let actual = new_path.relative_to(&from_path);
    ///
    /// // Then
    ///     assert_eq!(expected.to_owned(), actual)
    /// });
    /// ```
    fn relative_to(&self, base: &Self) -> String {
        let target = self.to_node_vec();
        let base = base.to_node_vec();

        let prefix_count =
            base.iter().zip(&target).take_while(|(a, b)| a == b).count();
        let mut ret =
            { std::iter::repeat("..").take(base.len() - prefix_count) }
                .chain(target[prefix_count..].iter().map(String::as_str))
                // Iterator::intersperse would be nice, but it's unstable
                .flat_map(|str| IntoIterator::into_iter([str, "/"]))
                .collect::<String>();
        ret.pop();

        if ret.is_empty() {
            return ".".to_string();
        }

        if !ret.starts_with('.') {
            return format!("./{}", ret);
        }

        ret
    }

    /// Build [Vec<String>] with each node in [VfsPath]
    ///
    /// Example
    /// =======
    ///
    /// ```
    /// // Test Environment
    /// use markdown_database_lib::wrapper::file_path::prelude::*;
    ///
    /// struct TestCase<'a> {
    ///     path: VfsPath,
    ///     expected: Vec<&'a str>,
    /// }
    /// let root = VfsPath::new(MemoryFS::default());
    ///
    /// // Given
    /// vec![
    ///     TestCase {
    ///         path: root.clone(),
    ///         expected: Vec::new(),
    ///     },
    ///     TestCase {
    ///         path: root.join("test").unwrap(),
    ///         expected: vec!["test"],
    ///     },
    ///     TestCase {
    ///         path: root.join("test/file/path").unwrap(),
    ///         expected: vec!["test", "file", "path"],
    ///     },
    /// ]
    /// .iter()
    /// .for_each(|TestCase { path, expected }| {
    ///
    /// // When
    ///     let actual = path.to_node_vec();
    ///
    /// // Then
    ///     assert_eq!(
    ///         actual,
    ///         expected
    ///             .iter()
    ///             .map(|&str| String::from(str))
    ///             .collect::<Vec<String>>()
    ///     )
    /// });
    /// ```
    fn to_node_vec(&self) -> Vec<String> {
        let mut result = Vec::new();

        let mut node = self.clone();
        while let Some(parent) = node.parent() {
            result.push(node.filename());
            node = parent;
        }
        result.reverse();

        result
    }
}

impl serde::Serialize for FilePath {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.path_string)
    }
}
