use super::*;

pub struct VfsMemoryFSFactory {
    pub root: FilePath,
    pub shortcut_hashmap: HashMap<String, FilePath>,
}

impl Default for VfsMemoryFSFactory {
    fn default() -> Self {
        Self {
            root: VfsPath::new(MemoryFS::default()).into(),
            shortcut_hashmap: Default::default(),
        }
    }
}

impl VfsMemoryFSFactory {
    pub fn add_dir(mut self, dir_name: &str, parent_path: &str) -> Self {
        let parent = self.borrow_path(parent_path);
        let new_dir = parent
            .join(dir_name)
            .expect("1639801246 - Unable to create new dir path");
        new_dir
            .create_dir()
            .expect("1639803227 - Unable to create new dir");

        self.insert_shortcut(dir_name, parent_path, new_dir.into());

        self
    }

    pub fn add_file(
        mut self,
        file_name: &str,
        parent_path: &str,
        content: &str,
    ) -> Self {
        let parent = self.borrow_path(parent_path);
        let new_file = parent
            .join(file_name)
            .expect("1639801467 - Unable to create new file path");
        write!(
            new_file
                .create_file()
                .expect("1639801508 - Unable to create new file")
                .as_mut(),
            "{}",
            content
        )
        .expect("1639801528 - Unable to write to new file");

        self.insert_shortcut(file_name, parent_path, new_file.into());

        self
    }

    pub fn insert_shortcut(
        &mut self,
        name: &str,
        parent_path: &str,
        path: FilePath,
    ) {
        self.shortcut_hashmap
            .insert(format!("{}/{}", parent_path, name), path);
    }

    pub fn borrow_path(&self, path: &str) -> &FilePath {
        if path.is_empty() {
            &self.root
        } else {
            self.shortcut_hashmap
                .get(path)
                .expect("1639801117 - Path not found")
        }
    }

    pub fn finalize<E: From<HashMap<String, FilePath>>>(self) -> E {
        E::from(self.shortcut_hashmap)
    }
}
