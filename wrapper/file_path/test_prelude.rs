pub use crate::wrapper::file_path::prelude::*;

pub struct TestEnvironment {
    pub root: FilePath,
    pub base_dir: FilePath,
}

impl Default for TestEnvironment {
    fn default() -> Self {
        let root = FilePath::new(MemoryFS::default());
        let base_dir = root.join("tmp/1629257040").unwrap();

        Self { root, base_dir }
    }
}

// TODO: crate::wrapper::vfs_error_partial_eq;
#[derive(Debug)]
pub enum ComparableResults {
    Ok(FilePath),
    Err(VfsError),
}

impl From<Result<FilePath, VfsError>> for ComparableResults {
    fn from(result: Result<FilePath, VfsError>) -> Self {
        match result {
            Ok(p) => Self::Ok(p),
            Err(e) => Self::Err(e),
        }
    }
}

/// [vfs::VfsError] does not derive [std::PartialEq]
impl PartialEq for ComparableResults {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Ok(l0), Self::Ok(r0)) => l0 == r0,
            (
                Self::Err(VfsError::InvalidPath { path: l_path }),
                Self::Err(VfsError::InvalidPath { path: r_path }),
            ) => l_path == r_path,
            _ => false,
        }
    }
}

pub struct TestCase<'a> {
    pub relative_path: &'a str,
    pub expected: Result<FilePath, VfsError>,
}
