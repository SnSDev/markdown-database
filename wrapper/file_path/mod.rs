mod memory_fs_factory;
mod wrapper;

#[doc(hidden)]
pub mod test_prelude;

pub use crate::wrapper::file_path as prelude;
pub use memory_fs_factory::VfsMemoryFSFactory;
pub use vfs::*;
pub use wrapper::FilePath;

use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::ops::Deref;
