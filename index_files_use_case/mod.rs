mod error;
mod file_path_to_link_iter;
mod request_model;
mod response_model;
mod use_case;

#[cfg(test)]
mod test;

pub use error::IndexFilesError;
pub use file_path_to_link_iter::FilePathToLinkIter;
pub use request_model::IndexFilesRequestModel;
pub use response_model::IndexFilesResponseModel;
pub use use_case::index_files;

pub type IndexFilesResult = Result<IndexFilesResponseModel, IndexFilesError>;

// use crate::compiler::markdown_link::MarkdownLinkLocalFileToken;
use crate::wrapper::file_path::prelude::*;
