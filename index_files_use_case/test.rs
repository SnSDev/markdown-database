use crate::wrapper::file_path::prelude::*;
use pretty_assertions::assert_eq;

use crate::index_files_use_case::{
    index_files, IndexFilesError, IndexFilesRequestModel,
    IndexFilesResponseModel,
};

#[test]
fn test1638832275_primary_course() {
    // Test Environment
    let root = FilePath::new(MemoryFS::default());
    let base_dir = root.join("test_directory_name").unwrap();
    base_dir.create_dir().unwrap();
    let file1 = base_dir.join("test_file1.md").unwrap();
    let file2 = base_dir.join("test_file2.md").unwrap();
    let dir3 = base_dir.join("dir3").unwrap();
    let dir4 = base_dir.join("dir4").unwrap();
    dir3.create_dir().unwrap();
    dir4.create_dir().unwrap();
    let file3 = dir3.join("index.md").unwrap();
    let file_ignore_me = dir4.join("anything.md").unwrap();

    // Given
    { file1.create_file().unwrap().as_mut() }
        .write_all(include_str!("test_file1.md").as_bytes())
        .unwrap();
    { file2.create_file().unwrap().as_mut() }
        .write_all(include_str!("test_file2.md").as_bytes())
        .unwrap();
    { file3.create_file().unwrap().as_mut() }
        .write_all(include_str!("./test_file3.md").as_bytes())
        .unwrap();
    (file_ignore_me.create_file().unwrap().as_mut())
        .write_all("".as_bytes())
        .unwrap();

    // When
    let request_model = IndexFilesRequestModel {
        base_dir,
        override_index: false,
    };

    // Then
    assert_eq!(
        Ok(IndexFilesResponseModel {
            new_file: r#"---
title: test_directory_name
---

- [Title3](./dir3/index.md)
- [Title1](./test_file1.md)
- [Title2](./test_file2.md)
"#
            .to_string(),
        }),
        index_files(request_model)
    )
}

#[test]
fn test1638834191_file_exists() {
    // Test Environment
    let root = FilePath::new(MemoryFS::default());
    let base_dir = root.join("test_directory_name").unwrap();
    base_dir.create_dir().unwrap();

    // Given
    let file1 = base_dir.join("index.md").unwrap();
    file1.create_file().unwrap();

    // When
    let request_model = IndexFilesRequestModel {
        base_dir,
        override_index: false,
    };

    // Then
    assert_eq!(
        Err(IndexFilesError::IndexFileAlreadyExists),
        index_files(request_model)
    )
}

#[test]
fn test1638834217_title_missing() {
    // Test Environment
    let root = FilePath::new(MemoryFS::default());
    let base_dir = root.join("test_directory_name").unwrap();
    base_dir.create_dir().unwrap();

    // Given
    let file3 = base_dir.join("test_file_no_title.md").unwrap();
    write!(
        file3.create_file().unwrap().as_mut(),
        include_str!("test_file_no_title.md")
    )
    .unwrap();

    // When
    let request_model = IndexFilesRequestModel {
        base_dir,
        override_index: false,
    };

    // Then
    assert_eq!(
        Err(IndexFilesError::TitleMissing(vec![file3])),
        index_files(request_model)
    )
}
