use super::*;

pub struct IndexFilesRequestModel {
    pub base_dir: FilePath,
    pub override_index: bool,
}
