use super::*;

pub fn index_files(
    IndexFilesRequestModel {
        base_dir,
        override_index,
    }: IndexFilesRequestModel,
) -> IndexFilesResult {
    let index_path = base_dir.join("index.md").expect(
        "1638927121 - Unreachable: Valid path attached to pre-verified \
         directory",
    );

    if !override_index
        && index_path.exists().expect(
            "1638927205 - Unreachable: Valid path either should exist or not",
        )
    {
        return Err(IndexFilesError::IndexFileAlreadyExists);
    }

    todo!("1640128657")

    // let file_list = list_files(ListFilesRequestModel {
    //     base_dir,
    //     for_index: true,
    // })?;

    // let link_iter = FilePathToLinkIter { iter: file_list };

    // let new_file_token_iter = set_title(SetTitleRequestModel {
    //     title: TitleType::YamlFrontMatterBlock(base_dir.filename()),
    //     token_iter: Vec::new().into_iter(),
    // })?;

    // let new_file_with_metadata = insert_metadata(InsertMetadataRequestModel {
    //     key: "WARNING".to_owned(),
    //     value: "File Auto-Generated. To update: run markdown-database index \
    //             --force in same directory"
    //         .to_owned(),
    //     token_iter: new_file_token_iter,
    // })?;

    // let new_file_with_data = insert_data(InsertDataRequestModel {
    //     new_data: link_iter,
    //     namespace: None,
    //     token_iter: new_file_with_metadata,
    // })?;

    // write_file(WriteFileRequestModel {
    //     file_path: index_path,
    //     token_iter: new_file_with_data,
    //     overwrite_file: true,
    // })?
    // .into()
}
