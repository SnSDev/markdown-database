use crate::compiler::markdown_link::MarkdownLinkLocalFileToken;

use super::*;

pub struct FilePathToLinkIter<I: Iterator<Item = FilePath>> {
    iter: I,
}

impl<I: Iterator<Item = FilePath>> Iterator for FilePathToLinkIter<I> {
    type Item = Result<MarkdownLinkLocalFileToken, IndexFilesError>;

    fn next(&mut self) -> Option<Self::Item> {
        let path = self.iter.next()?;

        todo!("1640128683")

        // let request_model = match ParseFileRequestModel::try_from(path) {
        //     Ok(r) => r,
        //     Err(e) => return Some(Err(IndexFilesError::from(e))),
        // };

        // let token_iterator = match parse_file(request_model) {
        //     Ok(i) => i,
        //     Err(e) => return Some(Err(IndexFilesError::from(e))),
        // };

        // let title = match parse_title(ParseTitleRequestModel { token_iterator })
        // {
        //     Ok(t) => t,
        //     Err(e) => Some(Err(IndexFilesError::from(e))),
        // };

        // Some(MarkdownLinkLocalFileToken::new(
        //     file_path,
        //     Some(title.title),
        // ))
    }
}
