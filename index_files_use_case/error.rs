use super::*;

#[derive(Debug, PartialEq)]
pub enum IndexFilesError {
    IndexFileAlreadyExists,
    TitleMissing(Vec<FilePath>),
}
