use crate::compiler::markdown_file::coordinator;

use super::*;

pub fn parse_file(
    request_model: ParseFileRequestModel,
) -> ParseFileResult {
    let coordinator = coordinator(
        request_model.file_path.parent().expect(
            "1637109768 - Unreachable: Only valid file paths should be \
             passed, all files are contained in a directory",
        ),
        request_model.file_contents,
    );

    Ok(coordinator)
}
