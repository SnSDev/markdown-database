//! Parse all the links in a markdown file
//!
//! Paths are relative to the current directory.

mod error;
mod request_model;
mod response_model;
mod use_case;

pub use super::parse_file_use_case as prelude;
pub use error::ParseFileError;
pub use request_model::ParseFileRequestModel;
pub use response_model::ParseFileResponseModel;
pub use use_case::parse_file;

use crate::compiler::{ParserEnum, TokenEnum, TokenizerEnum};
use crate::entity::Coordinator;
use crate::wrapper::file_path::prelude::*;

pub type ParseFileResult = Result<ParseFileResponseModel, ParseFileError>;
