use super::*;

pub struct ParseFileRequestModel {
    pub file_contents: String,
    pub file_path: FilePath,
}

impl TryFrom<FilePath> for ParseFileRequestModel {
    type Error = VfsError;

    fn try_from(file_path: FilePath) -> Result<Self, Self::Error> {
        Ok(Self {
            file_contents: file_path.read_to_string()?,
            file_path,
        })
    }
}
