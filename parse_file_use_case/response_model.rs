use super::*;

pub type ParseFileResponseModel =
    Coordinator<TokenEnum, TokenizerEnum, ParserEnum>;
