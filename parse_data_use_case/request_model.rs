use super::*;

pub struct ParseDataRequestModel {
    pub file_contents: String,
    pub file_path: FilePath,
}
