use super::*;

pub struct ParseDataResponseModel {
    pub coordinator: Coordinator<TokenEnum, TokenizerEnum, ParserEnum>,
}

impl Iterator for ParseDataResponseModel {
    type Item = TokenEnum;

    fn next(&mut self) -> Option<Self::Item> {
        self.coordinator.by_ref().find_map(|token| match token {
            TokenEnum::Datum(t) => Some(t.into()),
            TokenEnum::Title(t) => Some(t.into()),
            TokenEnum::YamlFrontMatterBlock(t) if t.find_title().is_some() => {
                Some(t.into())
            }
            _ => None,
        })
    }
}
