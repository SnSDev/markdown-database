mod error;
mod request_model;
mod response_model;
mod use_case;

pub use super::parse_data_use_case as prelude;
pub use error::ParseDataError;
pub use request_model::ParseDataRequestModel;
pub use response_model::ParseDataResponseModel;
pub use use_case::parse_data;

use crate::compiler::{
    markdown_file::coordinator, ParserEnum, TokenEnum, TokenizerEnum,
};
use crate::entity::Coordinator;
use crate::wrapper::file_path::prelude::*;

pub const SHORT_DESCRIPTION: &str = "Parse the data in a markdown file";

pub const LONG_DESCRIPTION: &str = r#"
Parse all the data elements in the specified markdown file
"#;

pub const COMMAND_NAME: &str = "data";

pub type ParseDataResult = Result<ParseDataResponseModel, ParseDataError>;
