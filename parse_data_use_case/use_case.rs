use super::*;

pub fn parse_data(request_model: &ParseDataRequestModel) -> ParseDataResult {
    let coordinator = coordinator(
        request_model.file_path.clone(),
        request_model.file_contents.clone(),
    );

    Ok(ParseDataResponseModel { coordinator })
}
