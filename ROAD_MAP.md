Roadmap
=======

- [x] Deploy MVP - ([Issue](https://gitlab.com/SnSDev/markdown-database/-/issues/3))
- [x] Only display "Parsed" or "Verified" if `-v` / `-verbose` flag set
  ([Issue](https://gitlab.com/SnSDev/markdown-database/-/issues/12))
- [ ] Summary at end of `CRAWL`
- [ ] `.gitignore` as filter by default
- [ ] error for orphaned markdown files
- [ ] `--no-orphans` tag to error on ANY orphaned file
- [ ] Windows support
- [ ] OSX support
- [ ] Parse indexable data in markdown files
- [ ] Parse index file metadata and fill contents
- [ ] `CREATE <Template>` opens text editor with contents of template,
      updates indexes
- [ ] `ASSERT <key> = <value>, ...` returns updates needed and prompts to
      auto-update
- [ ] `DELETE <NodeId>` deletes node and updates index
- [ ] `INDEX <NodeType> BY <key>` creates and updates index file
- [ ] Build query language
- [ ] Integrate into Language Server Protocol
- [ ] Add FFI support for all use-cases

See the
[open issues](https://gitlab.com/SnSDev/markdown-database/-/issues)
for a full list of proposed features (and known issues).
