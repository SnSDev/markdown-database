mod error;
mod request_model;
mod response_model;
mod use_case;

#[cfg(test)]
mod test;

pub use error::SetTitleError;
pub use request_model::SetTitleRequestModel;
pub use response_model::SetTitleResponseModel;
pub use use_case::set_title;

pub type SetTitleResult = Result<SetTitleResponseModel, SetTitleError>;
