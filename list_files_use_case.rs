//! Recursively list all files/folders in the current directory
//!
//! Paths are relative to the current directory.
//!
//! Currently the only files/folders filtered out are:
//! - Any starting with ".git"

use std::path::Path;

use vfs::{VfsError, VfsPath, WalkDirIterator};

/// Allow glob import of [`list_files`] and support items
pub mod prelude {
    pub use crate::list_files_use_case::{
        list_files, RequestModel as ListFilesRequestModel,
        ResponseModel as ListFilesResponseModel,
    };
}

/// TODO
pub struct DirWalker {
    walk_dir_iter: WalkDirIterator,
}

/// TODO
impl DirWalker {
    /// TODO
    ///
    /// # Errors
    /// // TODO
    pub fn try_new(base_dir: &VfsPath) -> Result<Self, VfsError> {
        Ok(Self {
            walk_dir_iter: base_dir.walk_dir()?,
        })
    }
}

impl Iterator for DirWalker {
    type Item = VfsPath;

    fn next(&mut self) -> Option<Self::Item> {
        self.walk_dir_iter.next()?.ok().or_else(|| self.next())
    }
}

/// TODO
pub enum WalkerEnum {
    /// TODO
    RecursiveWalker(RecursiveWalker),
    /// TODO
    ForIndexWalker(ForIndexWalker),
}

impl Iterator for WalkerEnum {
    type Item = VfsPath;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Self::RecursiveWalker(i) => i.next(),
            Self::ForIndexWalker(i) => i.next(),
        }
    }
}

impl From<RecursiveWalker> for WalkerEnum {
    fn from(walker: RecursiveWalker) -> Self {
        Self::RecursiveWalker(walker)
    }
}

impl From<ForIndexWalker> for WalkerEnum {
    fn from(walker: ForIndexWalker) -> Self {
        Self::ForIndexWalker(walker)
    }
}

/// TODO
#[allow(unused)] // TODO
pub struct ForIndexWalker {
    inner: Box<dyn Iterator<Item = VfsPath>>,
    level: usize,
    child_dirs: Vec<VfsPath>,
}

/// TODO
pub struct IntoFilePathIterator {
    inner: Box<dyn Iterator<Item = VfsPath>>,
}

impl Iterator for IntoFilePathIterator {
    type Item = VfsPath;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(Into::into)
    }
}

impl IntoFilePathIterator {
    fn from(inner: Box<dyn Iterator<Item = VfsPath>>) -> Box<Self> {
        Box::new(Self { inner })
    }
}

impl ForIndexWalker {
    /// TODO
    ///
    /// # Errors
    /// // TODO
    pub fn try_new(base_path: &VfsPath) -> Result<Self, VfsError> {
        Ok(Self {
            inner: IntoFilePathIterator::from(base_path.read_dir()?),
            level: 0,
            child_dirs: vec![],
        })
    }
}

impl Iterator for ForIndexWalker {
    type Item = VfsPath;

    fn next(&mut self) -> Option<Self::Item> {
        todo!("1673514233")
        // use VfsFileType::*;
        // loop {
        //     for path in self.inner.by_ref() {
        //         match path.metadata() {
        //             Ok(m) if m.file_type == Directory && self.level == 0 => {
        //                 self.child_dirs.push(path);
        //             }
        //             Err(_) => (), // Ignore Errors
        //             Ok(m) if m.file_type == Directory => (), // Ignore 2nd level child directory
        //             Ok(_) if path.extension() != Some("md".to_string()) => (), // Ignore non-markdown files
        //             Ok(_)
        //                 if self.level == 0 && path.filename() == "index.md" => {
        //             } // Ignore child `index.md`
        //             Ok(_)
        //                 if self.level != 0 && path.filename() != "index.md" => {
        //             } // Ignore grandchild files with names other than index.md
        //             _ => return Some(path),
        //         }
        //     }

        //     self.level = 1;

        //     let dir = self.child_dirs.pop()?;
        //     if let Ok(iter) = dir.read_dir() {
        //         self.inner = IntoFilePathIterator::from(iter);
        //     }
        // }
    }
}

/// TODO
pub struct RecursiveWalker {
    /// TODO
    pub iterator: DirWalker,
}

impl Iterator for RecursiveWalker {
    type Item = VfsPath;

    /// Iterate over all the files in the directory, applying filters
    ///
    /// Current items filtered out are:
    /// - the `.git` directory
    /// - any file inside the `.git` directory
    /// - paths with errors
    ///
    /// Example
    /// =======
    ///
    /// ```
    /// // TODO
    ///  // use markdown_database_lib::list_files_use_case::FilterWrapper;
    ///  // use markdown_database_lib::wrapper::file_path::prelude::*;
    /// //
    ///  // let root: FilePath = MemoryFS::new().into();
    ///  // let base_dir: FilePath = root.join("test/path").unwrap();
    ///  // let keeper_file = base_dir.join("keeper.file").unwrap();
    /// //
    ///  // let actual = FilterWrapper {
    ///  //     iterator: vec![
    ///  //         Ok(base_dir.join(".git").unwrap()),
    ///  //         Ok(base_dir.join(".git/child.file").unwrap()),
    ///  //         Ok(keeper_file.clone()),
    ///  //         Err(VfsError::Other {
    ///  //             message: "1631910586".to_string(),
    ///  //         }),
    ///  //     ]
    ///  //     .into_iter(),
    ///  // };
    /// //
    ///  // assert_eq!([keeper_file], actual.collect::<Vec<FilePath>>())
    /// ```
    fn next(&mut self) -> Option<Self::Item> {
        // flatten to skip Result::Err
        self.iterator.by_ref().find(|path| {
            !path.as_str().contains(".git/")
                && !Path::new(path.as_str())
                    .extension()
                    .map_or(false, |ext| ext.eq_ignore_ascii_case("git"))
        })
    }
}

/// TODO
pub struct RequestModel {
    /// TODO
    pub base_dir: VfsPath,
    /// TODO
    pub for_index: bool,
}

/// TODO
pub struct ResponseModel {
    /// TODO
    pub walker: WalkerEnum,
}

impl Iterator for ResponseModel {
    type Item = VfsPath;

    fn next(&mut self) -> Option<Self::Item> {
        self.walker.next()
    }
}

/// TODO
#[must_use]
pub fn list_files(
    RequestModel {
        base_dir,
        for_index,
    }: RequestModel,
) -> ResponseModel {
    ResponseModel {
        walker: if for_index {
            ForIndexWalker::try_new(&base_dir)
                .expect("1673514136")
                .into() // TODO FIX ME
        } else {
            RecursiveWalker {
                iterator: DirWalker::try_new(&base_dir).expect("1673514115"), // TODO FIX ME
            }
            .into()
        },
    }
}

#[cfg(test)]
mod test {
    use std::collections::HashMap;
    use vfs::VfsPath;

    #[allow(unused)] // TODO
    struct TestEnvironment {
        base: VfsPath,
        file1: VfsPath,
        child2_index_file: VfsPath,
    }

    impl Default for TestEnvironment {
        fn default() -> Self {
            todo!("1673740052")
            // let base = VfsPath::new(MemoryFS::new());

            // let child_dir1 = base.join("child_dir1").unwrap();
            // child_dir1.create_dir();

            // let child_dir2 = base.join("child_dir3").unwrap();
            // child_dir2.create_dir();

            // let child_dir3 = base.join("child_dir2").unwrap();
            // child_dir3.create_dir();

            // fn add_file(file_name: &str, parent_dir: &VfsPath) -> VfsPath {
            //     let file = parent_dir.join(file_name).unwrap();
            //     let writer = file.create_file().unwrap();
            //     writer.flush();
            //     drop(writer); // Enforce flush
            //     file
            // }

            // { ["index.md", "non_markdown_file1.img"].iter() }.for_each(
            //     |name| {
            //         add_file(name, &base);
            //     },
            // );

            // add_file("ignored_file1.md", &child_dir1);
            // add_file("index.md", &child_dir3);

            // let file1 = add_file("file1.md", &base);
            // let child2_index_file =
            //     add_file("non_markdown_file2.img", &child_dir2);
            // Self {
            //     base,
            //     file1,
            //     child2_index_file,
            // }
        }
    }

    impl From<HashMap<String, VfsPath>> for TestEnvironment {
        fn from(mut hash_map: HashMap<String, VfsPath>) -> Self {
            Self {
                base: hash_map.remove("/base_dir").unwrap(),
                file1: hash_map.remove("/base_dir/file1.md").unwrap(),
                child2_index_file: hash_map
                    .remove("/base_dir/child_dir2/index.md")
                    .unwrap(),
            }
        }
    }

    #[test]
    fn test1639628340_for_index() {
        // TODO !("1673740033")
        // // Test Environment
        // let TestEnvironment {
        //     base: base_dir,
        //     file1,
        //     child2_index_file,
        //     ..
        // } = TestEnvironment::default();

        // // Given
        // let request_model = RequestModel {
        //     base_dir,
        //     for_index: true,
        // };

        // // When
        // let result = list_files(request_model);

        // // Then
        // assert_eq!(
        //     [file1, child2_index_file],
        //     result.unwrap().collect::<Vec<VfsPath>>()
        // )
    }

    #[test]
    fn test1639707927_primary_course() {
        // TODO !("1673740020")
        // // Test Environment
        // let mut mock_file_system = mock_file_system().shortcut_hashmap;
        // let mut take_path =
        //     |dir_name: &str| mock_file_system.remove(dir_name).unwrap();
        // let base_dir = take_path("/base_dir");

        // // Given
        // let request_model = RequestModel {
        //     base_dir,
        //     for_index: false,
        // };

        // // When
        // let result = list_files(request_model);

        // // Then
        // assert_eq!(
        //     [
        //         take_path("/base_dir/child_dir1"),
        //         take_path("/base_dir/child_dir1/ignored_file1.md"),
        //         take_path("/base_dir/child_dir2"),
        //         take_path("/base_dir/child_dir2/index.md"),
        //         take_path("/base_dir/child_dir2/non_markdown_file2.img"),
        //         take_path("/base_dir/child_dir3"),
        //         take_path("/base_dir/file1.md"),
        //         take_path("/base_dir/index.md"),
        //         take_path("/base_dir/non_markdown_file1.img"),
        //     ],
        //     VfsPath::sort_iter(result.unwrap())
        // )
    }
}
