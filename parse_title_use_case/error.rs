use super::*;

#[derive(Debug, PartialEq)]
pub enum ParseTitleError {
    UnknownError,
    TitleNotFound(TokenEnum),
    TitleParserNotDetected,
}

impl From<ParseFileError> for ParseTitleError {
    fn from(_: ParseFileError) -> Self {
        Self::UnknownError
    }
}
