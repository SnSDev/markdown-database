use super::*;

pub struct ParseTitleRequestModel<I: Iterator<Item = TokenEnum>> {
    pub token_iterator: I,
}

impl From<ParseFileResponseModel>
    for ParseTitleRequestModel<ParseFileResponseModel>
{
    fn from(response_model: ParseFileResponseModel) -> Self {
        Self {
            token_iterator: response_model,
        }
    }
}
