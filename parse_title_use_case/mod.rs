mod error;
mod request_model;
mod response_model;
mod use_case;

#[cfg(test)]
mod test;

pub use error::ParseTitleError;
pub use request_model::ParseTitleRequestModel;
pub use response_model::ParseTitleResponseModel;
pub use use_case::parse_title;

pub type ParseTitleResult = Result<ParseTitleResponseModel, ParseTitleError>;

use crate::compiler::TokenEnum;
use crate::parse_file_use_case::{ParseFileError, ParseFileResponseModel};
