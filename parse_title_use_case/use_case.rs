use super::*;

pub fn parse_title<I: Iterator<Item = TokenEnum>>(
    request_model: ParseTitleRequestModel<I>,
) -> ParseTitleResult {
    for token in request_model.token_iterator {
        match token {
            TokenEnum::Title(t) => {
                return Ok(ParseTitleResponseModel { title: t.title });
            }
            TokenEnum::TitleNotFound(t) => {
                return Err(ParseTitleError::TitleNotFound(t.into()));
            }
            _ => (), // Ignore other tokens
        }
    }

    Err(ParseTitleError::TitleParserNotDetected)
}
