use pretty_assertions::assert_eq;

use crate::parse_title_use_case::{
    parse_title, ParseTitleError, ParseTitleRequestModel,
    ParseTitleResponseModel,
};
use crate::{
    compiler::{
        title::{TitleNotFoundToken, TitleToken, TitleType},
        TokenEnum,
    },
    entity::Token,
};

#[test]
fn test1638928341_has_title() {
    // Given
    let token_iterator = vec![TokenEnum::Title(
        TitleToken {
            range: 0..0,
            title: "Title1".to_string(),
            title_type: TitleType::TBD,
        }
        .from_code(),
    )]
    .into_iter();

    // When
    let actual = parse_title(ParseTitleRequestModel { token_iterator });

    // Then
    assert_eq!(
        Ok(ParseTitleResponseModel {
            title: "Title1".to_owned(),
        }),
        actual
    );
}

#[test]
fn test1638928582_no_title() {
    // Given
    let problem_token =
        TokenEnum::TitleNotFound(TitleNotFoundToken { range: 0..0 });
    let token_iterator = vec![problem_token.clone()].into_iter();

    // When
    let actual = parse_title(ParseTitleRequestModel { token_iterator });

    // Then
    assert_eq!(Err(ParseTitleError::TitleNotFound(problem_token)), actual);
}

#[test]
fn test1639857047_no_parser() {
    // Given
    let token_iterator = Vec::<TokenEnum>::new().into_iter();

    // When
    let actual = parse_title(ParseTitleRequestModel { token_iterator });

    // Then
    assert_eq!(Err(ParseTitleError::TitleParserNotDetected), actual);
}
