#[derive(Debug, PartialEq)]
pub struct ParseTitleResponseModel {
    pub title: String,
}
