use crate::markdown_token::prelude::*;

/// TODO
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct UnderlineToken {
    line_number: usize,
    range: Range<usize>,
    contents: Vec<TokenEnum>,
}

impl TokenTrait for UnderlineToken {
    fn start_absolute(&self) -> usize {
        self.range.start
    }

    fn end_absolute(&self) -> usize {
        self.range.end
    }
}

impl UnderlineToken {
    /// TODO
    ///
    /// # Panics
    /// // TODO
    ///
    /// # Errors
    /// // TODO
    pub fn try_from<'a>(
        regex_capture: &Captures,
        caret: usize,
    ) -> Result<TokenEnum, &'a str> {
        let tag_match = regex_capture.name("tag").unwrap();
        let contents_match = regex_capture.name("contents").unwrap();

        let contents_str = contents_match.as_str();

        if contents_str.is_empty() {
            return Err("1673099220 - Token empty");
        }

        if contents_str.starts_with(' ') {
            return Err("1673099223 - Token Starts with Space");
        }

        if contents_str.ends_with(' ') {
            return Err("1673099226 - Token ends with space");
        }

        let line_number = 0;
        let range = caret + tag_match.start()..caret + tag_match.end();
        let contents = vec![TokenEnum::String(StringToken::new(
            0,
            caret + 2,
            contents_str,
        ))];

        Ok(TokenEnum::UnderlinedText(UnderlineToken {
            line_number,
            range,
            contents,
        }))
    }
}

#[cfg(test)]
mod test {

    #[test]
    fn test1673084459_underlined_text() {
        use crate::markdown_token::prelude::*;
        // GIVEN
        let token_type_array = [TokenType::UnderlinedText];
        let file_text =
            r#"This string has **bold** text and __underlined__ text"#;

        // WHEN
        let actual = Parser::from(token_type_array.to_vec())
            .parse(file_text)
            .collect::<Vec<TokenEnum>>();

        // THEN
        assert_eq!(
            actual,
            [
                TokenEnum::String(StringToken::new(
                    0,
                    0,
                    "This string has **bold** text and "
                )),
                TokenEnum::UnderlinedText(UnderlineToken {
                    line_number: 0,
                    range: 34..48,
                    contents: vec![TokenEnum::String(StringToken::new(
                        0,
                        36,
                        "underlined"
                    ))]
                }),
                TokenEnum::String(StringToken::new(0, 48, " text"))
            ]
        );
    }
}
