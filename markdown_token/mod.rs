//! TODO

mod bold_token;
mod iterator;
mod parser;
mod string_token;
mod token_enum;
mod token_trait;
mod token_type;
mod tokenizer;
mod underline_token;

/// TODO
pub mod prelude {
    pub use crate::markdown_token::{
        BoldToken, Iterator, Parser, StringToken, TokenEnum, TokenTrait,
        TokenType, Tokenizer, UnderlineToken,
    };
    pub use regex::{Captures, Regex};
    pub use std::ops::Range;
}

pub use bold_token::BoldToken;
pub use iterator::Iterator;
pub use parser::Parser;
pub use string_token::StringToken;
pub use token_enum::TokenEnum;
pub use token_trait::TokenTrait;
pub use token_type::TokenType;
pub use tokenizer::Tokenizer;
pub use underline_token::UnderlineToken;
