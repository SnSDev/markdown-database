use crate::markdown_token::prelude::*;

/// TODO
pub struct Iterator {
    tokenizer_vec: Vec<Tokenizer>,
    eof: usize,
    caret: usize,
    file_contents: String,
}

impl Iterator {
    /// TODO
    #[must_use]
    pub fn new(tokenizer_vec: Vec<Tokenizer>, file_contents: &str) -> Self {
        Self {
            tokenizer_vec,
            eof: file_contents.len(),
            caret: 0,
            file_contents: file_contents.to_owned(),
        }
    }
}

impl std::iter::Iterator for Iterator {
    type Item = TokenEnum;

    fn next(&mut self) -> Option<Self::Item> {
        if self.caret >= self.eof {
            return None;
        }

        let mut start_relative = self.eof - self.caret;
        let file_contents = &self.file_contents[self.caret..];

        for token in self.tokenizer_vec.iter().filter_map(|t| {
            let (token_type, regex_capture) = t.find_match(file_contents)?;
            match token_type {
                TokenType::BoldText => {
                    BoldToken::try_from(&regex_capture, self.caret)
                }
                TokenType::UnderlinedText => {
                    UnderlineToken::try_from(&regex_capture, self.caret)
                }
            }
            .ok()
        }) {
            start_relative =
                start_relative.min(token.start_absolute() - self.caret);

            if start_relative == 0 {
                self.caret = token.end_absolute();
                return Some(token);
            }
        }

        let line_number = 0;
        let contents =
            &self.file_contents[self.caret..self.caret + start_relative];

        let caret = self.caret;
        self.caret += start_relative;
        Some(TokenEnum::String(StringToken::new(
            line_number,
            caret,
            contents,
        )))
    }
}
