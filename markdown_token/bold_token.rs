use crate::markdown_token::prelude::*;

/// TODO
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct BoldToken {
    line_number: usize,
    range: Range<usize>,
    contents: Vec<TokenEnum>,
}

impl TokenTrait for BoldToken {
    fn start_absolute(&self) -> usize {
        self.range.start
    }

    fn end_absolute(&self) -> usize {
        self.range.end
    }
}

impl BoldToken {
    /// TODO
    ///
    /// # Panics
    ///
    /// //TODO
    ///
    /// # Errors
    ///
    /// //TODO
    pub fn try_from<'a>(
        regex_capture: &Captures,
        caret: usize,
    ) -> Result<TokenEnum, &'a str> {
        let tag_match = regex_capture.name("tag").unwrap();
        let contents_match = regex_capture.name("contents").unwrap();

        let contents_str = contents_match.as_str();

        if contents_str.is_empty() {
            return Err("1673089833 - Token empty");
        }

        if contents_str.starts_with(' ') {
            return Err("1673089857 - Token Starts with Space");
        }

        if contents_str.ends_with(' ') {
            return Err("1673089804 - Token ends with space");
        }

        let line_number = 0;
        let range = caret + tag_match.start()..caret + tag_match.end();
        let contents = vec![TokenEnum::String(StringToken::new(
            0,
            caret + 2,
            contents_str,
        ))];

        Ok(TokenEnum::BoldText(BoldToken {
            line_number,
            range,
            contents,
        }))
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    #[test]
    fn test1673075133_bold_text() {
        use crate::markdown_token::prelude::*;
        // GIVEN
        let token_type_array = [TokenType::BoldText];
        let file_text =
            r#"This string has **bold** text and _underlined_ text"#;

        // WHEN
        let actual = Parser::from(token_type_array.to_vec())
            .parse(file_text)
            .collect::<Vec<TokenEnum>>();

        // THEN
        assert_eq!(
            actual,
            [
                TokenEnum::String(StringToken::new(0, 0, "This string has ")),
                TokenEnum::BoldText(BoldToken {
                    line_number: 0,
                    range: 16..24,
                    contents: vec![TokenEnum::String(StringToken::new(
                        0, 18, "bold"
                    ))]
                }),
                TokenEnum::String(StringToken::new(
                    0,
                    24,
                    " text and _underlined_ text"
                ))
            ]
        );
    }

    #[test]
    fn test1673078132_bold_text_invalid() {
        use crate::markdown_token::prelude::*;
        // GIVEN
        let token_type_array = [TokenType::BoldText];
        let file_text =
            r#"This string has **bold ** text and _underlined_ text"#;

        // WHEN
        let actual = Parser::from(token_type_array.to_vec())
            .parse(file_text)
            .collect::<Vec<TokenEnum>>();

        // THEN
        assert_eq!(
            actual,
            [TokenEnum::String(StringToken::new(
                0,
                0,
                "This string has **bold ** text and _underlined_ text"
            )),]
        );
    }
}
