use crate::markdown_token::prelude::*;

/// TODO
pub struct Tokenizer {
    token_type: TokenType,
    regex: Regex,
}

impl From<&TokenType> for Tokenizer {
    fn from(value: &TokenType) -> Self {
        match value {
            TokenType::BoldText => Tokenizer {
                token_type: *value,
                regex: Regex::new(r"(?P<tag>\*\*(?P<contents>.*?)\*\*)")
                    .expect("1673076186 - Previously validated"),
            },
            TokenType::UnderlinedText => Tokenizer {
                token_type: *value,
                regex: Regex::new(r"(?P<tag>__(?P<contents>.*?)__)")
                    .expect("1673084709 - Previously validated"),
            },
        }
    }
}

impl Tokenizer {
    /// TODO
    #[must_use]
    pub fn find_match<'t>(
        &'t self,
        file_contents: &'t str,
    ) -> Option<(TokenType, Captures<'t>)> {
        self.regex
            .captures(file_contents)
            .map(|c| (self.token_type, c))
    }
}
