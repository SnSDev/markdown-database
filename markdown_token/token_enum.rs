use crate::markdown_token::prelude::*;

/// TODO
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum TokenEnum {
    /// TODO
    String(StringToken),

    /// TODO
    BoldText(BoldToken),

    /// TODO
    UnderlinedText(UnderlineToken),
}

impl std::ops::Deref for TokenEnum {
    type Target = dyn TokenTrait;
    fn deref(&self) -> &Self::Target {
        match self {
            Self::String(t) => t,
            Self::BoldText(t) => t,
            Self::UnderlinedText(t) => t,
        }
    }
}
