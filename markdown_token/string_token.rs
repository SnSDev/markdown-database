use crate::markdown_token::prelude::*;

/// TODO
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct StringToken {
    line_number: usize,
    range: Range<usize>,
    contents: String,
}

impl StringToken {
    /// TODO
    #[must_use]
    pub fn new(line_number: usize, caret: usize, contents: &str) -> Self {
        Self {
            line_number,
            range: caret..caret + contents.len(),
            contents: contents.to_owned(),
        }
    }
}

impl TokenTrait for StringToken {
    fn start_absolute(&self) -> usize {
        self.range.start
    }

    fn end_absolute(&self) -> usize {
        self.range.end
    }
}
