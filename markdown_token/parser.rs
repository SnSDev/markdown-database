use crate::markdown_token::prelude::*;

/// TODO
pub struct Parser {
    token_type_vec: Vec<TokenType>,
}

impl From<Vec<TokenType>> for Parser {
    fn from(token_type_vec: Vec<TokenType>) -> Self {
        Self { token_type_vec }
    }
}

impl Parser {
    /// //TODO
    ///
    /// # Panics
    ///
    /// // TODO
    #[must_use]
    pub fn parse(self, file_contents: &str) -> Iterator {
        Iterator::new(
            self.token_type_vec.iter().map(Tokenizer::from).collect(),
            file_contents,
        )
    }
}
